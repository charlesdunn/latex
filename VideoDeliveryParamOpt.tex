\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{mathtools}
\usepackage{esint}
\usepackage{tikz}
\usepackage{float}
\usepackage{bm}
\usetikzlibrary{matrix,arrows,calc}

\newcounter{sarrow}
\newcommand\tab[1][1cm]{\hspace*{#1}}
\newcommand\xrsquigarrow[1]{%
\stepcounter{sarrow}%
\begin{tikzpicture}[decoration=snake]
\node (\thesarrow) {\strut#1};
\draw[->,decorate] (\thesarrow.south west) -- (\thesarrow.south east);
\end{tikzpicture}%
}


\DeclareMathOperator\erf{erf}
\DeclareMathOperator\erfinv{erf^{-1}}

\DeclareMathOperator*{\argmin}{arg\,min}

\title{Video Delivery Parameter Optimization}
\author{charles dunn\\YouVisit}
\date{\today}

\begin{document}

\maketitle
With Adaptive Focus, there are many parameters to tune in order to deliver the best viewing experience to users at the lowest possible cost. In this paper, we analyze the parameter space.

\section{Cost Function}

In general, there are three main values to consider: the cost of processing, the cost of storage, and the value of the viewing experience. These are combined to calculate the overall cost of deploying a system with certain parameters. Our goal is to reduce cost and increase value.\\
\tab$C_P$ is the cost in dollars per month of processing all new videos.\\
\tab$C_S$ is the cost in dollars per month of storing all videos.\\
\tab$V_R$ is the value in dollars per month for the average viewing experience.\\
\tab$C$ is the cost in dollars per month of processing, storing, and displaying all videos.\\
\begin{equation}
C = C_P + C_S - V_R
\end{equation}

\section{Processing}

There are many possible services to process our videos on. In order to create as general an equation as possible, we assume processing is charged in dollars per processing time, and that the number of processors can scale adaptively to the demand. Specifically, for this particular cost model, we assumed that if we always kept one more process open than was being used, that we could process any additional videos coming in by processing them on this overhead processor while a new overhead processing instance is requested. The essentially means that over any single month, we will pay for the total number of processors to process the all videos uploaded during the month plus one nearly-persistent idle overhead process, running for the duration of the month.\\
\tab$\lambda_P$ is the cost in dollars for one hour of processing.\\
\tab$P_{i}$ is the total processing time for all renditions per second of input video.\\
\tab$T^u_m$ is the total combined time in seconds of all new (i.e. uploaded or  unprocessed) input videos for month $m$.\\
\tab$T_o$ is the total seconds of overhead processing per month.\\
\tab$N_c$ is the average number of cores per processor.\\
\tab$N_{\phi}$ is the number of vertical renditions, either zero or two.\\
\tab$N_{\theta}$ is the number of equally-spaced horizon renditions.\\
\tab$N_r$ is the total number of renditions.\\
\begin{equation}
N_r = N_{\theta} + N_{\phi}
\end{equation}
\begin{equation}
C_P = \frac{\lambda_P}{3600} \left \lceil \frac{P_iT^u_m + T_o N_r}{N_c}\right \rceil
\end{equation}
\tab$P_r$ is the seconds of processing per rendition per second of input video.\\
\tab$P_0$ is the rendition-independent seconds of processing per second of input video.\\
\begin{equation}
P_i = P_rN_r + P_0
\end{equation}
\tab$N_p$ is the number of pixels per video rendition.\\
\tab$\gamma_{B}$ is the ideal megabits per second per pixel.\\
\tab$B$ is the video bitrate in megabits per second.\\
\begin{equation}
N_p = \gamma_{B}B
\end{equation}
\tab$\gamma_p$ is the seconds of processing per pixel per rendition per second of input video.\\
\tab$P_1$ is the pixel-independent seconds of processing per rendition per second of input video.\\
\begin{equation}
P_r = \gamma_p  N_p + P_1
\end{equation}
\begin{equation}
C_P = \frac{\lambda_P}{3600} \left \lceil \frac{\left ((\gamma_p  \gamma_{B}B + P_1)(N_{\theta} + N_{\phi}) + P_0\right )T^u_m + T_o (N_\theta + N_\phi )}{N_c} \right \rceil 
\end{equation}
\begin{equation}
C_P = \frac{\lambda_P}{3600} \left \lceil \frac{((\gamma_p  \gamma_{B}B + P_1)T^u_m + T_o)(N_{\theta} + N_{\phi}) + P_0 T^u_m}{N_c} \right \rceil 
\end{equation}

The factor of 3600 is the conversion from hours to seconds.

Note that the total processing cost per month is approximately linear in nearly all of the parameters, with the exception of the number of cores per processor.

\section{Storage}

The cost of storage is small compared to the other terms in the cost equation. It mostly depends on the bitrate of the videos and the number of renditions.\\
\tab$\lambda_S$ is the cost in dollars per month per gigabyte of storage.\\
\tab$T_m$ is the total seconds of all videos at the beginning of month $m$.\\
\tab$T^u_m$ is the total combined time in seconds of all new (i.e. uploaded or  unprocessed) input videos for month $m$.\\
\begin{equation}
C_S = \frac{\lambda_S BN_{r}(T_m + \frac{T^u_m}{2})}{8000}
\end{equation}
\begin{equation}
C_S = \frac{\lambda_S B(N_{\theta} + N_{\phi})(T_m + \frac{T^u_m}{2})}{8000}
\end{equation}
The factor of $8000$ is simply a conversion from megabits to gigabytes. Note that a video uploaded during month $m$ will on average be stored for half the month, so the approximate storage needed for the entire month is the length of video at the beginning of the month plus half the length of video uploaded during the month.

Note how the cost of storage is linear in all the parameters.

There are some other factors as well that are not currently included in this equation. Amazon S3 charges additionally for interactions with the data. A nominally fee is charged for every put, copy, get, list, or post command. The cost over a month for these commands depends on the chunk size, the number of viewers, and more factors, but depends a negligible amount on the number of renditions. In general, this cost is smaller than the basic cost of storage for data.

Additionally, there are caching costs for services like Amazon Cloudfront. These costs depend only on the data transfered in or out of the caches. This cost will again depend on chunk size and the number of viewers, but not the number of renditions. A more complex model of storage costs is probably required, but the overall trend is captured in than additional renditions cost more money to store, cache, and serve.

\section{Viewing Experience}

In order to derive a final cost per month for a given set of parameters, we must assign a dollar value for a certain viewing experience. There are three terms we felt contributed to the overall viewing experience. The first is simply the average resolution during normal viewing. This is potentially the most important. The second is the average proportional change in resolution seen during a chunk. If the resolution distribution varies greatly spatially, then as a user turns her head, she sees dramatically different resolutions. This is distracting, and ideally our methods will improve resolution without the user being aware of a difference from serving a higher resolution single rendition. The third term is the average proportional change in resolution seen across chunks. When a new chunk arrives, it could be from the same rendition, or from a new rendition. This sudden change in resolution is extremely noticeable if it is large. We would like to minimize this change.

All resolution units are the number of pixels for an equirectangular rendition with equivalent resolution along the horizon, abbreviated as equivalent equirectangular pixels (eeps).\\
\tab$\mu_R$ is the average resolution in eeps.\\
\tab$\sigma_R$ is the proportional intra-chunk resolution change.\\
\tab$\Delta_R$ is the proportional inter-chunk resolution change.\\
\tab$\lambda_\mu$ is the value in dollars per month per eep of average resolution.\\
\tab$\lambda_\sigma$ is the cost in dollars per month per unit intra-chunk resolution change.\\
\tab$\lambda_\Delta$ is the cost in dollars per month per unit intra-chunk resolution change.\\
\tab$t$ is the seconds per chunk.\\
\tab$V_R$ is the cost in dollars per month per unit inter-chunk resolution change.\\
\begin{equation}
V_R = \lambda_\mu\mu_R - \frac{\lambda_\sigma}{t} \sigma_R - \frac{\lambda_\Delta}{t}\Delta_R
\end{equation}
\tab$(\theta_0,\phi_0)$ is the view direction at the beginning of a chunk.\\
\tab$(\theta,\phi)$ is the view direction at the end of a chunk.\\
\tab$\theta_\leftrightarrow$ is the azimuth from a horizon rendition's center to its transition.\\
\tab$\phi_\updownarrow$ is the elevation from a horizon rendition's center to its transition.\\
\tab$(\sigma_\theta,\sigma_\phi)$ is the standard deviation of view direction in radians per second.\\
\tab $R(\theta,\phi,\alpha,\beta,\gamma_\theta,\gamma_\phi,N_p,N_-,N_:) = R_{\alpha,\beta}(\theta,\phi)$ is the resolution at coordinates $(\theta,\phi)$ for the rendition whose zone contains $(\alpha,\beta)$ with parameters $(\gamma_\theta,\gamma_\phi)$ and total pixels $N_p$ for an arrangement with $N_-$ horizon renditions and $N_:$ vertical renditions.

In measuring these values, we know that a user is viewing a single rendition for an entire chunk. At the start of the next chunk, the user's position will be measured, and the rendition with the highest resolution will be delivered. Note that there are some details overlooked here. Specifically, we ignore the time it takes to request a rendition and any associated delays.

We assume a normal distribution of starting positions in spherical coordinates, carefully taking in to account the Jacobian to downweight large elevations.
\begin{equation}
P(\theta_0,\phi_0) = P(\theta_0)P(\phi_0) = \frac{1}{\kappa_0}\exp\left( -\frac{(\theta_0 - m_{\theta})^2}{2 s_{\theta}^2}-\frac{(\phi_0 - m_{\phi})^2}{2 s_{\phi}^2}\right )\cos(\phi_0)
\end{equation}
See the appendix for how to calculate the normalization term $\kappa_0$.
\begin{equation}
\kappa_0 = 2\pi s_\theta s_\phi \erf\left( \frac{\pi}{\sqrt{2} s_\theta}\right) \exp\left ( \frac{-2s_\phi^2}{4}\right ) Re \left \{ \erf \left (\frac{\pi/2-i  s_\phi^2}{ \sqrt{2} s_\phi}\right) \right \}
\end{equation}
We model the viewing behavior of all users as a two dimensional Gaussian random walk. If users vary their viewing direction with standard deviations $\hat{\sigma}_\theta$ and $\hat{\sigma}_\phi$ in radians per second, and a chunk is $t$ seconds long, then the standard deviations over a chunk can be calculated.
\begin{equation}
 \sigma_\theta = \sqrt{t} \hat{\sigma}_\theta
\end{equation}
\begin{equation}
 \sigma_\phi = \sqrt{t} \hat{\sigma}_\phi
\end{equation}
We can solve for the probability distribution of the starting and ending viewing directions of a chunk. $(\tilde{\theta},\tilde{\phi})$ are the coordinates of $(\theta,\phi)$ after rotating by $(-\theta_0,-\phi_0)$, to account for the rotation of the Gaussian distribution centered at the view direction at the beginning of a chunk.
\begin{equation}
P(\theta,\phi,\theta_0,\phi_0) = P(\theta_0,\phi_0)P(\theta,\phi |\theta_0,\phi_0) = P(\theta_0,\phi_0)\frac{1}{\kappa}\exp \left( -\frac{   \tilde{\theta}^2   }{2t\sigma_\theta^2} -\frac{   \tilde{\phi}^2   }{2t\sigma_\phi^2} \right )\cos(\phi)
\end{equation}
$\kappa$ is the normalization term for $P(\theta,\phi |\theta_0,\phi_0)$. 
Note that this normalization shouldn't depend on $(\theta_0,\phi_0)$ since they simply rotate the sphere we are integrating over. We can therefore drop the rotations ($\tilde{\cdot}$) for the calculation of $\kappa$.
\begin{equation}
\kappa = 2\pi t \sigma_\theta \sigma_\phi \erf\left( \frac{\pi}{\sqrt{2 t} \sigma_\theta}\right) \exp\left ( \frac{-2t\sigma_\phi^2}{4}\right ) Re \left \{ \erf \left (\frac{\pi/2-i  t \sigma_\phi^2}{ \sqrt{2 t} \sigma_\phi}\right) \right \}
\end{equation}
\begin{equation}
P(\theta,\phi,\theta_0,\phi_0) = \frac{1}{\kappa_0\kappa}\exp\left( -\frac{(\theta_0 - m_{\theta})^2}{2 s_{\theta}^2}-\frac{(\phi_0 - m_{\phi})^2}{2 s_{\phi}^2} -\frac{   \tilde{\theta}^2   }{2t\sigma_\theta^2} -\frac{   \tilde{\phi}^2   }{2t\sigma_\phi^2} \right )\cos(\phi_0)\cos(\phi)
\end{equation}
We can now find the expected value of any function over all values of $(\theta,\phi,\theta_0,\phi_0)$.
\begin{equation}
\mathbf{E}[f(\theta,\phi,\theta_0,\phi_0)] = \int_{-\pi}^{\pi} \int_{-\pi/2}^{\pi/2} \int_{-\pi}^{\pi} \int_{-\pi/2}^{\pi/2} P(\theta,\phi,\theta_0,\phi_0)f(\theta,\phi,\theta_0,\phi_0)\; d\theta \, d\phi \; d\theta_0 \, d\phi_0
\end{equation}
Depending on the values of the initial viewing direction parameters, we might be able to simplify the integral slightly by only integrating over half or one quarter of the sphere.

We approximate the average resolution viewed over a chunk by linearly averaging the resolutions at the beginning and at the end of the chunk. This is an approximation of the average resolution of all the possible paths from the starting position to the ending position, weighted by their likelihood. Note that the resolution at the beginning of the chunk is $R_{\theta_0,\phi_0}(\theta_0,\phi_0)$ and the resolution at the end of the chunk is $R_{\theta_0,\phi_0}(\theta,\phi)$.\\
\tab$\overline{\mu}_R(\theta,\phi,\theta_0,\phi_0)$ is the approximated average resolution over a chunk with starting view direction $(\theta_0,\phi_0)$ and ending view direction $(\theta,\phi)$.
\begin{equation}
\overline{\mu}_R(\theta,\phi,\theta_0,\phi_0) = \frac{R_{\theta_0,\phi_0}(\theta_0,\phi_0) + R_{\theta_0,\phi_0}(\theta,\phi)}{2} 
\end{equation}
\begin{equation}
\mu_R = \mathbf{E}[\overline{\mu}_R] = \int_{-\pi}^{\pi} \int_{-\pi/2}^{\pi/2} \int_{-\pi}^{\pi} \int_{-\pi/2}^{\pi/2} P(\theta,\phi,\theta_0,\phi_0)\frac{R_{\theta_0,\phi_0}(\theta_0,\phi_0) + R_{\theta_0,\phi_0}(\theta,\phi)}{2} \; d\theta \, d\phi \; d\theta_0 \, d\phi_0
\end{equation}

We can similarly calculate the intra-chunk proportional resolution change.\\
\tab$\overline{\sigma}_R(\theta,\phi,\theta_0,\phi_0)$ is the approximated average intra-chunk proportional resolution change over a chunk with starting view direction $(\theta_0,\phi_0)$ and ending view direction $(\theta,\phi)$.
\begin{equation}
\overline{\sigma}_R(\theta,\phi,\theta_0,\phi_0) = \frac{R_{\theta_0,\phi_0}(\theta_0,\phi_0) - R_{\theta_0,\phi_0}(\theta,\phi)}{R_{\theta_0,\phi_0}(\theta_0,\phi_0)} 
\end{equation}
\begin{equation}
\sigma_R = \mathbf{E}[\overline{\sigma}_R] = \int_{-\pi}^{\pi} \int_{-\pi/2}^{\pi/2} \int_{-\pi}^{\pi} \int_{-\pi/2}^{\pi/2} P(\theta,\phi,\theta_0,\phi_0)\frac{R_{\theta_0,\phi_0}(\theta_0,\phi_0) - R_{\theta_0,\phi_0}(\theta,\phi)}{R_{\theta_0,\phi_0}(\theta_0,\phi_0)}  \; d\theta \, d\phi \; d\theta_0 \, d\phi_0
\end{equation}

To calculate the inter-chunk proportional resolution change by looking at the difference between the resolution at the end of a chunk and resolution in the same view direction but with the new rendition's resolution distribution.\\
\tab$\overline{\Delta}_R(\theta,\phi,\theta_0,\phi_0)$ is the approximated average inter-chunk proportional resolution change over a chunk with starting view direction $(\theta_0,\phi_0)$ and ending view direction $(\theta,\phi)$.
\begin{equation}
\overline{\Delta}_R(\theta,\phi,\theta_0,\phi_0) = \frac{R_{\theta,\phi}(\theta,\phi) - R_{\theta_0,\phi_0}(\theta,\phi)}{R_{\theta_0,\phi_0}(\theta,\phi)} 
\end{equation}
\begin{equation}
\Delta_R = \mathbf{E}[\overline{\Delta}_R] = \int_{-\pi}^{\pi} \int_{-\pi/2}^{\pi/2} \int_{-\pi}^{\pi} \int_{-\pi/2}^{\pi/2} P(\theta,\phi,\theta_0,\phi_0)\frac{R_{\theta,\phi}(\theta,\phi) - R_{\theta_0,\phi_0}(\theta,\phi)}{R_{\theta_0,\phi_0}(\theta,\phi)}  \; d\theta \, d\phi \; d\theta_0 \, d\phi_0
\end{equation}

\section{Appendix}

\subsection{Spherical Gaussian Normalization}

Here we calculate the normalization for a two-dimensional normal distribution over a sphere.

\begin{equation}
\kappa = \int_{-\pi}^{\pi} \int_{-\pi/2}^{\pi/2} \exp \left( -\frac{   ({\theta} - \mu_\theta)^2   }{2 \sigma_\theta^2}  -\frac{   ({\phi} - \mu_\phi)^2   }{2\sigma_\phi^2} \right ) \cos(\phi)\; d\phi\: d\theta
\end{equation}
Because rotation of the sphere doesn't affect the integral of a function over the entire sphere, we can ignore the means.
\begin{equation}
\kappa = \int_{-\pi}^{\pi} \int_{-\pi/2}^{\pi/2} \exp \left( -\frac{   {\theta}^2   }{2 \sigma_\theta^2}  -\frac{ {\phi} ^2   }{2\sigma_\phi^2} \right ) \cos(\phi)\; d\phi\: d\theta
\end{equation}
\begin{equation}
\kappa = \int_{-\pi}^{\pi} \exp \left( -\frac{{\theta} ^2}{2 \sigma_\theta^2} \right ) \; d\theta \int_{-\pi/2}^{\pi/2} \exp \left(-\frac{   {\phi}^2 }{2\sigma_\phi^2} \right )\cos(\phi) \; d\phi
\end{equation}
\begin{equation}
\kappa = \sqrt{2\pi} \sigma_\theta \erf\left( \frac{\pi}{\sqrt{2} \sigma_\theta}\right) \exp\left ( \frac{-2\sigma_\phi^2}{4}\right ) \sqrt{2 \pi } \sigma_\phi  Re \left \{ \erf \left (\frac{\pi/2-i  \sigma_\phi^2}{ \sqrt{2} \sigma_\phi}\right) \right \}
\end{equation}
\begin{equation}
\kappa = 2\pi \sigma_\theta \sigma_\phi \erf\left( \frac{\pi}{\sqrt{2} \sigma_\theta}\right) \exp\left ( \frac{-2\sigma_\phi^2}{4}\right ) Re \left \{ \erf \left (\frac{\pi/2-i  \sigma_\phi^2}{ \sqrt{2} \sigma_\phi}\right) \right \}
\end{equation}

\end{document}
