\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{esint}

\title{360$^{\circ}$ Mapping}
\author{charles dunn }
\date{December 2015}

\begin{document}

\maketitle

\section{Introduction}

360$^\circ$ content is omnidirectional content for virtual reality devices. In order to display the content to the user, a flat image or video is UV-mapped on to a 3-dimensional shape. Regardless of the shape chosen, the \emph{effective resolution} is measured in units of pixels per solid angle. In other words, regardless of the shape chosen, the pixel density on a sphere is the salient metric.

Still, the shape chosen has a dramatic effect on the effective resolution. For example, the ubiquitous equirectangular mapping creates a rectangular grid of latitudes and longitudes. This mapping samples the image sphere much more densely near the poles than at the equator. This idea can be easily comprehended by noting the relative size of Antarctica on a equirectangular projection of Earth (https://en.wikipedia.org/wiki/Equirectangular\_projection). The result is a much higher effective resolution near the poles than at the equator. This mapping is considered suboptimal for 360$^\circ$ content, since there is often more interesting content near the equator.

Since the resolution on a sphere is the important measure, our goal is to find a flat projection or mapping of a sphere with a higher resolution in important regions. A similar problem has been studied in depth by cartographers in "flattening" the globe. Our constraints are slightly different in that we require the final mapping to be rectangular to efficiently compress the content using traditional encoding methods (e.g. JPEG, H.264). A non-rectangular mapping would require padding to make it rectangular, requiring the encoding of informationless pixels. Since the flattened sphere will be sent through encoding methods with macroblocks that leverage the typically low-frequency nature of images, there is also an incentive to preserve pixel adjacency in the mapping from sphere to rectangle.

The goal of this paper is to compare equirectangular and cubic mappings to eachother, as well as to a (possibly theoretical) optimal mapping.

\section{Metric Functions}

To compare any projections, we need a measure of optimality. Simply asking which projection is "better" is a poorly defined question. Any valid mapping will take some number of pixels $N_p$ and map it to the sphere. Allowing for theoretical fractional pixels, every valid mapping of $N_p$ pixels maps to exactly $N_p$ pixels on the sphere. So any valid mapping has the same total and average resolutions on the sphere. What really matters is the relative local densities of pixels between the mappings.

We investigated two metric functions. The first is a weight function and the second is a minimum resolution function.

\subsection{Weight Function}

The weight function $w_n$ is a normalized function defined on the sphere signifying the relative importance of regions on the sphere. If the weight function has a value at the equator double its value at the poles, then the ideal projection would result in double the resolution at the equator compared to the poles. We evaluate a projection's score $\mu$ by calculating a weighted integral of its normalized resolution function $r_n$ over the unit sphere (u.s.) in Cartesian coordinates.

\begin{equation}
w_n(x,y,z) = \frac{w(x,y,z)}{\iint\displaylimits_{u.s.} w(x,y,z) dA}
\end{equation}

\begin{equation}
r_n(x,y,z) = \frac{r(x,y,z)}{\iint\displaylimits_{u.s.} r(x,y,z) dA}
\end{equation}

\begin{equation}
\mu = \iint\displaylimits_{u.s.} w_n(x,y,z)r_n(x,y,z) dA
\end{equation}

It can be verified that the maximum value of the integral of the product of two normalized functions, one defined and one variable, occurs when the two functions are identical (IS THIS ACTUALLY TRUE?!). Therefore, the maximum $\mu$ occurs when the normalized resolution function is the same as the normalized weight functions.

Note that this integral is much simpler to evaluate and analyze in projection-coordinate space. To account for the distortion of space, we include the magnitude of the determinant of the Jacobian ($||J||$).

\begin{equation}
\mu = \iint\displaylimits_{u.s.} w_n(u,v)r_n(u,v) ||J(u,v)||dA
\end{equation}

However, by definition, the resolution in projection coordinates is constant and is the ratio of the total number of pixels $N_p$ to the total area of the unit sphere in projection coordinates.

\begin{equation}
r(u,v) = \frac{N_p}{\iint\displaylimits_{u.s.} dA}
\end{equation}

\begin{equation}
r_n(u,v) = \frac{r(u,v)}{\iint\displaylimits_{u.s.} r(u,v) dA} = \frac{\frac{N_p}{\iint\displaylimits_{u.s.} dA}}{\frac{N_p}{\iint\displaylimits_{u.s.} dA}\iint\displaylimits_{u.s.} dA} = \frac{1}{\iint\displaylimits_{u.s.} dA}
\end{equation}


\begin{equation}
\mu = \frac{1}{\iint\displaylimits_{u.s.} dA} \iint\displaylimits_{u.s.} w_n(u,v) ||J(u,v)||dA
\end{equation}

We see that a projection's score based on a weight function is solely a function of its Jacobian.

\subsection{Minimum Resolution Function}

Instead of defining a weight function, we could defined a minimum resolution function over the unit sphere. We then apply the constraint that the effective resolution of a projection be at least this value at any point on the sphere. This is different from the weight function in that 




\section{Jacobian Derivations}

Since the Jacobian is so important in our metrics, we will show the derivation of the determinant of the Jacobian for the equirectangular and cubic projections.

\subsection{Equirectangular}

Let our equirectangular space be defined as the three dimensional point $(\theta,\phi,\rho)$ where $\theta$ is the azimuth angle counterclockwise from the $+x$ axis in the $xy$-plane, $\phi$ is the elevation angle towards $+z$ from the $xy$-plane, and $\rho$ is the magnitude of the distance from the origin. Note that we will later apply the constraint $\rho=1$, creating a two dimensional equirectangular space parameterized by $-\pi\leq\theta<\pi$ and $-\frac{\pi}{2}\leq \phi < \frac{\pi}{2}$. This final projection space is rectangular and maps the the entire unit sphere.

\begin{equation}
x = \rho \cos\phi \cos\theta
\end{equation}
\begin{equation}
y = \rho \cos\phi \sin\theta
\end{equation}
\begin{equation}
z = \rho \sin\phi
\end{equation}

\begin{equation}
J_{eq}
=
\begin{bmatrix}
    \frac{\partial{x}}{\partial{\theta}} & \frac{\partial{x}}{\partial{\phi}} & \frac{\partial{x}}{\partial{\rho}} \\
    \frac{\partial{y}}{\partial{\theta}} & \frac{\partial{y}}{\partial{\phi}} & \frac{\partial{y}}{\partial{\rho}} \\
    \frac{\partial{z}}{\partial{\theta}} & \frac{\partial{z}}{\partial{\phi}} & \frac{\partial{z}}{\partial{\rho}} \\
\end{bmatrix}
=
\begin{bmatrix}
    -\rho \cos{\phi}\sin{\theta} & -\rho\sin{\phi}\cos{\theta} & \cos{\phi}\cos{\theta} \\
    \rho\cos{\phi}\cos{\theta} & -\rho\sin{\phi}\cos{\theta} & \cos{\phi}\sin{\theta}  \\
    0 & \rho\cos{\phi} & \sin{\phi}  \\
\end{bmatrix}
\end{equation}


\begin{equation}
||J_{eq}|| = |\det(J_{eq})| = |\rho^2 \cos{\phi}| = \rho^2 \cos{\phi}
\end{equation}

Here, we apply the constraint $\rho=1$, converting to the final two-dimensional equirectangular projection space.

\begin{equation}
||J_{eq}|| = \cos{\phi}
\end{equation}

\susubsection{Equal Area Projection}

We can attempt to apply an additional simple transform to create a uniform Jacobian. Let our new projection space be based on spherical coordinates, in which case the Jacobians simply mulitiply. Since the equirectangular Jacobian is only a function of $\phi$, perhaps we only need to apply a simple transform to $\phi$.


\begin{equation}
\theta = a
\end{equation}

\begin{equation}
\phi = g(b)
\end{equation}

\begin{equation}
\rho = c
\end{equation}


\begin{equation}
J_{eq2u}
=
\begin{bmatrix}
    1 & 0 & 0 \\
    0 & \frac{\partial{g}}{\partial{b}} & 0 \\
    0 & 0 & 1 \\
\end{bmatrix}
\end{equation}



\begin{equation}
||J_{eq2u}||
= \frac{\partial{g}}{\partial{b}}
\end{equation}


\begin{equation}
||J_{u}|| = ||J_{eq2u}||||J_{eq}|| = \frac{\partial{g}}{\partial{b}} \cos{\phi}= \frac{\partial{g}}{\partial{b}} \cos{g(b)}
\end{equation}

Since we would like a constant $\alpha = ||J_u||$, we get a differential equation.

\begin{equation}
\alpha = \frac{\partial{g}}{\partial{b}} \cos{g(b)}
\end{equation}
\begin{equation}
g(b)  = \arccos{\left( \frac{\alpha}{\frac{\partial{g}}{\partial{b}}}\right )}
\end{equation}

We can solve this differential equation (or use Wolfram Alpha to solve it for us).

\begin{equation}
g(b)  = \arcsin{\left( \alpha b + C\right )}
\end{equation}

We now have an area preserving projection! Incidentally, this is called the Lambert Equal Area Projection, and has been used by cartographers for centuries. It is related to Archimedes' famous proof that a sphere has the same surface area as the cylinder that surrounds it.


\subsection{Cubic}
A mapping must be bijective in three-dimensions to create a valid Jacobian, so we must allow for cubes of any size, just as in calculating the Jacobian for equirectangular coordinates we kept $\rho$ as a variable until the last step. We will calculate the Jacobian for the front face ($x=\rho$) and then use arguments of symmetry for the remaining faces.

\begin{equation}
x = x_c \frac{|x_c|}{\sqrt{x_c^2 + y_c^2 + z_c^2}}
\end{equation}
\begin{equation}
y = y_c \frac{|x_c|}{\sqrt{x_c^2 + y_c^2 + z_c^2}}
\end{equation}
\begin{equation}
z = z_c \frac{|x_c|}{\sqrt{x_c^2 + y_c^2 + z_c^2}}
\end{equation}

Note how this is a projection in that a ray from the origin to a point $(x,y,z)$ travels through the cubic projection point ($x_c,y_c,z_c$). This can be verified by solving for the azimuth and elevation for both coordinates. Note also how the radius of the resulting projected point is $|x|$. This can be verified by calculating the magnitude of the resulting vector $(x,y,z)$.

For the front face, $x_c\geq 0$.

\begin{equation}
x = \frac{x_c^2}{\sqrt{x_c^2 + y_c^2 + z_c^2}}
\end{equation}
\begin{equation}
y = y_c \frac{x_c}{\sqrt{x_c^2 + y_c^2 + z_c^2}}
\end{equation}
\begin{equation}
z = z_c \frac{x_c}{\sqrt{x_c^2 + y_c^2 + z_c^2}}
\end{equation}

\begin{equation}
J_{c}
=
\begin{bmatrix}
    \frac{\partial{x}}{\partial{x_c}} & \frac{\partial{x}}{\partial{y_c}} & \frac{\partial{x}}{\partial{z_c}} \\
    \frac{\partial{y}}{\partial{x_c}} & \frac{\partial{y}}{\partial{y_c}} & \frac{\partial{y}}{\partial{z_c}} \\
    \frac{\partial{z}}{\partial{x_c}} & \frac{\partial{z}}{\partial{y_c}} & \frac{\partial{z}}{\partial{z_c}} \\
\end{bmatrix}
\end{equation}
\begin{equation}
J_{c}
=
\frac{1}{\sqrt{x_c^2 + y_c^2 + z_c^2}^{3}}
\begin{bmatrix}
    x_c(x_c^2 + 2y_c^2 + 2z_c^2) & -x_c^2y_c & -x_c^2z_c \\
    y_c(y_c^2+z_c^2) & x_c(x_c^2+z^2) & -x_cy_cz_c  \\
    z_c(y_c^2+z_c^2) & -x_cy_cz_c & x_c(x_c^2+y_c^2)  \\
\end{bmatrix}
\end{equation}


\begin{equation}
||J_{c}|| = \left ( \frac{|x_c|}{\sqrt{x_c^2+y_c^2+z_c^2}} \right )^3
\end{equation}

We now apply the constraint $x_c=\rho=1$.

\begin{equation}
||J_{c}||_{F,B} = (1+y_c^2+z_c^2)^{-\frac{3}{2}}
\end{equation}

Because only the magnitude of the determinant of the Jacobian matters for area distortion, it turns out that ths Jacobian is identical for opposite sides of the cube. So the Jacobian above is correct for the back side of the cube as well as the front side. The Jacobian for the remaining sides are found by simply replacing $y_c$ and $z_c$ in the above equation with the Cartesian directions that span the side. So, for the left and right sides, 

\begin{equation}
||J_{c}||_{L,R} = (1+x_c^2+z_c^2)^{-\frac{3}{2}}
\end{equation}

and for the up and down sides,

\begin{equation}
||J_{c}||_{U,D} = (1+x_c^2+y_c^2)^{-\frac{3}{2}}
\end{equation}

Unfortunately, an analytical solution to the double integral was not found. Fortunately, MATLAB's numerical integration methods are sufficient.

A two dimensional mapping has not actually been defined yet since all of $x_c,y_c,z_c$ appear in the Jacobians for the various sides. There are a few ways to represent the faces of the cube in a rectangular space and any are valid since rotations and translations do not affect the Jacobian. In our scripts, we chose a simple strip of the cube faces, in the order left, front, right, back, up, down (LFRBUD). Some simple bookkeeping keeps the piecewise Jacobian and coordinate transformations in order and still allows for integration over a two-dimensional space.

\section{Results}

\end{document}
