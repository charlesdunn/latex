\beamer@endinputifotherversion {3.36pt}
\beamer@sectionintoc {1}{Introduction}{3}{0}{1}
\beamer@sectionintoc {2}{Projections}{10}{0}{2}
\beamer@sectionintoc {3}{Resolution Functions}{16}{0}{3}
\beamer@sectionintoc {4}{Equirectangular Projection}{22}{0}{4}
\beamer@sectionintoc {5}{Cubic Projection}{29}{0}{5}
\beamer@sectionintoc {6}{Facebook's Pyramid}{36}{0}{6}
\beamer@sectionintoc {7}{Resolution-Defined Projections}{41}{0}{7}
\beamer@sectionintoc {8}{Example}{52}{0}{8}
\beamer@sectionintoc {9}{Results}{71}{0}{9}
\beamer@sectionintoc {10}{Summary}{78}{0}{10}
\beamer@sectionintoc {11}{Thank You!}{80}{0}{11}
