\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{mathtools}
\usepackage{esint}

\DeclareMathOperator*{\argmin}{arg\,min}

\title{360$^{\circ}$ Content Projections}
\author{charles dunn\\YouVisit}
\date{January 2016}

\begin{document}

\maketitle

Let the user's view be the origin. Let $a_0$ be the height of the pyramid. Let $x_0$ be the distance to the base of the pyramid from the origin. Let $\gamma$ be the horizontal field of view in degrees. Let $r$ be the aspect ratio. The numbers discovered through reverse engineering are $a_0=2$, $x_0 = 1$, $\gamma=120^{\circ}$, and $r = \frac{8}{7}$.

Let $(u,v)$ be the coordinates in projection space. The limits of the space are defined by the above parameters. Let $u_0$ and $v_0$ be the extent around the origin that $u$ and $v$ span, respectively.

\begin{equation}
    u_0 = \tan{\frac{\gamma}{2x_0}}
\end{equation}
\begin{equation}
    v_0 = \frac{1}{r}\tan{\frac{\gamma}{2x_0}}
\end{equation}

To solve the Jacobian for the "top left" side of the pyramid, we first shift, flip, and scale the $u,v$ coordinates.

\begin{equation}
    u_n = -\frac{u - u_0}{u_0}
\end{equation}
\begin{equation}
    v_n = -\frac{v - v_0}{v_0}
\end{equation}
\begin{equation}
    \frac{\partial{u_n}}{\partial{u}} = -\frac{1}{u_0}
\end{equation}
\begin{equation}
    \frac{\partial{v_n}}{\partial{v}} = -\frac{1}{v_0}
\end{equation}

The origin is now at the point that corresponds to the apex of the pyramid. The triangle spans [0 1] in both $u$ and $v$.

We now perform a change of basis so the unit vectors along the $u$ and $v$ axes become vectors along the sides of the pyramid. Let $b$ be the coordinates of the vertical corner of the base and let $c$ be the coordinates of the horizontal corner of the base. Let $a$ be the coordinates of the apex of the pyramid. Let coordinates in pyramid-side space be denoted by $\prescript{}{s}{}$. This space has its origin at the point normal to the cartesian origin. The coordinates orthogonally span the side of the pyramid.

This is an affine transformation with matrix $M$ and offset $\prescript{}{s}a$.

\begin{equation}
    M = \begin{bmatrix}
    \prescript{}{s}b_y - \prescript{}{s}a_y & \prescript{}{s}c_y - \prescript{}{s}a_y \\
    \prescript{}{s}b_z - \prescript{}{s}a_z & \prescript{}{s}c_z - \prescript{}{s}a_z \\
\end{bmatrix}
\end{equation}

\begin{equation}
    y_p = \left ( \prescript{}{s}b_y - \prescript{}{s}a_y \right ) u_n + \left ( \prescript{}{s}c_y - \prescript{}{s}a_y \right ) v_n + \prescript{}{s}a_y
\end{equation}
\begin{equation}
    z_p = \left ( \prescript{}{s}b_z - \prescript{}{s}a_z \right ) u_n + \left ( \prescript{}{s}c_z - \prescript{}{s}a_z \right ) v_n + \prescript{}{s}a_z
\end{equation}
\begin{equation}
    \frac{\partial{y_p}}{\partial{u}} = \left ( \prescript{}{s}b_y - \prescript{}{s}a_y \right )\frac{\partial{u_n}}{\partial{u}}
\end{equation}
\begin{equation}
    \frac{\partial{y_p}}{\partial{v}} = \left ( \prescript{}{s}c_y - \prescript{}{s}a_y \right )\frac{\partial{v_n}}{\partial{v}}
\end{equation}
\begin{equation}
    \frac{\partial{z_p}}{\partial{u}} =  \left ( \prescript{}{s}b_z - \prescript{}{s}a_z \right ) \frac{\partial{u_n}}{\partial{u}}
\end{equation}
\begin{equation}
    \frac{\partial{z_p}}{\partial{v}} = \left ( \prescript{}{s}c_z - \prescript{}{s}a_z \right )\frac{\partial{v_n}}{\partial{v}}
\end{equation}
\begin{equation}
    \frac{\partial{y_p}}{\partial{u}} = -\left ( \prescript{}{s}b_y - \prescript{}{s}a_y \right )\frac{1}{u_0}
\end{equation}
\begin{equation}
    \frac{\partial{y_p}}{\partial{v}} = -\left ( \prescript{}{s}c_y - \prescript{}{s}a_y \right )\frac{1}{v_0}
\end{equation}
\begin{equation}
    \frac{\partial{z_p}}{\partial{u}} =  -\left ( \prescript{}{s}b_z - \prescript{}{s}a_z \right ) \frac{1}{u_0}
\end{equation}
\begin{equation}
    \frac{\partial{z_p}}{\partial{v}} = -\left ( \prescript{}{s}c_z - \prescript{}{s}a_z \right )\frac{1}{v_0}
\end{equation}


Finally, we project from the surface of the pyramid to the unit sphere. Let $d$ be the distance from the origin to a side of the pyramid.

\begin{equation}
\begin{bmatrix}
    x \\
    y \\
    z \\
\end{bmatrix} = \frac{w}{\sqrt{d^2 + y_p^2 + z_p^2}}
\begin{bmatrix}
    d \\
    y_p \\
    z_p \\
\end{bmatrix}
\end{equation}

\begin{equation}
\frac{\partial{x}}{\partial{u}} = \frac{-wd}{\left ({d^2 + y_p^2 + z_p^2} \right) ^{3/2}} \left ( y_p \frac{\partial{y_p}}{\partial{u}} + z_p \frac{\partial{z_p}}{\partial{u}} \right) 
\end{equation}
\begin{equation}
\frac{\partial{x}}{\partial{v}} = \frac{-wd}{\left ({d^2 + y_p^2 + z_p^2} \right) ^{3/2}} \left ( y_p \frac{\partial{y_p}}{\partial{v}} + z_p \frac{\partial{z_p}}{\partial{v}} \right) 
\end{equation}
\begin{equation}
\frac{\partial{x}}{\partial{w}} = \frac{d}{\sqrt{d^2 + y_p^2 + z_p^2}}
\end{equation}
\begin{equation}
\frac{\partial{y}}{\partial{u}} = \frac{-wy_p}{\left ({d^2 + y_p^2 + z_p^2} \right) ^{3/2}} \left ( y_p \frac{\partial{y_p}}{\partial{u}} + z_p \frac{\partial{z_p}}{\partial{u}} \right) + \frac{w}{\sqrt{d^2 + y_p^2 + z_p^2}}\frac{\partial{y_p}}{\partial{u}}
\end{equation}
\begin{equation}
\frac{\partial{y}}{\partial{v}} = \frac{-wy_p}{\left ({d^2 + y_p^2 + z_p^2} \right) ^{3/2}} \left ( y_p \frac{\partial{y_p}}{\partial{v}} + z_p \frac{\partial{z_p}}{\partial{v}} \right) + \frac{w}{\sqrt{d^2 + y_p^2 + z_p^2}}\frac{\partial{y_p}}{\partial{v}}
\end{equation}
\begin{equation}
\frac{\partial{y}}{\partial{w}} = \frac{y_p}{\sqrt{d^2 + y_p^2 + z_p^2}}
\end{equation}\begin{equation}
\frac{\partial{y}}{\partial{u}} = \frac{-wz_p}{\left ({d^2 + y_p^2 + z_p^2} \right) ^{3/2}} \left ( y_p \frac{\partial{y_p}}{\partial{u}} + z_p \frac{\partial{z_p}}{\partial{u}} \right) + \frac{w}{\sqrt{d^2 + y_p^2 + z_p^2}}\frac{\partial{z_p}}{\partial{u}}
\end{equation}
\begin{equation}
\frac{\partial{z}}{\partial{v}} = \frac{-wz_p}{\left ({d^2 + y_p^2 + z_p^2} \right) ^{3/2}} \left ( y_p \frac{\partial{y_p}}{\partial{v}} + z_p \frac{\partial{z_p}}{\partial{v}} \right) + \frac{w}{\sqrt{d^2 + y_p^2 + z_p^2}}\frac{\partial{z_p}}{\partial{v}}
\end{equation}
\begin{equation}
\frac{\partial{z}}{\partial{w}} = \frac{z_p}{\sqrt{d^2 + y_p^2 + z_p^2}}
\end{equation}
\begin{equation}
r = \sqrt{d^2 + y_p^2 + z_p^2}
\end{equation}
\begin{equation}
\alpha = y_p \frac{\partial{y_p}}{\partial{u}} + z_p \frac{\partial{z_p}}{\partial{u}} 
\end{equation}
\begin{equation}
\beta = y_p \frac{\partial{y_p}}{\partial{v}} + z_p \frac{\partial{z_p}}{\partial{v}} 
\end{equation}


\begin{equation}
||J|| = \left | \det \begin{bmatrix}
    \frac{-wd}{r ^{3}} \alpha   & \frac{-wd}{r^{3}}  \beta  & \frac{d}{r}\\
    \frac{-wy_p}{r^{3}} \alpha + \frac{w}{r}\frac{\partial{y_p}}{\partial{u}} & \frac{-wy_p}{r^{3}} \beta + \frac{w}{r}\frac{\partial{y_p}}{\partial{v}} & \frac{y_p}{r}\\
    \frac{-wz_p}{r^{3}} \alpha + \frac{w}{r}\frac{\partial{z_p}}{\partial{u}} & \frac{-wz_p}{r^{3}} \beta + \frac{w}{r}\frac{\partial{z_p}}{\partial{v}} & \frac{z_p}{r}\\
\end{bmatrix} \right |
\end{equation}

\begin{equation}
||J|| = \left | \frac{dw^2}{r^7} \det \begin{bmatrix}
    {\alpha}   & {\beta }   & 1\\
    {y_p} \alpha - \frac{\partial{y_p}}{\partial{u}}r^2 & {y_p} \beta - \frac{\partial{y_p}}{\partial{v}}r^2 & {y_p}\\
    {z_p} \alpha - \frac{\partial{z_p}}{\partial{u}}r^2 & {z_p} \beta - \frac{\partial{z_p}}{\partial{v}}r^2 & {z_p}\\
\end{bmatrix} \right |
\end{equation}
\begin{multline}
||J|| = \left | \frac{dw^2}{r^7} \left (    {\alpha} \left ( \left ( {y_p} \beta - \frac{\partial{y_p}}{\partial{v}}r^2 \right ) \left ( {z_p} \right ) - \left(  {y_p}\right) \left({z_p} \beta - \frac{\partial{z_p}}{\partial{v}}r^2 \right) \right )  + \right.  \right.\\
{\beta } \left (  \left ( {y_p} \right )   \left ( {z_p} \alpha - \frac{\partial{z_p}}{\partial{u}}r^2 \right )   -    \left ( {y_p} \alpha - \frac{\partial{y_p}}{\partial{u}}r^2 \right )  \left ( {z_p} \right )  \right )  + \\
\left . \left. \left ( {y_p} \alpha - \frac{\partial{y_p}}{\partial{u}}r^2 \right )\left ( {z_p} \beta - \frac{\partial{z_p}}{\partial{v}}r^2 \right ) - 
\left ( {y_p} \beta - \frac{\partial{y_p}}{\partial{v}}r^2 \right ) \left ( {z_p} \alpha - \frac{\partial{z_p}}{\partial{u}}r^2 \right ) \right ) \right |
\end{multline}
\begin{multline}
||J|| = \left | \frac{dw^2}{r^7} \left (    {\alpha} \left ( z_py_p \beta - z_p\frac{\partial{y_p}}{\partial{v}}r^2   - y_pz_p \beta + y_p\frac{\partial{z_p}}{\partial{v}}r^2 \right)   + \right.  \right.\\
{\beta } \left (   y_pz_p \alpha - y_p\frac{\partial{z_p}}{\partial{u}}r^2  -   z_p{y_p} \alpha +z_p \frac{\partial{y_p}}{\partial{u}}r^2 \right )  + \\
\left ( {y_p} \alpha {z_p} \beta + \frac{\partial{z_p}}{\partial{v}}r^2 \frac{\partial{y_p}}{\partial{u}}r^2 - \frac{\partial{y_p}}{\partial{u}}r^2 {z_p} \beta - {y_p} \alpha \frac{\partial{z_p}}{\partial{v}}r^2 \right ) - \\
\left . \left. \left ( {y_p} \beta  {z_p} \alpha + \frac{\partial{z_p}}{\partial{u}}r^2 \frac{\partial{y_p}}{\partial{v}}r^2 - \frac{\partial{y_p}}{\partial{v}}r^2 {z_p} \alpha - {y_p} \beta \frac{\partial{z_p}}{\partial{u}}r^2 \right ) \right ) \right |
\end{multline}

\begin{multline}
||J|| = \left | \frac{dw^2}{r^7} \left (    {\alpha} r^2 \left ( y_p\frac{\partial{z_p}}{\partial{v}} - z_p\frac{\partial{y_p}}{\partial{v}}  \right)   + \right.  \right.\\
{\beta }r^2 \left (  z_p \frac{\partial{y_p}}{\partial{u}} - y_p\frac{\partial{z_p}}{\partial{u}} \right )  + \\
\left ( \frac{\partial{z_p}}{\partial{v}}\frac{\partial{y_p}}{\partial{u}}r^4 - \frac{\partial{y_p}}{\partial{u}}r^2 {z_p} \beta - {y_p} \alpha \frac{\partial{z_p}}{\partial{v}}r^2 \right ) - \\
\left . \left. \left ( \frac{\partial{z_p}}{\partial{u}} \frac{\partial{y_p}}{\partial{v}}r^4 - \frac{\partial{y_p}}{\partial{v}}r^2 {z_p} \alpha - {y_p} \beta \frac{\partial{z_p}}{\partial{u}}r^2 \right ) \right ) \right |
\end{multline}

\begin{multline}
||J|| = \left | \frac{dw^2}{r^5} \left (     y_p\alpha\frac{\partial{z_p}}{\partial{v}} - z_p\alpha\frac{\partial{y_p}}{\partial{v}}   + 
z_p\beta  \frac{\partial{y_p}}{\partial{u}} - y_p\beta \frac{\partial{z_p}}{\partial{u}}  + \right.  \right.\\
 \frac{\partial{z_p}}{\partial{v}}\frac{\partial{y_p}}{\partial{u}}r^2 - {z_p} \beta\frac{\partial{y_p}}{\partial{u}}  - {y_p} \alpha \frac{\partial{z_p}}{\partial{v}} - \\
\left . \left. \frac{\partial{z_p}}{\partial{u}} \frac{\partial{y_p}}{\partial{v}}r^2 + {z_p} \alpha \frac{\partial{y_p}}{\partial{v}}  + {y_p} \beta \frac{\partial{z_p}}{\partial{u}}  \right ) \right |
\end{multline}

\begin{equation}
||J|| = \left | \frac{dw^2}{r^5} \left (\frac{\partial{z_p}}{\partial{v}}\frac{\partial{y_p}}{\partial{u}}r^2   -
 \frac{\partial{z_p}}{\partial{u}} \frac{\partial{y_p}}{\partial{v}}r^2  \right ) \right |
\end{equation}
\begin{equation}
||J|| = \left | \frac{dw^2}{r^3} \left (\frac{\partial{z_p}}{\partial{v}}\frac{\partial{y_p}}{\partial{u}}   -
 \frac{\partial{z_p}}{\partial{u}} \frac{\partial{y_p}}{\partial{v}}  \right ) \right |
\end{equation}
\begin{equation}
||J|| = \left | \frac{dw^2}{r^3} \left (\frac { \prescript{}{s}c_z - \prescript{}{s}a_z }{v_0}\frac{ \prescript{}{s}b_y - \prescript{}{s}a_y }{u_0}   -
\frac { \prescript{}{s}b_z - \prescript{}{s}a_z }{u_0} \frac { \prescript{}{s}c_y - \prescript{}{s}a_y }{v_0} \right ) \right |
\end{equation}
\begin{equation}
||J|| = \left | \frac{dw^2}{u_0v_0r^3} \left ( \left ( \prescript{}{s}c_z - \prescript{}{s}a_z \right )  \left ( \prescript{}{s}b_y - \prescript{}{s}a_y   \right ) -\left ( 
 \prescript{}{s}b_z - \prescript{}{s}a_z\right )  \left (  \prescript{}{s}c_y - \prescript{}{s}a_y \right )\right ) \right |
\end{equation}
\begin{equation}
||J|| = \left | \frac{dw^2\det M}{u_0v_0} \right | r^{-3}
\end{equation}


We don't need to worry about this rotation since it does not affect the Jacobian.

We now rotate the plane so it is coplanar with the actual pyramid side in cartesian space. We rotate first in elevation, and then in azimuth. $\theta$ and $\phi$ are the azimuth and elevation angles of the vector normal to this side of the pyramid from the origin.

\begin{equation}
    R = \begin{bmatrix}
    \cos{\theta} & -\sin{\theta} & 0 \\
    \sin{\theta} & \cos{\theta} & 0 \\
    0 & 0 & 1 \\
\end{bmatrix}\begin{bmatrix}
    \cos{\phi} & 0 & \sin{\phi}\\
    -\sin{\phi} & 0 & \cos{\phi}\\
    0 & 1 & 0 \\
\end{bmatrix}
\end{equation}
\begin{equation}
    R = \begin{bmatrix}
    \cos{\theta}\cos{\phi} + \sin{\theta}\sin{\phi} & 0 & \cos{\theta}\sin{\phi} - \sin{\theta}\cos{\phi} \\
    \sin{\theta}\cos{\phi} - \cos{\theta}\sin{\phi}  & 0 & \sin{\theta}\sin{\phi} + \cos{\theta}\cos{\phi} \\
    0 & 1 & 0 \\
\end{bmatrix}
\end{equation}

Therefore, the final cartesian coordinates on the pyramid before projection to the unit sphere can be calculated.

\begin{equation}
\begin{bmatrix}
    x_p \\
    y_p \\
    z_p \\
\end{bmatrix}
 = R 
\begin{bmatrix}
    w_s \\
    u_s \\
    v_s \\
\end{bmatrix}
\end{equation}

\begin{equation}
    \frac{\partial{x_p}}{\partial{u}} = \left ( \cos{\theta}\sin{\phi} - \sin{\theta}\cos{\phi} \right )\frac{\partial{v_s}}{\partial{u}}
\end{equation}
\begin{equation}
    \frac{\partial{x_p}}{\partial{v}} = \left ( \cos{\theta}\sin{\phi} - \sin{\theta}\cos{\phi} \right )\frac{\partial{v_s}}{\partial{v}}
\end{equation}
\begin{equation}
    \frac{\partial{y_p}}{\partial{u}} = \left ( \sin{\theta}\sin{\phi} + \cos{\theta}\cos{\phi} \right )\frac{\partial{v_s}}{\partial{u}}
\end{equation}
\begin{equation}
    \frac{\partial{y_p}}{\partial{v}} = \left ( \sin{\theta}\sin{\phi} + \cos{\theta}\cos{\phi} \right )\frac{\partial{v_s}}{\partial{v}}
\end{equation}
\begin{equation}
    \frac{\partial{z_p}}{\partial{u}} = \frac{\partial{u_s}}{\partial{u}}
\end{equation}
\begin{equation}
    \frac{\partial{z_p}}{\partial{v}} = \frac{\partial{u_s}}{\partial{v}}
\end{equation}

\end{document}
