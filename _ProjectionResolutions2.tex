\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{mathtools}
\usepackage{esint}
\usepackage{tikz}
\usepackage{float}
\usepackage{bm}
\usetikzlibrary{matrix,arrows,calc}
\usepackage{graphicx}
\usepackage{breqn}
\usepackage[margin=0.5in]{geometry}

\usepackage{array}
\usepackage{calc}

\newlength\celldim \newlength\fontheight \newlength\extraheight
\newcounter{sqcolumns}
\newcolumntype{S}{ @{}
>{\centering \rule[-0.5\extraheight]{0pt}{\fontheight + \extraheight}}
p{\celldim} @{} }
\newcolumntype{Z}{ @{} >{\centering} p{\celldim} @{} }
\newenvironment{squarecells}[1]
{\setlength\celldim{2em}%
\settoheight\fontheight{A}%
\setlength\extraheight{\celldim - \fontheight}%
\setcounter{sqcolumns}{#1 - 1}%
\begin{tabular}{|S|*{\value{sqcolumns}}{Z|}}\hline}
% squarecells tabular goes here
{\end{tabular}}

\newcommand\nl{\tabularnewline\hline}


\newcounter{sarrow}
\newcommand\xrsquigarrow[1]{%
\stepcounter{sarrow}%
\begin{tikzpicture}[decoration=snake]
\node (\thesarrow) {\strut#1};
\draw[->,decorate] (\thesarrow.south west) -- (\thesarrow.south east);
\end{tikzpicture}%
}


\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator\erf{erf}
\DeclareMathOperator\erfinv{erf^{-1}}

\DeclareMathOperator*{\argmin}{arg\,min}

\title{Projection Resolutions}
\author{charles dunn\\YouVisit}
\date{\today}

\begin{document}

\maketitle

\section{Notation}

A sphere of content is in $(x,y,z)$ Cartesian space.
\begin{equation}
x^2 + y^2 + z^2 = r^2
\end{equation}
The forward direction is $(1,0,0)$.

The following are all projections of the sphere to a two-dimensional space spanned by $(u,v)$ with $r$ being the radius of the circle. To calculate the resolution, we use the following formula.

\begin{equation}
r(u,v,r)  = \frac{N_uN_v}{|A_P| ||J(u,v,r)|| }
\end{equation}

We use the standard formula for the Jacobian of a projection.

\begin{equation}
J(u,v,r) = \left . \begin{bmatrix}
    \frac{\partial{x}}{\partial{u}} & \frac{\partial{x}}{\partial{v}} & \frac{\partial{x}}{\partial{r}} \\    \frac{\partial{y}}{\partial{u}} & \frac{\partial{y}}{\partial{v}} & \frac{\partial{y}}{\partial{r}} \\    \frac{\partial{z}}{\partial{u}} & \frac{\partial{z}}{\partial{v}} & \frac{\partial{z}}{\partial{r}} \\
\end{bmatrix}\right |_{(u,v,r)} \\
\end{equation}

Note that the $\arctan$ function refers to the quadrant-aware function, usually represented by $\text{atan2}$.

\section{Spherical Coordinates}


\begin{equation}
\theta = \arctan {\frac{y}{x}}
\end{equation}
\begin{equation}
\phi = \arcsin{\frac{z}{\sqrt{x^2 +y^2 +z^2}}}
\end{equation}
\begin{equation}
\rho = \sqrt{x^2 + y^2 + z^2}
\end{equation}

\begin{equation}
x = \rho \cos\phi \cos\theta
\end{equation}
\begin{equation}
y = \rho \cos\phi \sin\theta
\end{equation}
\begin{equation}
z = \rho \sin\phi
\end{equation}

\section{Equirectangular}

\begin{equation}
u \in [\pi, -\pi)
\end{equation}
\begin{equation}
v \in [\frac{\pi}{2}, -\frac{\pi}{2}]
\end{equation}
\begin{equation}
w \in (0,\infty)
\end{equation}

\begin{equation}
u = \arctan \left ({\frac{y}{x}}\right )
\end{equation}
\begin{equation}
v = \arcsin \left ( {\frac{z}{\sqrt{x^2 + y^2 + z^2}}}\right)
\end{equation}
\begin{equation}
w = \sqrt{x^2 + y^2 + z^2}
\end{equation}

\begin{equation}
x =w\cos(u) \cos(v) 
\end{equation}
\begin{equation}
y = w \sin(u)\cos(v)
\end{equation}
\begin{equation}
z = w\sin(v)
\end{equation}

\begin{equation}
J(u,v,w) = 
\begin{bmatrix}
    -w\sin(u)\cos(v) & -w\cos(u) \sin(v)& \cos(u)\cos(v) \\   w\cos(u)\cos(v) & -w\sin(u)\sin(v) & \sin(u)\cos(v) \\   0 & w\cos(v) & \sin(v) \\
\end{bmatrix}
\end{equation}
\begin{equation}
||J_u(u,v,w)||  = w\cos(v)
\end{equation}
\begin{equation}
||J_v(u,v,w)||  = w
\end{equation}
\begin{equation}
||J(u,v,w)||  = w^2 \cos(v)
\end{equation}
\begin{equation}
r_u(u,v,w)  = \frac{N_u}{2\pi w \cos(v)}
\end{equation}
\begin{equation}
r_v(u,v,w)  = \frac{N_v}{\pi w}
\end{equation}
\begin{equation}
r(u,v,w)  = \frac{N_uN_v}{2\pi^2 w^2 \cos(v)}
\end{equation}
\begin{equation}
r_u(x,y,z)  = \frac{N_u}{2\pi \sqrt{x^2 + y^2}}
\end{equation}
\begin{equation}
r_v(x,y,z)  = \frac{N_v}{\pi \sqrt{x^2 + y^2 + z^2}}
\end{equation}
\begin{equation}
r(x,y,z)  = \frac{N_uN_v}{2\pi^2 \sqrt{x^2 + y^2 + z^2} \sqrt{x^2 + y^2}}
\end{equation}
\begin{equation}
r_u(\theta,\phi,\rho)  = \frac{N_u}{2\pi \rho \cos(\phi)}
\end{equation}
\begin{equation}
r_v(\theta,\phi,\rho)  = \frac{N_v}{\pi \rho}
\end{equation}
\begin{equation}
r(\theta,\phi,\rho)  = \frac{N_uN_v}{2\pi^2 \rho^2 \cos(\phi)}
\end{equation}

\section{Lambert}

\begin{equation}
u \in [\pi, -\pi)
\end{equation}
\begin{equation}
v \in [1, -1]
\end{equation}
\begin{equation}
w \in (0,\infty)
\end{equation}

\begin{equation}
u = \arctan \left ({\frac{y}{x}}\right )
\end{equation}
\begin{equation}
v = \frac{z}{\sqrt{x^2 + y^2 + z^2}}
\end{equation}
\begin{equation}
w = \sqrt{x^2 + y^2 + z^2}
\end{equation}

\begin{equation}
x =w\cos(u) \cos(\arcsin(v)) 
\end{equation}
\begin{equation}
y = w\sin(u)\cos(\arcsin(v))
\end{equation}
\begin{equation}
z = w v
\end{equation}

\begin{equation}
J(u,v,w) = 
\begin{bmatrix}
    -w\sin(u)\cos(\arcsin(v)) & -\frac{w\cos(u)v}{\sqrt{(1 - v^2)}} & \cos(u)\cos(\arcsin(v)) \\   w\cos(u)\cos(\arcsin(v)) & -\frac{w\sin(u)v}{\sqrt{(1 - v^2)}} & \sin(u)\cos(\arcsin(v)) \\   0 & w & v \\
\end{bmatrix}
\end{equation}
\begin{equation}
||J_u(u,v,w)||  = w\sqrt{1 - v^2}
\end{equation}
\begin{equation}
||J_v(u,v,w)||  = \frac{w}{\sqrt{1 - v^2}}
\end{equation}
\begin{equation}
||J(u,v,w)||  = w^2
\end{equation}
\begin{equation}
r_u(u,v,w)  = \frac{N_u}{2\pi w\sqrt{1 - v^2}}
\end{equation}
\begin{equation}
r_v(u,v,w)  = \frac{N_v\sqrt{1 - v^2}}{2 w}
\end{equation}
\begin{equation}
r(u,v,w)  = \frac{N_uN_v}{4\pi w^2}
\end{equation}
\begin{equation}
r_u(x,y,z)  = \frac{N_u}{2\pi \sqrt{x^2 + y^2}}
\end{equation}
\begin{equation}
r_v(x,y,z)  = \frac{N_v\sqrt{x^2 + y^2}}{2 (x^2 + y^2 + z^2)}
\end{equation}
\begin{equation}
r(x,y,z)  = \frac{N_uN_v}{4\pi (x^2 + y^2 + z^2)}
\end{equation}
\begin{equation}
r_u(\theta,\phi,\rho)  = \frac{N_u}{2\pi \rho \cos(\phi)}
\end{equation}
\begin{equation}
r_v(\theta,\phi,\rho)  = \frac{N_v \cos(\phi)}{2 \rho}
\end{equation}
\begin{equation}
r(\theta,\phi,\rho)  = \frac{N_uN_v}{4\pi \rho^2}
\end{equation}

\section{Planar}

The projection of a sphere on to an infinite only maps to a hemisphere, but is useful in our later calculations.

We will define our plane by an orthonormal basis. $\hat{\bm{n}}$ is the unit vector normal to the plane. The two-dimensional plane is spanned by normalized vectors $\hat{\bm{a}}$ and $\hat{\bm{b}}$ which are also provided to avoid ambiguity in the plane coordinates, but don't impact the final resolution calculations. We enforce a right-handed coordinate system, so $\bm{a}\times \bm{b} = \bm{c}$.

\begin{equation}
a \in \left(\infty, -\infty\right)
\end{equation}
\begin{equation}
b \in \left(\infty, -\infty\right)
\end{equation}
\begin{equation}
c \in \left(0, \infty\right)
\end{equation}

Here, we project the $(x,y,z)$ vector $\bm{v}$ on to the plane, and then calculate the $a$ and $b$ coordinates of this new point on the plane.

\begin{equation}
a = \left (\frac{\bm{v}}{\bm{v}\cdot\hat{\bm{n}}} - ||\bm{v}||\hat{\bm{n}} \right )\cdot \hat{\bm{a}}
\end{equation}

\begin{equation}
b = \left (\frac{\bm{v}}{\bm{v}\cdot\hat{\bm{n}}} - ||\bm{v}||\hat{\bm{n}} \right )\cdot \hat{\bm{b}}
\end{equation}
\begin{equation}
c = ||\bm{v}||
\end{equation}

\begin{dmath}
a = \left (\frac{x}{x\hat{n}_x +  y\hat{n}_y +z\hat{n}_z} - \sqrt{x^2 + y^2 + z^2}\hat{n}_x \right ) \hat{a}_x + \left (\frac{y}{x\hat{n}_x +  y\hat{n}_y +z\hat{n}_z} - \sqrt{x^2 + y^2 + z^2}\hat{n}_y \right ) \hat{a}_y + \left ( \frac{z}{x\hat{n}_x +  y\hat{n}_y +z\hat{n}_z} - \sqrt{x^2 + y^2 + z^2}\hat{n}_z\right ) \hat{a}_z
\end{dmath}

\begin{dmath}
b = \left (\frac{x}{x\hat{n}_x +  y\hat{n}_y +z\hat{n}_z} - \sqrt{x^2 + y^2 + z^2}\hat{n}_x \right ) \hat{b}_x + \left (\frac{y}{x\hat{n}_x +  y\hat{n}_y +z\hat{n}_z} - \sqrt{x^2 + y^2 + z^2}\hat{n}_y \right ) \hat{b}_y + \left ( \frac{z}{x\hat{n}_x +  y\hat{n}_y +z\hat{n}_z} - \sqrt{x^2 + y^2 + z^2}\hat{n}_z\right ) \hat{b}_z
\end{dmath}
\begin{equation}
c = \sqrt{x^2 + y^2 + z^2}
\end{equation}

\begin{equation}
x = \frac{c(a\hat{a}_x + b\hat{b}_x + c\hat{n}_x)}{\sqrt{a^2 + b^2 + c^2}}
\end{equation}
\begin{equation}
y = \frac{c(a\hat{a}_y + b\hat{b}_y + c\hat{n}_y)}{\sqrt{a^2 + b^2 + c^2}}
\end{equation}
\begin{equation}
z = \frac{c(a\hat{a}_z + b\hat{b}_z + c\hat{n}_z)}{\sqrt{a^2 + b^2 + c^2}}
\end{equation}

\begin{dmath}
J(a,b,c) = \frac{1}{(a^2 + b^2 + c^2)^{3/2}}
\begin{bmatrix}
    c(\hat{a}_x(b^2 + c^2) - a(b\hat{b}_x + c\hat{c}_x)) &   c(\hat{b}_x(a^2 + c^2) - b(a\hat{a}_x + c\hat{c}_x)) &  (a\hat{a}_x + b \hat{b}_x + c \hat{c}_x)(a^2 + b^2)   \\   c(\hat{a}_y(b^2 + c^2) - a(b\hat{b}_y + c\hat{c}_y)) &  c(\hat{b}_y(a^2 + c^2) - b(a\hat{a}_y + c\hat{c}_y)) & (a\hat{a}_y + b \hat{b}_y + c \hat{c}_y)(a^2 + b^2 )  \\  c(\hat{a}_z(b^2 + c^2) - a(b\hat{b}_z + c\hat{c}_z))  &  c(\hat{b}_z(a^2 + c^2) - b(a\hat{a}_z + c\hat{c}_z)) & (a\hat{a}_z + b \hat{b}_z + c \hat{c}_z)(a^2 + b^2)   \\
\end{bmatrix}
\end{dmath}

\begin{equation}
||J_a(a,b,c)||  =\frac{c\sqrt{ (b^2 + c^2)}}{a^2 + b^2 + c^2} 
\end{equation}
\begin{equation}
||J_b(a,b,c)||  =\frac{c\sqrt{ (a^2 + c^2)}}{a^2 + b^2 + c^2} 
\end{equation}
\begin{equation}
||J(a,b,c)|| = \frac{\left|c^3( (a^2 + b^2 + c^2)^2 - c^2(a^2 +  b^2))\right |}{(a^2 + b^2 + c^2)^{7/2}} 
\end{equation}

\begin{equation}
r_a(a,b,c)  = 
\end{equation}
\begin{equation}
r_b(a,b,c)  = 
\end{equation}
\begin{equation}
r(a,b,c)  \propto  \frac{(a^2 + b^2 + c^2)^{7/2}}{\left|c^3( (a^2 + b^2 + c^2)^2 - c^2(a^2 +  b^2))\right |} 
\end{equation}
\begin{equation}
r(a,b,c)  \propto  \frac{(a^2 + b^2 + c^2)^{7/2}}{\left|c^3( (a^2 + b^2 + c^2)^2 - c^2(a^2 +  b^2))\right |} 
\end{equation}


\section{Front Cubic Face}

We will derive the projection for the forward face of a cubic projection.

\begin{equation}
a \in \left[a_0,-a_0\right]
\end{equation}
\begin{equation}
b \in \left[b_0, -b_0\right]
\end{equation}
\begin{equation}
c \in \left(0, \infty\right)
\end{equation}

\begin{equation}
a = \frac{y\sqrt{x^2 + y^2 + z^2}}{x}
\end{equation}
\begin{equation}
b = \frac{z\sqrt{x^2 + y^2 + z^2}}{x}
\end{equation}
\begin{equation}
c = \sqrt{x^2 + y^2 + z^2}
\end{equation}

\begin{equation}
x = \frac{c^2}{\sqrt{a^2 + b^2 + c^2}}
\end{equation}
\begin{equation}
y = \frac{ac}{\sqrt{a^2 + b^2 + c^2}}
\end{equation}
\begin{equation}
z = \frac{bc}{\sqrt{a^2 + b^2 + c^2}}
\end{equation}

\begin{equation}
J(a,b,c) = 
\begin{bmatrix}
    -\frac{ac^2}{(a^2 + b^2 + c^2)^{3/2}} &  -\frac{bc^2}{(a^2 + b^2 + c^2)^{3/2}} &  \frac{c(2a^2 + 2b^2 + c^2)}{(a^2 + b^2 + c^2)^{3/2}} \\  \frac{c (b^2 + c^2)}{(a^2 + b^2 + c^2)^{3/2}} & -\frac{abc}{(a^2 + b^2 + c^2)^{3/2}} & \frac{a (a^2 + b^2)}{(a^2 + b^2 + c^2)^{3/2}} \\    -\frac{abc}{(a^2 + b^2 + c^2)^{3/2}}  &\frac{c (a^2 + c^2)}{(a^2 + b^2 + c^2)^{3/2}}& \frac{b (a^2 + b^2)}{(a^2 + b^2 + c^2)^{3/2}} \\
\end{bmatrix}
\end{equation}
\begin{equation}
||J_a(a,b,c)||  = \frac{c\sqrt{b^2 + c^2}}{a^2 + b^2 + c^2}
\end{equation}
\begin{equation}
||J_b(a,b,c)||  = \frac{c\sqrt{a^2 + c^2}}{a^2 + b^2 + c^2}
\end{equation}
\begin{equation}
||J_c(a,b,c)||  = \frac{\sqrt{c^2(2(a^2 + b^2)^2 + c^2) + (a^2 + b^2)^3}}{(a^2 + b^2 + c^2)^{3/2}}
\end{equation}
\begin{equation}
||J(a,b,c)||  = \frac{c^3}{(a^2 + b^2 + c^2)^{3/2}}
\end{equation}
\begin{equation}
r_a(a,b,c)  = \frac{N_a(a^2 + b^2 + c^2)}{2a_0c\sqrt{b^2 + c^2}}
\end{equation}
\begin{equation}
r_b(a,b,c)  = \frac{N_b(a^2 + b^2 + c^2)}{2b_0c\sqrt{a^2 + c^2}}
\end{equation}
\begin{equation}
r(a,b,c)  = \frac{N_aN_b}{4c^5\tan(\theta_a)\tan(\theta_b)} (a^2 + b^2 + c^2)^{3/2}
\end{equation}
\begin{equation}
a = \frac{y\sqrt{x^2 + y^2 + z^2}}{x}
\end{equation}
\begin{equation}
b = \frac{z\sqrt{x^2 + y^2 + z^2}}{x}
\end{equation}
\begin{equation}
c = \sqrt{x^2 + y^2 + z^2}
\end{equation}
\begin{equation}
r(x,y,z)  = \frac{N_aN_b}{4\tan(\theta_a)\tan(\theta_b)x^3}\sqrt{x^2 +y^2 + z^2}
\end{equation}
\begin{equation}
r(\theta,\phi,\rho)  = \frac{N_aN_b}{4\tan(\theta_a)\tan(\theta_b)\rho^2\cos(\phi)^3\cos(\theta)^3}
\end{equation}



\section{Cubic}

By symmetry, we can leverage the result for the front face of a cube to find the characteristic functions of the full cubic projection. Here, we apply a suboptimal arrangement of faces, but any arrangement should yield the same results in terms of resolutions and Jacobians.

\begin{center}
\begin{squarecells}{3}
    \hline
    \rotatebox[origin=c]{270}{R}&  U \nl
    \rotatebox[origin=c]{270}{B} &  F \nl
    \rotatebox[origin=c]{270}{L} & D \nl
\end{squarecells}
\end{center}

\begin{equation}
\alpha = \arctan \left ({\frac{{\sin\mu_\theta \cos\mu_\phi \cos\theta\cos\phi  + \cos\mu_\theta  \sin\theta \cos\phi -\sin\mu_\theta\sin\mu_\phi   \sin\phi}}{{\cos\mu_\theta \cos\mu_\phi \cos\theta\cos\phi  -\sin\mu_\theta  \sin\theta \cos\phi -\cos\mu_\theta\sin\mu_\phi \sin\phi}}}\right )
\end{equation}
\begin{equation}
\beta = \arcsin \left ( {{\sin\mu_\phi \cos\theta\cos\phi   + \cos\mu_\phi \sin\phi}} \right )
\end{equation}
\begin{equation}
\gamma = \rho
\end{equation}

if $\mu_\phi=0$.
\begin{equation}
\alpha = \theta - \mu_\theta
\end{equation}
\begin{equation}
\beta = \phi
\end{equation}
\begin{equation}
\gamma = \rho
\end{equation}

if $\mu_\theta=0$.
\begin{equation}
\alpha = \arctan \left ({\frac{{\sin\theta \cos\phi}}{{ \cos\mu_\phi \cos\theta\cos\phi - \sin\mu_\phi \sin\phi}}}\right )
\end{equation}
\begin{equation}
\beta = \arcsin \left ( {{\sin\mu_\phi \cos\theta\cos\phi   + \cos\mu_\phi \sin\phi}} \right )
\end{equation}
\begin{equation}
\gamma = \rho
\end{equation}

\begin{equation}
u \in [1, -1]
\end{equation}
\begin{equation}
v \in \left [\frac{3}{2}, -\frac{3}{2}\right ]
\end{equation}
\begin{equation}
w \in (0, \infty)
\end{equation}

\begin{equation}
a(u,v,w) =  \left\{
  \begin{array}{lr}
   u + \frac{1}{2} : u\leq 0 \\
   v - \lfloor v \rceil + \frac{1}{2} : u> 0 \\
  \end{array}
\right.
\end{equation}

\begin{equation}
b(u,v,w) =  \left\{
  \begin{array}{lr}
   v  - \lfloor v \rceil + \frac{1}{2} : u\leq 0 \\
   \frac{1}{2} - u : u> 0 \\
  \end{array}
\right.
\end{equation}

\begin{equation}
c(u,v,w) = w
\end{equation}

\begin{equation}
N_a(u,v,w) =  \left\{
  \begin{array}{lr}
   \frac{N_u}{2} : u\leq 0 \\
   \frac{N_v}{3} : u> 0 \\
  \end{array}
\right.
\end{equation}

\begin{equation}
N_b(u,v,w) =  \left\{
  \begin{array}{lr}
   \frac{N_v}{3} : u\leq 0 \\
   \frac{N_u}{2} : u> 0 \\
  \end{array}
\right.
\end{equation}

\begin{equation}
||J_u(u,v,w)||  =  \left\{
  \begin{array}{lr}
   \frac{w\sqrt{(v  - \lfloor v \rceil + \frac{1}{2})^2 + w^2}}{(u + \frac{1}{2})^2 + (v  - \lfloor v \rceil + \frac{1}{2})^2 + w^2} : u\leq 0 \\
   \frac{w\sqrt{(\frac{1}{2} - u)^2 + w^2}}{(v - \lfloor v \rceil + \frac{1}{2})^2 + (\frac{1}{2} - u)^2 + w^2} : u> 0 \\
  \end{array}
\right.
\end{equation}

\begin{equation}
||J_v(u,v,w)||  =  \left\{
  \begin{array}{lr}
  \frac{w\sqrt{(u + \frac{1}{2})^2 + w^2}}{(u + \frac{1}{2})^2 + (v  - \lfloor v \rceil + \frac{1}{2})^2 + w^2} : u\leq 0 \\
  \frac{w\sqrt{(v - \lfloor v \rceil + \frac{1}{2})^2 + w^2}}{(v - \lfloor v \rceil + \frac{1}{2})^2 + (\frac{1}{2} - u)^2 + w^2} : u> 0 \\
  \end{array}
\right.
\end{equation}

\begin{equation}
||J_w(u,v,w)||  =  \left\{
  \begin{array}{lr}
  \frac{\sqrt{c^2(2((u + \frac{1}{2})^2 + (v  - \lfloor v \rceil + \frac{1}{2})^2)^2 + c^2) + ((u + \frac{1}{2})^2 + (v  - \lfloor v \rceil + \frac{1}{2})^2)^3}}{((u + \frac{1}{2})^2 + (v  - \lfloor v \rceil + \frac{1}{2})^2 + c^2)^{3/2}} : u\leq 0 \\
  \frac{\sqrt{w^2(2((v - \lfloor v \rceil + \frac{1}{2})^2 + (\frac{1}{2} - u)^2)^2 + w^2) + ((v - \lfloor v \rceil + \frac{1}{2})^2 + (\frac{1}{2} - u)^2)^3}}{((v - \lfloor v \rceil + \frac{1}{2})^2 + (\frac{1}{2} - u)^2 + w^2)^{3/2}} : u> 0 \\
  \end{array}
\right.
\end{equation}

\begin{equation}
r_u(u,v,w)  =  \left\{
  \begin{array}{lr}
   \frac{N_u}{2}\frac{(u + \frac{1}{2})^2 + (v  - \lfloor v \rceil + \frac{1}{2})^2 + w^2}{w\sqrt{(v  - \lfloor v \rceil + \frac{1}{2})^2 + w^2}} : u\leq 0 \\
   \frac{N_v}{3}\frac{(v - \lfloor v \rceil + \frac{1}{2})^2 + (\frac{1}{2} - u)^2 + w^2}{w\sqrt{(\frac{1}{2} - u)^2 + w^2}} : u> 0 \\
  \end{array}
\right.
\end{equation}

\begin{equation}
r_v(u,v,w) =  \left\{
  \begin{array}{lr}\frac{N_v}{3}
  \frac{(u + \frac{1}{2})^2 + (v  - \lfloor v \rceil + \frac{1}{2})^2 + w^2}{w\sqrt{(u + \frac{1}{2})^2 + w^2}} : u\leq 0 \\
  \frac{N_u}{2}\frac{(v - \lfloor v \rceil + \frac{1}{2})^2 + (\frac{1}{2} - u)^2 + w^2}{w\sqrt{(v - \lfloor v \rceil + \frac{1}{2})^2 + w^2}} : u> 0 \\
  \end{array}
\right.
\end{equation}

\begin{equation}
r(u,v,w)  =  
  \frac{N_uN_v}{6}\left (1 +\frac{ (v - \lfloor v \rceil + \frac{1}{2})^2 + (\frac{1}{2} - |u|)^2}{w^2}\right )^{3/2}
\end{equation}

\section{Scalable YouVisit Projections}

Scalable YouVisit Projections are based on a central projection which is a separable projection that applies on top of the equirectangular projection. There are also caps at the top and bottom of the image to avoid regions with large distortion.

A Scalable YouVisit projection is defined by an encoder blocksize, a minimum width, a minimum height, a pixel overlap, and the central projection. 

$B$ is the encoder blocksize which determines the height of the caps in the final encoding. $n_u$ is the minimum width and $n_v$ is the minimum height. $P$ is the pixel overlap which is included to avoid interpolation artifacts when reconstructing the original sphere. $\hat{p}_a(u; \bm{\gamma})$ is the central projection along the first dimension, which converts from $u$ to azimuth $a$, defined by parameters $\bm{\gamma}$. $\hat{p}_e(v; \bm{\gamma})$ is the central projection along the second dimension, which converts from $v$ to azimuth $e$, defined by parameters $\bm{\gamma}$.

\subsection{Parameters}

From the defining parameters described above, we can calculate some useful numbers.

$\hat{p}_u^{-1}(a; \bm{\gamma})$ is the inverse projection along the first dimension. $\hat{p}_v^{-1}(e; \bm{\gamma})$ is the inverse projection along the second dimension. 

$[u_0,u_1)$ is the span of the first coordinate. $[v_0,v_1]$ is the span of the second coordinate.

\begin{equation}
u_0 = \hat{p}^{-1}_u(\pi; \bm{\gamma})
\end{equation}
\begin{equation}
u_1 = \hat{p}^{-1}_u(-\pi; \bm{\gamma})
\end{equation}

\begin{equation}
v_0 = \hat{p}^{-1}_v\left (\frac{\pi}{2}; \bm{\gamma}\right )
\end{equation}
\begin{equation}
v_1 = \hat{p}^{-1}_v\left (-\frac{\pi}{2}; \bm{\gamma}\right)
\end{equation}

$\psi_i$ is the upper elevation for the ith ring, so $\psi_1$ is the bottom elevation angle of the top cap and the top elevation angle of the first ring ignoring overlaps. $\psi_0$ is $\frac{\pi}{2}$ by definition.

$n_i$ is the number of blocks in the ith ring. $n_0$ is always $1$ as the $0$th ring is the cap.

$R$ is the number of rings, including the caps.

$r_c$ is the elevation extent of the caps, including overlap pixels.

\begin{equation}
r_c = (\pi - \psi_1)\frac{B + 2P}{B}
\end{equation}

\subsection{Cap Projection}

\begin{equation}
\hat{u} \in [0,\frac{B}{n_u}(u_1 - u_0) + u_0]
\end{equation}
\begin{equation}
\hat{v} \in [0,\frac{B}{n_v}(v_1 - v_0) + v_0]
\end{equation}
\begin{equation}
\hat{r} \in [0,\infty)
\end{equation}

\begin{equation}
m = 2\frac{\hat{u} - u_0}{\frac{B}{n_u}(u_1 - u_0)} - 1
\end{equation}
\begin{equation}
n = 2\frac{\hat{v} - v_0}{\frac{B}{n_v}(v_1 - v_0)} - 1
\end{equation}

\begin{equation}
\alpha = \frac{r_c n\max(|n|,|m|)}{\sqrt{n^2 + m^2}}
\end{equation}
\begin{equation}
\beta = \frac{r_c m\max(|n|,|m|)}{\sqrt{n^2 + m^2}}
\end{equation}

\begin{equation}
\hat{a} = \arctan\left(\frac{\sin(\alpha)}{\tan(\beta)}\right);
\end{equation}
\begin{equation}
\hat{e} = \arcsin(\cos(\alpha)\cos(\beta));
\end{equation}

\begin{equation}
x =\hat{r}\cos(\hat{a}) \cos(\hat{e}) 
\end{equation}
\begin{equation}
y = \hat{r}\sin(\hat{a})\cos(\hat{e})
\end{equation}
\begin{equation}
z = \hat{r}\sin(\hat{e})
\end{equation}

\begin{equation}
\hat{J}(\hat{u},\hat{v},\hat{r}) = 
\begin{bmatrix}
    \frac{dx}{d\hat{a}} & \frac{dx}{d\hat{e}} & \frac{dx}{d\hat{r}} \\   \frac{dy}{d\hat{a}} & \frac{dy}{d\hat{e}} & \frac{dy}{d\hat{r}} \\\frac{dz}{d\hat{a}} & \frac{dz}{d\hat{e}} & \frac{dz}{d\hat{r}} \\
\end{bmatrix}
\begin{bmatrix}
    \frac{d\hat{a}}{d\alpha} & \frac{d\hat{a}}{d\beta} & 0 \\   \frac{d\hat{e}}{d\alpha} & \frac{d\hat{e}}{d\beta} & 0 \\ 0 & 0 & 1 \\
\end{bmatrix}
\begin{bmatrix}
    \frac{d\alpha}{dm} & \frac{d\alpha}{dn} & 0 \\   \frac{d\beta}{dm} & \frac{d\beta}{dn} & 0 \\ 0 & 0 & 1 \\
\end{bmatrix}
\begin{bmatrix}
    \frac{dm}{d\hat{u}} & 0 & 0 \\   0 & \frac{dn}{d\hat{v}} & 0 \\ 0 & 0 & 1 \\
\end{bmatrix}
\end{equation}
\begin{equation}
\hat{J}(\hat{u},\hat{v},\hat{r}) = 
\begin{bmatrix}
    \frac{dx}{d\hat{a}} & \frac{dx}{d\hat{e}} & \frac{dx}{d\hat{r}} \\   \frac{dy}{d\hat{a}} & \frac{dy}{d\hat{e}} & \frac{dy}{d\hat{r}} \\\frac{dz}{d\hat{a}} & \frac{dz}{d\hat{e}} & \frac{dz}{d\hat{r}} \\
\end{bmatrix}
\begin{bmatrix}
    \frac{d\hat{a}}{d\alpha} & \frac{d\hat{a}}{d\beta} & 0 \\   \frac{d\hat{e}}{d\alpha} & \frac{d\hat{e}}{d\beta} & 0 \\ 0 & 0 & 1 \\
\end{bmatrix}
\begin{bmatrix}
    \frac{d\alpha}{dm}\frac{dm}{d\hat{u}} & \frac{d\alpha}{dn}\frac{dn}{d\hat{v}} & 0 \\   \frac{d\beta}{dm}\frac{dm}{d\hat{u}} & \frac{d\beta}{dn}\frac{dn}{d\hat{v}} & 0 \\ 0 & 0 & 1 \\
\end{bmatrix}
\end{equation}

\begin{equation}
||\hat{J}(\hat{u},\hat{v},\hat{r})|| = \left | \hat{r}^2\cos(\hat{e}) \left( \right) \left( \right) \left( \right) \right |
\end{equation}


\subsection{Central Projection}

$B, n_u, n_v, R, \vec{\bm{n}}, \vec{\psi}, P, u_{p}(\theta), v_{p}(\phi), \theta(u_p), \phi(v_p), u_0, u_1, v_0, v_1$

\begin{equation}
u_0 = u_p(\pi)
\end{equation}
\begin{equation}
u_1 = u_p(-\pi)
\end{equation}
\begin{equation}
v_0 = v_p(\frac{\pi}{2})
\end{equation}
\begin{equation}
v_1 = v_p(-\frac{\pi}{2})
\end{equation}
\begin{equation}
\phi_t = \phi\left (\frac{\frac{B}{n_v}(n_v - 2P) - P}{n_v - 2P}(v_1 - v_0) + v_0\right )
\end{equation}

\begin{equation}
u \in [u_0, u_1)
\end{equation}
\begin{equation}
v \in [v_0, v_1]
\end{equation}
\begin{equation}
r \in [0,\infty)
\end{equation}

\begin{equation}
u = \left\{
  \begin{array}{lr}
   \frac{n_u - 2P}{n_u} u_p\left(\arctan \left ( {\frac{y}{x}} \right ) \right ) & : |z| <= \cos(\phi_t)\\
   \frac{n_u - 2P}{n_u} u_p\left(\arctan \left ( {\frac{y}{x}} \right ) \right ) & : |z| >= \cos(\psi_1)\\
   wraparound case\\
    x^3 & : x \ge 0
  \end{array}
\right.
\end{equation}
\begin{equation}
v = \frac{z}{\sqrt{x^2 + y^2 + z^2}}
\end{equation}
\begin{equation}
r = \sqrt{x^2 + y^2 + z^2}
\end{equation}

%\frac{B}{n_u}(u_1 - u_0) + u_0

%\frac{B}{n_v}(v_1 - v_0) + v_0

\begin{equation}
r_c = (\pi - \psi_1)\frac{B + 2P}{B}
\end{equation}
\begin{equation}
m = 2\frac{u - u_0}{\frac{B}{n_u}(u_1 - u_0)} - 1
\end{equation}
\begin{equation}
    n = 2\frac{|v| - v_0}{
\frac{B}{n_v}(v_1 - v_0)} - 1
\end{equation}

\begin{equation}
az = \frac{r_c n\max(|n|,|m|)}{\sqrt{n^2 + m^2}}
\end{equation}
\begin{equation}
el = -\frac{r_c m\max(|n|,|m|)}{\sqrt{n^2 + m^2}}
\end{equation}

\begin{equation}
\theta(u,v) = \left\{
  \begin{array}{lr}
  \theta\left(\frac{\frac{u - u_0}{u_1 - u_0}(n_v)}{n_v - 2P}(u_1 - u_0) + u_0\right) : |v| <= \frac{B}{n_v}(v_1 - v_0) + v_0\\
\arctan(\frac{\sin(az)}{-\tan(el)}) : |v| > \frac{B}{n_v}(v_1 - v_0) + v_0 \text{ and } u > \left(\frac{B}{n_u}(u_1 - u_0) + u_0 \right)\\
   \\
  \end{array}
\right.
\end{equation}

\begin{equation}
x =r \cos(\theta(u)) \cos(\phi(v)) 
\end{equation}
\begin{equation}
y = r \sin(u)\cos(\arcsin(v))
\end{equation}
\begin{equation}
z = r v
\end{equation}

\begin{equation}
J(u,v,w) = 
\begin{bmatrix}
    -r\sin(u)\cos(\arcsin(v)) & -\frac{r\cos(u)v}{\sqrt{(1 - v^2)}} & \cos(u)\cos(\arcsin(v)) \\   r\cos(u)\cos(\arcsin(v)) & -\frac{r\sin(u)v}{\sqrt{(1 - v^2)}} & \sin(u)\cos(\arcsin(v)) \\   0 & r & v \\
\end{bmatrix}
\end{equation}
\begin{equation}
||J_u(u,v,r)||  = r\sqrt{1 - v^2}
\end{equation}
\begin{equation}
||J_v(u,v,r)||  = \frac{r}{\sqrt{1 - v^2}}
\end{equation}
\begin{equation}
||J_r(u,v,r)||  = r^2
\end{equation}
\begin{equation}
||J(u,v,w)||  = r^2
\end{equation}
\begin{equation}
r_u(u,v,r)  = \frac{N_u}{2\pi r\sqrt{1 - v^2}}
\end{equation}
\begin{equation}
r_v(u,v,r)  = \frac{N_v\sqrt{1 - v^2}}{\pi r}
\end{equation}
\begin{equation}
r(u,v,r)  = \frac{N_uN_v}{2\pi^2 r^2 \cos(v)}
\end{equation}


\end{document}
