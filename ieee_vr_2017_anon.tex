% $Id: ieee_vr_2017_anon.tex 11 2007-04-03 22:25:53Z jpeltier $

\documentclass{vgtc}                          % final (conference style)
\let\ifpdf\relax
%\documentclass[review]{vgtc}                 % review
%\documentclass[widereview]{vgtc}             % wide-spaced review
%\documentclass[preprint]{vgtc}               % preprint
%\documentclass[electronic]{vgtc}             % electronic version

%% Uncomment one of the lines above depending on where your paper is 
%% in the conference process. ``review'' and ``widereview'' are for review
%% submission, ``preprint'' is for pre-publication, and the final version
%% doesn't use a specific qualifier. Further, ``electronic'' includes
%% hyperreferences for more convenient online viewing.

%% Please use one of the ``review'' options in combination with the
%% assigned online id (see below) ONLY if your paper uses a double blind
%% review process. Some conferences, like IEEE Vis and InfoVis, have NOT
%% in the past.

%% Figures should be in CMYK or Grey scale format, otherwise, colour 
%% shifting may occur during the printing process.

%% These three lines bring in essential packages: ``mathptmx'' for Type 1 
%% typefaces, ``graphicx'' for inclusion of EPS figures. and ``times''
%% for proper handling of the times font family.

\usepackage{mathptmx}
\usepackage{graphicx}
\usepackage{times}
\usepackage{amsmath}
\usepackage{mathtools}
\usepackage{array}
\usepackage{calc}
\usepackage{booktabs}

\usepackage{asymptote}
\usepackage[utf8]{inputenc}
\usepackage{esint}
\usepackage{tikz}
\usepackage{float}
\usepackage{bm}
\usetikzlibrary{matrix,arrows,calc}
\usepackage{breqn}
\usepackage[margin=0.5in]{geometry}

\DeclareMathOperator*{\argmin}{arg\,min}
\DeclareMathOperator*{\argmax}{arg\,max}

\newlength\celldim \newlength\fontheight \newlength\extraheight
\newcounter{sqcolumns}
\newcolumntype{S}{ @{}
>{\centering \rule[-0.5\extraheight]{0pt}{\fontheight + \extraheight}}
p{\celldim} @{} }
\newcolumntype{Z}{ @{} >{\centering} p{\celldim} @{} }
\newenvironment{squarecells}[1]
{\setlength\celldim{2em}%
\settoheight\fontheight{A}%
\setlength\extraheight{\celldim - \fontheight}%
\setcounter{sqcolumns}{#1 - 1}%
\begin{tabular}{|S|*{\value{sqcolumns}}{Z|}}\hline}
% squarecells tabular goes here
{\end{tabular}}

\newcommand\nl{\tabularnewline\hline}


%% We encourage the use of mathptmx for consistent usage of times font
%% throughout the proceedings. However, if you encounter conflicts
%% with other math-related packages, you may want to disable it.

%% If you are submitting a paper to a conference for review with a double
%% blind reviewing process, please replace the value ``0'' below with your
%% OnlineID. Otherwise, you may safely leave it at ``0''.
\onlineid{0}

%% declare the category of your paper, only shown in review mode
\vgtccategory{Research}

%% allow for this line if you want the electronic option to work properly
\vgtcinsertpkg

%% In preprint mode you may define your own headline.
%\preprinttext{To appear in an IEEE VGTC sponsored conference.}

%% Paper title.

\title{Resolution-Defined Projections for Virtual Reality Video Compression}

%% This is how authors are specified in the conference style

%% Author and Affiliation (single author).
%%\author{Roy G. Biv\thanks{e-mail: roy.g.biv@aol.com}}
%%\affiliation{\scriptsize Allied Widgets Research}

%% Author and Affiliation (multiple authors with single affiliations).
\author{Author1\thanks{e-mail: anon@company} %
\and Author2\thanks{e-mail: anon@company}}
\affiliation{\scriptsize Company Research}

%% Author and Affiliation (multiple authors with multiple affiliations)
%%\author{Charles Dunn\thanks{e-mail:charles.dunn@youvisit.com}\\ %
%%        \scriptsize CompanyName %
%%\and Brian Knott\thanks{e-mail:brian.knott@CompanyName.com}\\ %
%%     \scriptsize CompanyName %
%%}

%% A teaser figure can be included as follows, but is not recommended since
%% the space is now taken up by a full width abstract.
%\teaser{
%  \includegraphics[width=1.5in]{sample.eps}
%  \caption{Lookit! Lookit!}
%}

%% Abstract section.
\abstract{Spherical data compression methods for Virtual Reality (VR) currently leverage popular rectangular data encoding algorithms. Traditional compression algorithms have massive adoption and hardware support on computers and mobile devices. Efficiently utilizing these two-dimensional compression methods for spherical data necessitates a projection from the three-dimensional surface of a sphere to a two-dimensional rectangle. Any such projection affects the final resolution distribution of the data after decoding.

Popular projections used for VR video benefit from mathematical or geometric simplicity, but result in suboptimal resolution distributions. We introduce a method for generating a projection to match a desired resolution function. This method allows for customized projections with smooth, continuous and optimal resolution functions. Compared to commonly used projections, our resolution-defined projections drastically improve compression ratios for any given quality.

%In this paper, we will describe our methodology, propose two projections that improve on existing widely-used projections, and demonstrate the improvement our method provides to resolution function and distortion.
} % end of abstract

%% ACM Computing Classification System (CCS). 
%% See <http://www.acm.org/class/1998/> for details.
%% The ``\CCScat'' command takes four arguments.

\CCScatlist{ 
  \CCScat{H.5.1}{Multimedia Information Systems}%
{Virtual Reality}{};
  \CCScat{E.4}{Coding and Information Theory}{Data compaction and compression}{}
}
%% Copyright space is enabled by default as required by guidelines.
%% It is disabled by the 'review' option or via the following command:
% \nocopyrightspace

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%% START OF THE PAPER %%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

%% The ``\maketitle'' command must be the first command after the
%% ``\begin{document}'' command. It prepares and prints the title block.
%% the only exception to this rule is the \firstsection command
\firstsection{Introduction}

\maketitle

%% \section{Introduction} 

Omnidirectional videos are inherently immersive, making them a preferred medium for VR experiences~\cite{1}. Omnidirectional video data is rendered to a virtual three-dimensional (3D) sphere for viewing, yet common video encodings operate only on two-dimensional (2D) rectangular data. Therefore, it is necessary to use a projection to transform data from a sphere in 3D space to a rectangle in 2D space before using traditional encoding methods~\cite{8}\cite{2}. There are, however, an infinite number of such projections, each having a unique effect of the final resolution of the data during viewing~\cite{10}. In this paper, we will analyze the resolution distributions resulting from common projections used in VR video and compare them to custom projections we have generated by aiming to match a desired resolution distribution.

The most commonly used projection used in VR is the \emph{equirectangular} projection which maps a sphere to azimuth and elevation coordinates~\cite{3}. While this mapping benefits from mathematical simplicity, equirectangular images have increasing spatial resolution as the elevation increases toward the poles. Many related globe projections suffer from this same problem, inaccurately magnifying polar regions (e.g. Antarctica or Greenland)~\cite{10}. Despite this known flaw, the equirectangular projection format is nearly ubiquitous in omnidirectional media storage. 

The \emph{cubic} mapping projects the sphere to a cube and encodes its faces as planar images~\cite{5}. This geometric approach avoids the singularities of the equirectangular approach, but it sacrifices adjacency (there is no rectangular arrangement of the faces of a cube such that adjacency is maintained). Also, the cubic projection still results in a suboptimal resolution distribution, with resolution increasing towards the corners of the cube and away from the forward viewing direction.

Some modern approaches of VR video delivery leverage the knowledge that users do not view the entire sphere simultaneously~\cite{11}. For VR video streaming on mobile devices, where headtracking data is readily available and data reduction is crucial, a projection that improves the resolution in the user's instantaneous viewing direction is preferable. This can be accomplished at the expense of resolution degradation in unseen directions.

The \emph{pyramid} projection is one such adaptive technique. Facebook developed it specifically for adaptive VR video delivery based on the viewer's head motion. The projection is inspired by a pyramid where the base has a much higher resolution than the sides. The entire projection is rotated so the base follows the viewer's forward direction as closely as possible. This adaptive rotation and pyramid projection greatly improve resolution in the viewing direction compared to data projected using the equirectangular or cubic mappings. Facebook claims that using this projection can reduce the bitrate of a video by 80 percent relative to the equirectangular projection~\cite{6}. Despite this improvement, the resolution undesirably increases away from the forward viewing direction towards the corners of the pyramid base.

In this paper, we propose projections defined by desired resolution functions that can outperform existing projections. One proposed projection satisfies constant resolution, besting the equirectangular and cubic projections for non-adaptive content delivery. A second proposed projection greatly improves on the pyramid projection for use in adaptive streaming.

%While these projections reduce 360 video file sizes by 25 percent and 80 percent, respectively, from equirectangular layouts, we will show that each exhibits a suboptimal pixel density distribution. In this paper, we also propose projections for static streaming (as in CubeMap) as well as dynamic streaming (as in Pyramid) that aim for more favorable pixel density distributions to improve effective resolution while maintaining data reduction benefits.

\section{Method}

We are interested in projections that map from the Cartesian unit sphere in $(x,y,z)$ space to a flat rectangle in  $(u,v,w)$ space. Therefore, we define our projections by their mapping equations of $x$, $y$, and $z$ as functions of $u$, $v$, and $w$.

Assuming the final displayed resolution is limited only by the representation during transmission (i.e. not limited by the display resolution or the initial resolution of the data), then the final resolution function $R$, as a function of azimuth and elevation viewing direction $(\theta,\phi)$, depends only on the projection and the pixel dimensions of the 2D rectangle.

\begin{equation} \label{eq:res}
R(\theta,\phi) = \frac{N_uN_v}{ L_uL_v } \frac{1}{||J(\theta,\phi,1)||}
\end{equation}

Here, $N_u$ and $N_v$ are the horizontal and vertical projection dimensions in pixels, $L_u$ and $L_v$ are the horizontal and vertical lengths of the unit-sphere rectangle after projection, and $J(\theta,\phi,\rho)$ is the Cartesian Jacobian of the projection as a function of spherical coordinates.

\begin{equation} \label{eq:J}
J(\theta,\phi,\rho) = \left . \begin{bmatrix}
    \frac{\partial{x}}{\partial{u}} & \frac{\partial{x}}{\partial{v}} & \frac{\partial{x}}{\partial{w}} \\    \frac{\partial{y}}{\partial{u}} & \frac{\partial{y}}{\partial{v}} & \frac{\partial{y}}{\partial{w}} \\    \frac{\partial{z}}{\partial{u}} & \frac{\partial{z}}{\partial{v}} & \frac{\partial{z}}{\partial{w}} \\
\end{bmatrix}\right |_{(\theta,\phi,\rho)}
\end{equation}

We create our resolution-defined projections by solving for the Jacobian for a given resolution function. Our approach leverages an existing valid projection from $(x,y,z)$ space to $(a,b,c)$ space as a seed projection. That is, we use an existing rectangular projection (defined with coordinates $(a,b,c)$) as a seed for deriving our new projection. This method requires that the Jacobian of the seed projection $\tilde{J}$ and the desired resolution function be separable in $(a,b,c)$ coordinate space, so the following conditions must be met.

\begin{equation}
\tilde{J}(a,b,c) = {\tilde{J}_{a}}(a){\tilde{J}_{b}}(b){\tilde{J}_c}(c)
\end{equation}
\begin{equation}
R(a,b,c) =  {R_{a}}(a){R_{b}}(b){R_{c}}(c)
\end{equation}

From these equations, we solve a set of single-variable first-order differential equations to derive our final projection. As an example, solving the following for $a$ as a function of $u$ gives us the final mapping from $(x,y,z)$ space to the first coordinate $u$ in the final projection space.

\begin{equation}
\frac{d{a}}{d{u}}(u) \propto  \frac{1}{{R_{a}}(a(u))||{\tilde{J}_{a}}(a(u))||} 
\end{equation}

\section{Projections}

\subsection{Lambert Equal-Area Cylindrical}

Using our method for deriving projections, we re-derived the Lambert Equal-Area Cylindrical projection as a projection with exactly uniform resolution. Unlike the equirectangular and cubic projections, the same amount of data describes every viewing direction on the sphere~\cite{2}. Using this projection provides 57\% better resolution on the horizon than the equirectangular projection. Unfortunately distortion remains an issue at the poles, but we have developed a related projection outside the scope of this paper to improve uniformity. %Note that the Lambert projection is highly related to Archimedes' finding that the surface area of a sphere is the same as the side of the circumscribing cylinder.

%voluptua~\cite{kitware2003,Max:1995:OMF}. At vero eos et accusam et

\subsection{CompanyName Adaptive}

Using a resolution function to match the spatial resolution of the human eye, our method is able to derive a nearly optimal projection. As a result, our novel projection's resolution function peaks in the forward viewing direction, with resolution smoothly decreasing in all directions. Again, the projection suffers from some distortion near the poles, but we are able to deal with this distortion as we do with the Lambert projection. As a result, our new projection significantly outperforms other projections derived for adaptive resolution in VR delivery. 

\section{Results}

In order to quantitatively compare the projections described above, we describe our methods of calculating the total resolution $\bar{R}$ and total uniformity $\bar{U}$ of a projection. Since the desired resolution for a certain application can be spatially varying, we introduce a normalized weighting function $\hat{W}(\theta,\phi)$ that represents the importance of each metric as function of viewing direction.

\begin{equation} \label{eq:U}
\bar{R} = \oiint\limits_{S}\hat{W}(\theta,\phi)R(\theta,\phi)dA
\end{equation}

In order to measure uniformity, we define $R_u$ and $R_v$ to be the horizontal and vertical resolutions which are related to the scale elements $J_u$ and $J_v$, respectively. $J_u$ and $J_v$ are simply the corresponding columns of a projection's Jacobian.

\begin{equation}\label{eq:r_u}
R_u(\theta,\phi) = \frac{N_u}{ L_u } \frac{1}{||J_u(\theta,\phi,1)||}
\end{equation}

We can now measure the uniformity of the projection ${U}$ as a ratio of the horizontal and vertical resolution functions to calculate the total weighted uniformity.

\begin{equation} \label{eq:U}
{U}(\theta,\phi) = \frac{\min \left( R_u(\theta,\phi),R_v(\theta,\phi)\right)}{\max \left( R_u(\theta,\phi),R_v(\theta,\phi)\right)}
\end{equation} 

\begin{equation} \label{eq:U}
\bar{U} = \oiint\limits_{S}\hat{W}(\theta,\phi)U(\theta,\phi)dA
\end{equation}

\begin{figure}[htb]
  \centering
  \includegraphics[width=3in]{res_plots.png}
  \caption{Resolution and uniformity in Mollweide coordinates for a) Equirectangular b) Cubic c) Lambert d) Pyramid and e) CompanyName projections.}
\end{figure}

\subsection{Weighting Functions}

We compare projections using three weighting functions. The first is an impulse function in the forward direction, measuring only the projection resolution and distortion in the forward direction. The second resembles the foveated resolution function of the human eye when looking forward. The third approximates the empirical viewing patterns of humans while viewing VR video content. 

Table \ref{vis_accept} reports metrics for these three weighting functions over the five projections described previously. Note that we exclude the empirical metric for adaptive delivery methods since content is dynamically switched to match the user's viewing direction.

\begin{table}
  \caption{Projection Metrics}
  \label{vis_accept}
  \scriptsize
  \begin{center}
    \begin{tabular}{ccccccc}
     \; & \multicolumn{3}{c}{Resolution} & \multicolumn{3}{c}{Uniformity}\\
      Projection & forward & fovea & emp. & forward & fovea & emp. \\
    \hline
      Equirectangular & 0.64 & 0.65 & 0.76 & 1.00 & 0.98 & 0.88 \\
      Cubic & 0.52 & 0.66 & 0.99 & 1.00 & 0.96 & 0.90 \\
      Lambert & 1.00 & 1.00 & 1.00 & 1.00 & 0.96 & 0.80\\
      \hline
      Pyramid & 1.39 & 1.75 & - & 0.86 & 0.84 & - \\
      CompanyName & 2.49 & 2.28 & - & 1.00 & 0.92 & - 
    \end{tabular}
  \end{center}
\end{table}

\section{Conclusion}

Projections created by our method outperform existing industry standards in terms of increased resolution and decreased distortion in important areas. This method can be used to generate custom projections for special cases of VR video where resolution is significantly more important in some viewing directions than in others. For example, video-specific projections can be designed as empirical measurements of viewing patterns are made on a video-by-video basis. Removing the restriction of geometrically designed projections means that moving forward, context based compression for omnidirectional video may become useful in more contexts and more heavily optimized.

%% if specified like this the section will be ommitted in review mode
\acknowledgements{The authors wish to thank CTOName and our coworkers at CompanyName for tremendous support on this project and its implementation.}

\bibliographystyle{abbrv}
%%use following if all content of bibtex file should be shown
%\nocite{*}
\bibliography{ieee_vr_2017_anon}
\end{document}
