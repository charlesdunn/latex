\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{mathtools}
\usepackage{esint}

\DeclareMathOperator\erf{erf}
\DeclareMathOperator\erfinv{erf^{-1}}

\DeclareMathOperator*{\argmin}{arg\,min}

\title{Projection from Ideal Resolution Function for Virtual Reality Content Delivery}
\author{charles dunn\\YouVisit}
\date{January 2016}

\begin{document}

\maketitle

\section{Introduction}

Virtual Reality (VR) content is typically displayed on the inside of a sphere, with the user at the origin of the sphere. Data compression for VR content, typically image or video data, is essential since the data will often be transmitted via a bandwidth-constrained network (e.g. celullar network, Wi-Fi). To optimally utilize existing algorithms for image and video data compression, the content must be rectangular. Therefore, a projection must be used to convert between the three-dimensional (3D) spherical content in VR space and the two-dimensional (2D) rectangular representation for input to traditional encoding methods.

In this paper we will analyze this projection step, define metrics to compare methods, and derive optimal projections. Finally, we detail a projection specially designed for VR content delivery.

\section{Projections}

The projections we will consider map 3D Cartesian coordinates, $(x,y,z)$, to a new coordinate space, $(u,v,w)$. Let $P\in \mathcal{R}^3 \rightarrow \mathcal{R}^3$ be any such projection and let $p_{(\cdot)} \in \mathcal{R}^3 \rightarrow \mathcal{R}^1$ be the associated projections to the individual coordinates $u$, $v$ and $w$.

\begin{equation}
P(x,y,z) = (u,v,w) = (p_u(x,y,z),p_v(x,y,z),p_w(x,y,z))
\end{equation}

For our applications, the projection must be bijective, with a defined $P^{-1}\in \mathcal{R}^3 \rightarrow \mathcal{R}^3$ and associated $p^{-1}_{(\cdot)} \in \mathcal{R}^3 \rightarrow \mathcal{R}^1$.

\begin{equation}
(x,y,z) = P^{-1}(u,v,w) = (p^{-1}_x(u,v,w),p^{-1}_y(u,v,w),p^{-1}_z(u,v,w))
\end{equation}

The projections we will consider must also map the unit sphere (u.s.) in Cartesian space to a rectangle spanning only $u$ and $v$ in projection space. In other words, the projection of any point on the unit sphere in Cartesian space must map to the same $w$ coordinate. For each projection, $w_0$ is this value of $w$ that maps to the unit sphere. We will call this rectangle in projection space the unit-sphere rectangle (u.s.r).

\begin{equation}
P(x,y,z)|_{(x,y,z)\in u.s.} = (u,v,w_0) = (u,v)|_{w_0}
\end{equation}

Since the value of $w_0$ can be derived for any projection, we will often omit the implied $|_{w_0}$ notation.

\begin{equation}
P^{-1}(u,v) \equiv P^{-1}(u,v)|_{w_0} = P^{-1}(u,v,w_0) 
\end{equation}

Let $u\in [u_a,u_b)$ and $v\in [v_a,v_b)$ (with $w=w_0$) define the unit-sphere rectangle in projection space. The VR content will be mapped to coordinates within this rectangle in projection space. Let $A_p$ be the area of the unit-sphere rectangle in projection space and let $\Sigma_p$ be a surface area element in projection space.

\begin{equation}
\iint\displaylimits_{u.s.r.} d\Sigma_p = \int_{v_a}^{v_b} \int_{u_a}^{u_b} \; du \; dv = (v_b - v_a)(u_b - u_a) = A_p
\end{equation}

Note that to differentiate the coordinate bases, the subscript $_c$ refers to a value in Cartesian coordinates while the subscript $_p$ refers to a value in projection coordinates. 

We also know the area of the unit sphere in Cartesian space. Let $A_c$ be the area of the unit sphere in Cartesian space and let $\Sigma_c$ be a surface area element in projection space. 

\begin{equation}
\iint\displaylimits_{u.s.} d\Sigma_c = 4 \pi = A_c
\end{equation}

In order to calculate surface integrals across coordinate spaces, we must include the Jacobian of a projection. The Jacobian is a function of the inverse individual coordinate projection equations (i.e. $p^{-1}_{(\cdot)}$).

\begin{equation}
J(u,v) = J(u,v,w_0) = \left . \begin{bmatrix}
    \frac{\partial{p^{-1}_x}}{\partial{u}} & \frac{\partial{p^{-1}_x}}{\partial{v}} & \frac{\partial{p^{-1}_x}}{\partial{w}} \\    \frac{\partial{p^{-1}_y}}{\partial{u}} & \frac{\partial{p^{-1}_y}}{\partial{v}} & \frac{\partial{p^{-1}_y}}{\partial{w}} \\    \frac{\partial{p^{-1}_z}}{\partial{u}} & \frac{\partial{p^{-1}_z}}{\partial{v}} & \frac{\partial{p^{-1}_z}}{\partial{w}} \\
\end{bmatrix}\right |_{w = w_0}
\end{equation}

We have defined the constraints on the projections $P$ we consider valid, and explained the associated functions $P^{-1}$, $p_{(\cdot)}$, $p^{-1}_{(\cdot)}$, and $J$, all of which can be derived from any valid $P$.

\subsection{Surface Integrals}

We can now write the full equation for the surface integral of a function in Cartesian space evaluated in projection space and vice versa. Let $f_c(x,y,z)$ be any function defined on the unit sphere in Cartesian space.

\begin{equation}
\iint\displaylimits f_c(x,y,z) \; d\Sigma_c = \iint\displaylimits f_c(P^{-1}(u,v)) \; ||J(u,v)|| \; d\Sigma_p
\end{equation}

If we are integrating over the unit sphere in Cartesian space, we can be more explicit with the integration since we are integrating over the unit-sphere rectangle in projection space.

\begin{equation}
\begin{split}
\iint\displaylimits_{u.s.} f_c(x,y,z) \; d\Sigma_c & = \iint\displaylimits_{u.s.r.} f_c(P^{-1}(u,v)) \; ||J(u,v)|| \; d\Sigma_p \\ & = \int_{v_a}^{v_b} \int_{u_a}^{u_b} f_c(P^{-1}(u,v)) \; ||J(u,v)|| \; du \; dv
\end{split}
\end{equation}

Analogously, we can write the full equation for the projection space surface integral of a function $f_p(u,v,w)$ evaluated in Cartesian space as well.

\begin{equation}
\iint\displaylimits f_p(u,v,w) \; d\Sigma_p = \iint\displaylimits f_p(P(x,y,z)) \; ||J^{-1}(x,y,z)|| \; d\Sigma_c
\end{equation}

$J^{-1}$ is the Jacobian for the inverse projection.

\begin{equation}
||J^{-1}(x,y,z)|| = \frac{1}{||J(P(x,y,z))||}
\end{equation}



In practice, implementing a projection change for visual data is not trivial. The projection $P$ defines the mapping from one coordinate space to the other, but interpolation is required where the $||J||<$.


\section{Resolution Functions}

Resolution $r$ is defined as the ratio of discrete pixels $N$ to the area they occupy $A$.

\begin{equation}
r \equiv \frac{N}{A}
\end{equation}

In practice, pixels are typically uniform and square, so the resolution is constant over an entire image or frame of video. 

The final content resolution is limited by the representation in projection space.\footnote{In this paper, we assume that the resolution of the VR device in terms of pixels per solid angle is much higher than the final resolution of the VR content. This is especially true when the data has been scaled to create file sizes small enough to deliver over a cellular data network.} The pixels are square and uniform in projection space, but after the warping of space during the inverse projection, the pixels are not necessarily square or uniform. Therefore, the uniform resolution in projection space leads to a variable resolution in the final sphere.

In projection space, the resolution $r_p$ is uniform. Let $N_p$ be the total number of pixels in projection space.

\begin{equation}
r_p = \frac{N_p}{A_p}
\end{equation}

The integral of resolution over the unit-sphere rectangle is predictably the total number of pixels.

\begin{equation}
\iint\displaylimits_{u.s.r.} r_p(u,v,w) \; d\Sigma_p = \iint\displaylimits_{u.s.r.} \frac{N_p}{A_p} \; d\Sigma_p = N_p
\end{equation}

In Cartesian space, however, the resolution $r_c$ is not necessarily uniform. It is in fact inversely related to the Jacobian of the projection. 

\begin{equation}
\iint\displaylimits_{u.s.r.} r_p(u,v,w) \; d\Sigma_p = \iint\displaylimits_{u.s.r.} \frac{N_p}{A_p} \; d\Sigma_p = N_p
\end{equation}


The local resolution is still a ratio of pixel count to area, but for an infinitesimally small area, we use the Jacobian of the projection to 

The Jacobian accounts for the warping of space during projection. This warping of space is also what determines the final content resolution viewed by the user. If we take a sphere of VR content, project to rectangular projection space for encoding and delivery, and then reverse project to recreate the sphere of content, then the final viewing resolution $r_c$ is inversely proportional to the magnitude of the determinant of the Jacobian of that projection.

\begin{equation}
r_c(x,y,z) \propto \frac{1}{||J(p_u(x,y,z),p_v(x,y,z))||}
\end{equation}

This assumes the resolution of the original content is sufficiently high such that no oversampling occurs during projection, which we can ensure by choosing the total number of pixels of the content in projection space $N_p$. $N_p$ also predictably affects the final viewing resolution.

The other factor that determines the final viewing resolution is the total number of pixels $N_p$ of the content in its rectangular, projection-space representation. $N_p$ is also an information measure of the final content on the sphere if the resolution of the VR display is at least the final resolution $r_c$. We must also include a normalizing factor $\alpha_p$ to ensure that the total 

\begin{equation}
    r_c(x,y,z) = \frac{N_p}{\alpha} \hat{r}_c(x,y,z)
\end{equation}


Let $r_c^*$ be an ideal resolution function measuring pixels per area in Cartesian space. Let it be defined in shape by $\hat{r}_c^*(x,y,z)$ and in magnitude by $N^*$, a number of pixels on the unit sphere in Cartesian space.

\begin{equation}
    r_c^*(x,y,z) = \frac{N^*}{\alpha^*} \hat{r}_c^*(x,y,z)
\end{equation}
\begin{equation}
    \alpha^* = \iint\displaylimits_{u.s.} \hat{r}_c^*(x,y,z)\; dA_c
\end{equation}

$\alpha^*$ is a normalizing factor to ensure that $N^*$ is the number of pixels over the sphere.

\begin{equation}
    \iint\displaylimits_{u.s.} r_c^*(x,y,z) \; dA_c = \iint\displaylimits_{u.s.} \frac{N^*}{\alpha^*} \hat{r}_c^*(x,y,z) \; dA_c = N^*
\end{equation}

Let $r_c$ be the resolution on the unit sphere after mapping from projection space for a certain projection. Similar to $r_c^*$, this is defined in shape by the a resolution function $\hat{r}_c$ and in magnitude by some number of pixels in projection space $N$. 

\begin{equation}
    r_c(u,v) = \frac{N}{\alpha}\hat{r}_c(u,v)
\end{equation}

The warping of space due to a projection causes the uniform resolution in projection space to potentially be non-uniform in Cartesian space. This warping of space is represented by the magnitude of the determinant of the Jacobian.

\begin{equation}
    \hat{r}_c(u,v) = \frac{1}{||J(u,v)||}
\end{equation}
\begin{equation}
    \alpha =  \iint\displaylimits_{u.s.} \hat{r}_c(u,v)\; dA_c =  \iint\displaylimits_{u.s.} \frac{1}{||J(u,v)||} ||J(u,v)|| \; dA_p = \iint\displaylimits_{u.s.} dA_p = A_p
\end{equation}

$N$ is the total number of pixels in projection space. The resolution \emph{in projection space} $r_p$ is constant with respect to $u,v$ since we require square pixels in projection space.

\begin{equation}
    r_p = \frac{N}{\alpha} = \frac{N}{A_p}
\end{equation}

Here $\alpha$ is again a normalizing factor, but it is also the total area in projection space.

\section{Cost Functions}

We would like to define a single metric $\gamma$ for every projection that measures how perfectly its resolution function matches the ideal resolution function. Let this score be generated by integrating some cost function $C(\cdot,\cdot)$ in the projection space over the unit sphere. A lower $\gamma$ indicates a better match to $r_c^*$.

\begin{equation}
    \gamma = \iint\displaylimits_{u.s.} C(r_c(u,v),r_c^*(u,v))\; du \;dv
\end{equation}

In general, the cost function should be non-negative everywhere and non-increasing with respect to the resolution function; higher resolutions should never increase the cost. It should also be 0 if $r_c=r_c^*$.

We would like to solve for the fewest pixels in a projection space such that the metric $\gamma$ is at most some value $\gamma^*$.

\begin{equation}
    N_{\min} = \min \left \{ N \; \mid \; \gamma\leq \gamma^* \right \}
\end{equation}

\subsection{Extreme Step Cost Function}

For simplicity, an \emph{extreme step} (ES) cost function was selected for this paper.

\begin{equation}
    C_{ES}(r_c,r_c^*) = \begin{cases} 
      \infty & r_c< r_c^* \\
      0 & r_c \geq r_c^*
   \end{cases}
\end{equation}

 This cost function means the resolution function is a \emph{minimum resolution function} (MRF), since it enforces that the resolution in any projection space is at least the target resolution everywhere. Assuming $\gamma^*$ is not infinite, we can solve more explicitly for $N_{\min}$.


\begin{equation}
    N_{\min} = \min \left \{ N \; \mid \; \iint\displaylimits_{u.s.} C_{ES}(r_c(u,v),r_c^*(u,v))\; du \;dv < \infty \right \}
\end{equation}
\begin{equation}
    N_{\min} = \min \left \{ N \; \mid \; \frac{N}{\alpha}\frac{1}{||J(u,v)||} \geq r_c^*(u,v) \: \: \forall \; (u,v) \right \}
\end{equation}
\begin{equation}
    N_{\min} = \min \left \{ N \; \mid \; \frac{N}{\alpha}\geq \max_{u,v} \left \{ ||J(u,v)||  r_c^*(u,v) \right \} \right \}
\end{equation}
\begin{equation}
    N_{\min} =  A_p \max_{u,v} \left \{ ||J(u,v)|| r_c^*(u,v) \right \}
\end{equation}

Given a projection with a solvable Jacobian, we can calculate a number of pixels $N_{\min}$ such that our ideal resolution function is at least met, if not exceeded, for all points. Note that $N_{\min}\geq N^*$ with equality only when the projection produces the ideal resolution function (i.e. $r_c(u,v) = r_c^*(u,v)$).

\section{Optimal Projections}

Given an ideal resolution function $r_c^*(x,y,z)$, it would be convenient if we could solve for an \emph{optimal projection}. This projection would be optimal in the sense that the resulting resolution in projection space would be identical to the ideal resolution for any points that map to the unit sphere. This optimal projection would be optimal for \emph{any} cost function where $C(r_c^*,r_c^*)=0$ is the minimum possible cost.

\begin{equation}
    r_c^*(u,v) \equiv r_c^*(P^{-1}(u,v))
\end{equation}
\begin{equation}
    r_c^*(u,v) = \frac{N}{\alpha} \frac{1}{||J^*(u,v)||}
\end{equation}

In order to simplify the determinant of the Jacobian, we apply a constraint to the projection that coordinates are; off-diagonal terms of the Jacobian matrix will be 0.

\begin{equation}
    ||J^*|| = 
\left |\det \begin{bmatrix}
    \frac{\partial{x}}{\partial{u}} & 0 & 0 \\
    0 & \frac{\partial{y}}{\partial{v}} & 0 \\
    0 & 0 & \frac{\partial{z}}{\partial{w}} \\
\end{bmatrix}
\right | = \left | \frac{\partial{x}}{\partial{u}} \frac{\partial{y}}{\partial{v}} \frac{\partial{z}}{\partial{w}} \right |
\end{equation}

Because of the above constraint, the forward mapping and its partial derivatives are functions of just one of the projection space coordinates.

\begin{equation}
\left| \frac{\partial{x}}{\partial{u}}(u) \frac{\partial{y}}{\partial{v}}(v) \frac{\partial{z}}{\partial{w}}(w) \right | = \frac{N}{\alpha} \frac{1}{r_c^*(x(u),y(v),z(w))} = \frac{N}{\alpha} \frac{\alpha^*}{N^*}\frac{1}{\hat{r}_c^*(x(u),y(v),z(w))}
\end{equation}

If we only consider ideal resolution functions that are separable in $x$, $y$, and $z$, then the differential equation is separable.

\begin{equation}
\hat{r}_c^*(x,y,z) = \prescript{}{x}{\hat{r}}_c^*(x)\prescript{}{y}{\hat{r}}_c^*(y)\prescript{}{z}{\hat{r}}_c^*(z)
\end{equation}
\begin{equation}
\left | \frac{\partial{x}}{\partial{u}}(u) \right | \propto \frac{1}{\prescript{}{x}{\hat{r}}_c^*(x(u))} 
\end{equation}
\begin{equation}
\left | \frac{\partial{y}}{\partial{v}}(v) \right | \propto \frac{1}{\prescript{}{y}{\hat{r}}_c^*(y(v))} 
\end{equation}
\begin{equation}
\left | \frac{\partial{z}}{\partial{w}}(w) \right | \propto \frac{1}{\prescript{}{z}{\hat{r}}_c^*(z(w))} \end{equation}

If we can solve these differential equations for $x(u)$, $y(v)$, and $z(w)$, then we can solve for the optimal projection. 

Since $\alpha$ and $\alpha^*$ are normalizing factors, even if $\frac{1}{||J^*||}$ and $\hat{r}_c^*$ are equal only up to a constant, they are exactly equal after normalization.

\begin{equation}
\frac{\hat{r}_c^*(u,v)}{\alpha^*} = \frac{1}{\alpha}\frac{1}{||J^*(u,v)||}
\end{equation}

For the extreme step cost function we would like to show that $N_{\min}=N^*$ when the above is true.

\begin{equation}
    N_{\min} =  A_p \max_{u,v} \left \{ ||J^*(u,v)|| r_c^*(u,v) \right \}
\end{equation}
\begin{equation}
    N_{\min} = \max_{u,v} \left \{ \alpha ||J^*(u,v)|| \frac{N^*}{\alpha^*}\hat{r}_c^*(u,v) \right \}
\end{equation}
\begin{equation}
    N_{\min} = \max_{u,v} \left \{ N^* \right \} = N^*
\end{equation}

Therefore, when the ideal resolution function is separable and we can solve the three differential equations above, then we are guaranteed to have an optimal projection.

\subsection{Intermediate Projections}

In the above solution for an optimal projection, we require the ideal resolution function to be separable in $x$,$y$, and $z$. In fact, if there is some intermediate projection in which the ideal resolution and Jacobian are separable, this basis $(a,b,c)$ can be used by including the Jacobian from the intermediate projection.

\begin{equation}
P_i(x,y,z) = (a,b,c)
\end{equation}
\begin{equation}
P_j(a,b,c) = (u,v,w)
\end{equation}
\begin{equation}
J_{i}(a,b,c) = \prescript{}{a} J_{i}(a(u))\prescript{}{b} J_{i}(b(v))\prescript{}{c} J_{i}(c(w))
\end{equation}
\begin{equation}
\hat{r}_c^*(x,y,z) = \hat{r}_c^*(P^{-1}(a,b,c)) = \hat{r}_c^*(a,b,c)
\end{equation}
\begin{equation}
\hat{r}_c^*(a,b,c) = \prescript{}{a}{\hat{r}}_c^*(a)\prescript{}{b}{\hat{r}}_c^*(b)\prescript{}{c}{\hat{r}}_c^*(c)
\end{equation}
\begin{equation}
\left | \prescript{}{a} J_{i}(a(u)) \frac{\partial{a}}{\partial{u}}(u) \right | \propto \frac{1}{\prescript{}{a}{\hat{r}}_c^*(a(u))} 
\end{equation}
\begin{equation}
\left | \prescript{}{b} J_{i}(b(v)) \frac{\partial{b}}{\partial{v}}(v) \right | \propto \frac{1}{\prescript{}{b}{\hat{r}}_c^*(b(v))} 
\end{equation}
\begin{equation}
\left | \prescript{}{c} J_{i}(c(w)) \frac{\partial{c}}{\partial{w}}(w) \right | \propto \frac{1}{\prescript{}{c}{\hat{r}}_c^*(c(w))} 
\end{equation}

If we drop the absolute values, the equations are still valid and are more restrictive but simpler. We use just the first coordinates, $a$ and $u$, to continue the derivation.

\begin{equation}
 \frac{\partial{a}}{\partial{u}}(u) \propto \frac{1}{\prescript{}{a} J_{i}(a(u)) \prescript{}{a}{\hat{r}}_c^*(a(u))} 
\end{equation}

This differential equation has the following solution.

\begin{equation}
 \frac{\partial{y}}{\partial{x}}(x) \propto \frac{1}{g(y(x)) f(y(x))}
 \end{equation}
\begin{equation}
F(y) = \int g(\gamma) f(\gamma)\; d\gamma \mid_{\gamma = y}
\end{equation}
\begin{equation}
y(x) = F^{-1}(x)
\end{equation}


Using these equations, we might be able to solve for $a(u)$, $b(v)$, and $c(w)$ and therefore have the projection from intermediate coordinates to our final projection coordinates $P_j$. Since we already know the intermediate projection, we can then easily solve for the full projection.

\begin{equation}
P(x,y,z) = (u,v,w) = P_j(P_i(x,y,z))
\end{equation}

\subsubsection{Spherical Intermediate Projection}

Because we (so far) only use spherical coordinates as an intermediate projection, the Jacobians are included here.

\begin{equation}
x = \rho \cos (\phi)\cos (\theta)
\end{equation}
\begin{equation}
y = \rho \cos(\phi) \sin(\theta)
\end{equation}
\begin{equation}
z = \rho \sin(\phi)
\end{equation}


\begin{equation}
J_{i}(\theta,\phi,\rho) = \prescript{}{\theta} J_{i}(\theta(u))\prescript{}{\phi} J_{i}(\phi(v))\prescript{}{c} J_{i}(\rho(w)) = \rho^2 \cos(b)
\end{equation}

\begin{equation}
\prescript{}{\theta} J_{i}(\theta(u)) = 1
\end{equation}
\begin{equation}
\prescript{}{\phi} J_{i}(\phi(v)) = \cos(\phi)
\end{equation}
\begin{equation}
\prescript{}{\rho} J_{i}(\rho(w)) = \rho^2
\end{equation}

\section{Example Optimal Projections}
\subsection{Azimuthal Cosine MRF}

The following is the derivation for an optimal projection for a minimum resolution function that is a cosine in azimuth and is constant in elevation. $\lambda$ should be greater than or equal to 2 to ensure that the resolution requirement is not negative. This $\lambda$ parameter can be used to widen or narrow the azimuth angles with good resolution.

\begin{equation}
\prescript{}{\theta}{\hat{r}}_c^*(\theta) = \cos\left(\frac{\theta}{\lambda}\right)
\end{equation}
\begin{equation}
\prescript{}{\phi}{\hat{r}}_c^*(\phi) = 1
\end{equation}
\begin{equation}
\prescript{}{\rho}{\hat{r}}_c^*(\rho) = \frac{1}{\rho^2}
\end{equation}

\begin{equation}
\hat{r}_c^*(\theta,\phi,\rho) = \prescript{}{\theta}{\hat{r}}_c^*(\theta)\prescript{}{\phi}{\hat{r}}_c^*(\phi)\prescript{}{\rho}{\hat{r}}_c^*(\rho) = \frac{\cos\left (\frac{\theta}{\lambda}\right )}{\rho^2}
\end{equation}

\begin{equation}
\left | \frac{\partial{\theta}}{\partial{u}}(u) \right | \propto \frac{1}{\cos\left (\frac{\theta(u)}{\lambda} \right )} 
\end{equation}
\begin{equation}
\left | \cos(\phi(v)) \frac{\partial{\phi}}{\partial{v}}(v) \right | \propto 1 
\end{equation}
\begin{equation}
\left | \frac{1}{\rho(w)^2} \frac{\partial{\rho}}{\partial{w}}(w) \right | \propto \frac{1}{\rho(w)^2} 
\end{equation}

\begin{equation}
\frac{\partial{\theta}}{\partial{u}}(u) \propto \frac{1}{\cos\left (\frac{\theta(u)}{\lambda} \right )} 
\end{equation}
\begin{equation}
\frac{\partial{\phi}}{\partial{v}}(v) \propto \frac{1}{\cos(\phi(v))}
\end{equation}
\begin{equation}
\frac{\partial{\rho}}{\partial{w}}(w) \propto 1
\end{equation}

\begin{equation}
\theta(u) = \lambda \arcsin \left ( \frac{u}{\lambda} \right )
\end{equation}
\begin{equation}
\phi(v) = \arcsin ( v )
\end{equation}
\begin{equation}
\rho(w) = w
\end{equation}

\begin{equation}
x(u,v,w) = w \cos (\arcsin ( v ))\cos (\lambda \arcsin \left ( \frac{u}{\lambda} \right ))= w \sqrt{1 -  v^2}\cos (\lambda \arcsin \left ( \frac{u}{\lambda} \right ))
\end{equation}
\begin{equation}
y(u,v,w) = w \cos(\arcsin ( v )) \sin(\lambda \arcsin \left ( \frac{u}{\lambda} \right )) = w \sqrt{1 -  v^2} \sin(\lambda \arcsin \left ( \frac{u}{\lambda} \right )) 
\end{equation}
\begin{equation}
z(u,v,w) = w \sin(\arcsin ( v )) = wv
\end{equation}

\subsection{Uniform MRF}

An optimal projection for a uniform minimum resolution function is called an \emph{Equal Area Projection}. The following is the derivation for an optimal projection for a minimum resolution function that is a cosine in azimuth and is constant in elevation. We use the above result to simplify the calculation. It turns out that derived projection is known as the Lambert Equal Area Cylindrical projection.

\begin{equation}
\prescript{}{\theta}{\hat{r}}_c^*(\theta) = 1
\end{equation}
\begin{equation}
\prescript{}{\phi}{\hat{r}}_c^*(\phi) = 1
\end{equation}
\begin{equation}
\prescript{}{\rho}{\hat{r}}_c^*(\rho) = \frac{1}{\rho^2}
\end{equation}

\begin{equation}
\prescript{}{\theta}{\hat{r}}_c^*(\theta) = 1 = \lim_{\lambda\rightarrow \infty} \cos \left ( \frac{\theta}{\lambda} \right )
\end{equation}

\begin{equation}
\theta(u) = \lim_{\lambda\rightarrow \infty} \lambda \arcsin \left ( \frac{u}{\lambda} \right )
\end{equation}

\begin{equation}
\theta(u) = u
\end{equation}
\begin{equation}
\phi(v) = \arcsin ( v )
\end{equation}
\begin{equation}
\rho(w) = w
\end{equation}

\subsection{Elevational Cosine MRF}

A minimum resolution function that varies in elevation leads to some issues. Since the intermediate projection's Jacobian is not constant with respect to the elevation, the differential equation that must be solved is much more complicated. We have not yet found a solution for the optimal projection in this case.

\begin{equation}
\prescript{}{\theta}{\hat{r}}_c^*(\theta) = 1
\end{equation}
\begin{equation}
\prescript{}{\phi}{\hat{r}}_c^*(\phi) = \cos(\frac{\phi}{\lambda})
\end{equation}
\begin{equation}
\prescript{}{\rho}{\hat{r}}_c^*(\rho) = \frac{1}{\rho^2}
\end{equation}

\begin{equation}
\hat{r}_c^*(\theta,\phi,\rho) = \prescript{}{\theta}{\hat{r}}_c^*(\theta)\prescript{}{\phi}{\hat{r}}_c^*(\phi)\prescript{}{\rho}{\hat{r}}_c^*(\rho) = \frac{\cos(\phi)}{\rho^2}
\end{equation}

\begin{equation}
\left | \frac{\partial{\theta}}{\partial{u}}(u) \right | \propto 1
\end{equation}
\begin{equation}
\left | \cos(\phi(v)) \frac{\partial{\phi}}{\partial{v}}(v) \right | \propto \frac{1}{\cos\left (\frac{\phi(v)}{\lambda} \right )}  
\end{equation}
\begin{equation}
\left | \frac{1}{\rho(w)^2} \frac{\partial{\rho}}{\partial{w}}(w) \right | \propto \frac{1}{\rho(w)^2} 
\end{equation}

\begin{equation}
\frac{\partial{\theta}}{\partial{u}}(u) \propto 1
\end{equation}
\begin{equation}
\frac{\partial{\phi}}{\partial{v}}(v) \propto \frac{1}{\cos(\phi(v)) \cos\left (\frac{\phi(v)}{\lambda} \right )} 
\end{equation}
\begin{equation}
\frac{\partial{\rho}}{\partial{w}}(w) \propto 1
\end{equation}

\begin{equation}
\theta(u) = u
\end{equation}
\begin{equation}
\phi(v) = ?
\end{equation}
\begin{equation}
\rho(w) = w
\end{equation}

\subsection{Azimuthal Gaussian MRF}

\begin{equation}
\prescript{}{\theta}{\hat{r}}_c^*(\theta) = \exp \left ( -\frac{\theta^2}{2 \sigma_\theta^2} \right )
\end{equation}
\begin{equation}
\prescript{}{\phi}{\hat{r}}_c^*(\phi) = 1
\end{equation}
\begin{equation}
\prescript{}{\rho}{\hat{r}}_c^*(\rho) = \frac{1}{\rho^2}
\end{equation}

\begin{equation}
\hat{r}_c^*(\theta,\phi,\rho) = \prescript{}{\theta}{\hat{r}}_c^*(\theta)\prescript{}{\phi}{\hat{r}}_c^*(\phi)\prescript{}{\rho}{\hat{r}}_c^*(\rho) = \frac{1}{\rho^2}\exp \left ( -\frac{\theta^2}{2 \sigma_\theta^2} \right )
\end{equation}

\begin{equation}
\left | \frac{\partial{\theta}}{\partial{u}}(u) \right | \propto \frac{1}{\exp \left ( -\frac{\theta(u)^2}{2 \sigma_\theta^2} \right )}
\end{equation}
\begin{equation}
\left | \cos(\phi(v)) \frac{\partial{\phi}}{\partial{v}}(v) \right | \propto 1
\end{equation}
\begin{equation}
\left | \frac{1}{\rho(w)^2} \frac{\partial{\rho}}{\partial{w}}(w) \right | \propto \frac{1}{\rho(w)^2} 
\end{equation}

\begin{equation}
\frac{\partial{\theta}}{\partial{u}}(u) \propto \exp \left (\frac{\theta(u)^2}{2 \sigma_\theta^2} \right )
\end{equation}
\begin{equation}
\frac{\partial{\phi}}{\partial{v}}(v) \propto \frac{1}{\cos(\phi(v))} 
\end{equation}
\begin{equation}
\frac{\partial{\rho}}{\partial{w}}(w) \propto 1
\end{equation}

\begin{equation}
\theta(u) = \sqrt{2}\sigma_\theta \erfinv\left (\frac{\sqrt{2}u}{\sqrt{\pi}\sigma_\theta}\right )
\end{equation}
\begin{equation}
\phi(v) = \arcsin (v)
\end{equation}
\begin{equation}
\rho(w) = w
\end{equation}

\subsection{Elevational Gaussian MRF}

\begin{equation}
\prescript{}{\theta}{\hat{r}}_c^*(\theta) = 1
\end{equation}
\begin{equation}
\prescript{}{\phi}{\hat{r}}_c^*(\phi) = \exp \left ( -\frac{\phi^2}{2 \sigma_\phi^2} \right )
\end{equation}
\begin{equation}
\prescript{}{\rho}{\hat{r}}_c^*(\rho) = \frac{1}{\rho^2}
\end{equation}

\begin{equation}
\hat{r}_c^*(\theta,\phi,\rho) = \prescript{}{\theta}{\hat{r}}_c^*(\theta)\prescript{}{\phi}{\hat{r}}_c^*(\phi)\prescript{}{\rho}{\hat{r}}_c^*(\rho) = \frac{1}{\rho^2}\exp \left ( -\frac{\phi^2}{2 \sigma_\phi^2} \right )
\end{equation}

\begin{equation}
\left | \frac{\partial{\theta}}{\partial{u}}(u) \right | \propto 1
\end{equation}
\begin{equation}
\left | \cos(\phi(v)) \frac{\partial{\phi}}{\partial{v}}(v) \right | \propto \frac{1}{\exp \left ( -\frac{\phi(v)^2}{2 \sigma_\phi^2} \right )}
\end{equation}
\begin{equation}
\left | \frac{1}{\rho(w)^2} \frac{\partial{\rho}}{\partial{w}}(w) \right | \propto \frac{1}{\rho(w)^2} 
\end{equation}

\begin{equation}
\frac{\partial{\theta}}{\partial{u}}(u) \propto 1
\end{equation}
\begin{equation}
\frac{\partial{\phi}}{\partial{v}}(v) \propto \frac{\exp \left ( \frac{\phi(v)^2}{2 \sigma_\phi^2} \right )}{\cos(\phi(v)) } 
\end{equation}
\begin{equation}
\frac{\partial{\rho}}{\partial{w}}(w) \propto 1
\end{equation}

\begin{equation}
\theta(u) = 1
\end{equation}
\begin{equation}
\phi(v) = ?
\end{equation}
\begin{equation}
\rho(w) = w
\end{equation}


\end{document}
