\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{mathtools}
\usepackage{esint}

\DeclareMathOperator\erf{erf}
\DeclareMathOperator\erfinv{erf^{-1}}

\DeclareMathOperator*{\argmin}{arg\,min}

\title{360$^{\circ}$ Content Projections}
\author{charles dunn\\YouVisit}
\date{January 2016}

\begin{document}

\maketitle

\section{Introduction}

A map projection converts from one set of coordinates to another. We are interested in finding a projection that maps the unit sphere in Cartesian-coordinate space to a rectangle in projection-coordinate space. We could then use this projection to map spherical 360$^{\circ}$ content for virtual reality (VR) to a rectangular frame which can be encoded using standard video encoding methods.

\section{Projections}

The projections we will consider map 3-dimensional (3D) Cartesian coordinates, $(x,y,z)$, to a new coordinate space, $(u,v,w)$. For our applications, the projection must be invertible and must map the unit sphere (u.s.) in Cartesian space to a rectangle spanning just $(u,v)$ in projection space. Let $P\in \mathcal{R}^3 \rightarrow \mathcal{R}^3$ be the forward projection.

\begin{equation}
P(x,y,z) = (u,v,w) = (u(x,y,z),v(x,y,z),w(x,y,z))
\end{equation}
\begin{equation}
(x,y,z) = P^{-1}(u,v,w) = (x(u,v,w),y(u,v,w),z(u,v,w))
\end{equation}

The third coordinate, $w$, should be orthogonal to the unit-sphere-mapped rectangle. If $w=w_0$ for any point on the Cartesian unit sphere, then the entire unit sphere will be spanned by just $(u,v)$ in projection space. For shorthand in this paper the $w$ coordinate is sometimes left off of equations where $w=w_0$ is implied.

\begin{equation}
P^{-1}(u,v) \equiv P^{-1}(u,v,w_0) 
\end{equation}

Let $u\in [u_a,u_b)$, $v\in [v_a,v_b)$, and $w=w_0$ define the rectangle in projection space that maps to the unit sphere in Cartesian space. In other words, the 360$^\circ$ content will be mapped to coordinates within this rectangle in projection-coordinate space. Let $A_p$ be the area of this rectangle in projection space.

\begin{equation}
\iint\displaylimits_{u.s.} dA_p = \int_{v_a}^{v_b} \int_{u_a}^{u_b} \; du \; dv = (v_b - v_a)(u_b - u_a) = A_p
\end{equation}

The inverse of the determinant of the Jacobian of a projection tells us the final resolution in VR space if we pick a certain projection. The Jacobian is a function of the partial derivatives of the projection equations.

\begin{equation}
J(u,v)
=
\begin{bmatrix}
    \frac{\partial{x}}{\partial{u}} & \frac{\partial{x}}{\partial{v}} & \frac{\partial{x}}{\partial{w}} \\    \frac{\partial{y}}{\partial{u}} & \frac{\partial{y}}{\partial{v}} & \frac{\partial{y}}{\partial{w}} \\    \frac{\partial{z}}{\partial{u}} & \frac{\partial{z}}{\partial{v}} & \frac{\partial{z}}{\partial{w}} \\
\end{bmatrix}
\end{equation}

The Jacobian measures the warping of space due to the projection. In order to keep the areas in the different coordinates straight, the subscript $_c$ refers to area or resolution in Cartesian coordinates while the subscript $_p$ refers to area or resolution in projection coordinates. Including the Jacobian, we can switch between area measures in the different coordinates.


\begin{equation}
\iint\displaylimits f(x,y,z) \; dA_c = \iint\displaylimits f(P^{-1}(u,v)) \; ||J(u,v)|| \; dA_p
\end{equation}
\begin{equation}
\iint\displaylimits_{u.s.} f(u,v,w) \; dA_p = \int_{v_a}^{v_b} \int_{u_a}^{u_b} f(u,v,w_0) \; du \; dv
\end{equation}

\section{Resolution Functions}

Let $r_c^*$ be an ideal resolution function measuring pixels per area in Cartesian space. Let it be defined in shape by $\hat{r}_c^*(x,y,z)$ and in magnitude by $N^*$, a number of pixels on the unit sphere in Cartesian space.

\begin{equation}
    r_c^*(x,y,z) = \frac{N^*}{\alpha^*} \hat{r}_c^*(x,y,z)
\end{equation}
\begin{equation}
    \alpha^* = \iint\displaylimits_{u.s.} \hat{r}_c^*(x,y,z)\; dA_c
\end{equation}

$\alpha^*$ is a normalizing factor to ensure that $N^*$ is the number of pixels over the sphere.

\begin{equation}
    \iint\displaylimits_{u.s.} r_c^*(x,y,z) \; dA_c = \iint\displaylimits_{u.s.} \frac{N^*}{\alpha^*} \hat{r}_c^*(x,y,z) \; dA_c = N^*
\end{equation}

Let $r_c$ be the resolution on the unit sphere after mapping from projection space for a certain projection. Similar to $r_c^*$, this is defined in shape by the a resolution function $\hat{r}_c$ and in magnitude by some number of pixels in projection space $N$. 

\begin{equation}
    r_c(u,v) = \frac{N}{\alpha}\hat{r}_c(u,v)
\end{equation}

The warping of space due to a projection causes the uniform resolution in projection space to potentially be non-uniform in Cartesian space. This warping of space is represented by the magnitude of the determinant of the Jacobian.

\begin{equation}
    \hat{r}_c(u,v) = \frac{1}{||J(u,v)||}
\end{equation}
\begin{equation}
    \alpha =  \iint\displaylimits_{u.s.} \hat{r}_c(u,v)\; dA_c =  \iint\displaylimits_{u.s.} \frac{1}{||J(u,v)||} ||J(u,v)|| \; dA_p = \iint\displaylimits_{u.s.} dA_p = A_p
\end{equation}

$N$ is the total number of pixels in projection space. The resolution \emph{in projection space} $r_p$ is constant with respect to $u,v$ since we require square pixels in projection space.

\begin{equation}
    r_p = \frac{N}{\alpha} = \frac{N}{A_p}
\end{equation}

Here $\alpha$ is again a normalizing factor, but it is also the total area in projection space.

\section{Cost Functions}

We would like to define a single metric $\gamma$ for every projection that measures how perfectly its resolution function matches the ideal resolution function. Let this score be generated by integrating some cost function $C(\cdot,\cdot)$ in the projection space over the unit sphere. A lower $\gamma$ indicates a better match to $r_c^*$.

\begin{equation}
    \gamma = \iint\displaylimits_{u.s.} C(r_c(u,v),r_c^*(u,v))\; du \;dv
\end{equation}

In general, the cost function should be non-negative everywhere and non-increasing with respect to the resolution function; higher resolutions should never increase the cost. It should also be 0 if $r_c=r_c^*$.

We would like to solve for the fewest pixels in a projection space such that the metric $\gamma$ is at most some value $\gamma^*$.

\begin{equation}
    N_{\min} = \min \left \{ N \; \mid \; \gamma\leq \gamma^* \right \}
\end{equation}

\subsection{Extreme Step Cost Function}

For simplicity, an \emph{extreme step} (ES) cost function was selected for this paper.

\begin{equation}
    C_{ES}(r_c,r_c^*) = \begin{cases} 
      \infty & r_c< r_c^* \\
      0 & r_c \geq r_c^*
   \end{cases}
\end{equation}

 This cost function means the resolution function is a \emph{minimum resolution function} (MRF), since it enforces that the resolution in any projection space is at least the target resolution everywhere. Assuming $\gamma^*$ is not infinite, we can solve more explicitly for $N_{\min}$.


\begin{equation}
    N_{\min} = \min \left \{ N \; \mid \; \iint\displaylimits_{u.s.} C_{ES}(r_c(u,v),r_c^*(u,v))\; du \;dv < \infty \right \}
\end{equation}
\begin{equation}
    N_{\min} = \min \left \{ N \; \mid \; \frac{N}{\alpha}\frac{1}{||J(u,v)||} \geq r_c^*(u,v) \: \: \forall \; (u,v) \right \}
\end{equation}
\begin{equation}
    N_{\min} = \min \left \{ N \; \mid \; \frac{N}{\alpha}\geq \max_{u,v} \left \{ ||J(u,v)||  r_c^*(u,v) \right \} \right \}
\end{equation}
\begin{equation}
    N_{\min} =  A_p \max_{u,v} \left \{ ||J(u,v)|| r_c^*(u,v) \right \}
\end{equation}

Given a projection with a solvable Jacobian, we can calculate a number of pixels $N_{\min}$ such that our ideal resolution function is at least met, if not exceeded, for all points. Note that $N_{\min}\geq N^*$ with equality only when the projection produces the ideal resolution function (i.e. $r_c(u,v) = r_c^*(u,v)$).

\section{Optimal Projections}

Given an ideal resolution function $r_c^*(x,y,z)$, it would be convenient if we could solve for an \emph{optimal projection}. This projection would be optimal in the sense that the resulting resolution in projection space would be identical to the ideal resolution for any points that map to the unit sphere. This optimal projection would be optimal for \emph{any} cost function where $C(r_c^*,r_c^*)=0$ is the minimum possible cost.

\begin{equation}
    r_c^*(u,v) \equiv r_c^*(P^{-1}(u,v))
\end{equation}
\begin{equation}
    r_c^*(u,v) = \frac{N}{\alpha} \frac{1}{||J^*(u,v)||}
\end{equation}

In order to simplify the determinant of the Jacobian, we apply a constraint to the projection that coordinates are; off-diagonal terms of the Jacobian matrix will be 0.

\begin{equation}
    ||J^*|| = 
\left |\det \begin{bmatrix}
    \frac{\partial{x}}{\partial{u}} & 0 & 0 \\
    0 & \frac{\partial{y}}{\partial{v}} & 0 \\
    0 & 0 & \frac{\partial{z}}{\partial{w}} \\
\end{bmatrix}
\right | = \left | \frac{\partial{x}}{\partial{u}} \frac{\partial{y}}{\partial{v}} \frac{\partial{z}}{\partial{w}} \right |
\end{equation}

Because of the above constraint, the forward mapping and its partial derivatives are functions of just one of the projection space coordinates.

\begin{equation}
\left| \frac{\partial{x}}{\partial{u}}(u) \frac{\partial{y}}{\partial{v}}(v) \frac{\partial{z}}{\partial{w}}(w) \right | = \frac{N}{\alpha} \frac{1}{r_c^*(x(u),y(v),z(w))} = \frac{N}{\alpha} \frac{\alpha^*}{N^*}\frac{1}{\hat{r}_c^*(x(u),y(v),z(w))}
\end{equation}

If we only consider ideal resolution functions that are separable in $x$, $y$, and $z$, then the differential equation is separable.

\begin{equation}
\hat{r}_c^*(x,y,z) = \prescript{}{x}{\hat{r}}_c^*(x)\prescript{}{y}{\hat{r}}_c^*(y)\prescript{}{z}{\hat{r}}_c^*(z)
\end{equation}
\begin{equation}
\left | \frac{\partial{x}}{\partial{u}}(u) \right | \propto \frac{1}{\prescript{}{x}{\hat{r}}_c^*(x(u))} 
\end{equation}
\begin{equation}
\left | \frac{\partial{y}}{\partial{v}}(v) \right | \propto \frac{1}{\prescript{}{y}{\hat{r}}_c^*(y(v))} 
\end{equation}
\begin{equation}
\left | \frac{\partial{z}}{\partial{w}}(w) \right | \propto \frac{1}{\prescript{}{z}{\hat{r}}_c^*(z(w))} \end{equation}

If we can solve these differential equations for $x(u)$, $y(v)$, and $z(w)$, then we can solve for the optimal projection. 

Since $\alpha$ and $\alpha^*$ are normalizing factors, even if $\frac{1}{||J^*||}$ and $\hat{r}_c^*$ are equal only up to a constant, they are exactly equal after normalization.

\begin{equation}
\frac{\hat{r}_c^*(u,v)}{\alpha^*} = \frac{1}{\alpha}\frac{1}{||J^*(u,v)||}
\end{equation}

For the extreme step cost function we would like to show that $N_{\min}=N^*$ when the above is true.

\begin{equation}
    N_{\min} =  A_p \max_{u,v} \left \{ ||J^*(u,v)|| r_c^*(u,v) \right \}
\end{equation}
\begin{equation}
    N_{\min} = \max_{u,v} \left \{ \alpha ||J^*(u,v)|| \frac{N^*}{\alpha^*}\hat{r}_c^*(u,v) \right \}
\end{equation}
\begin{equation}
    N_{\min} = \max_{u,v} \left \{ N^* \right \} = N^*
\end{equation}

Therefore, when the ideal resolution function is separable and we can solve the three differential equations above, then we are guaranteed to have an optimal projection.

\subsection{Intermediate Projections}

In the above solution for an optimal projection, we require the ideal resolution function to be separable in $x$,$y$, and $z$. In fact, if there is some intermediate projection in which the ideal resolution and Jacobian are separable, this basis $(a,b,c)$ can be used by including the Jacobian from the intermediate projection.

\begin{equation}
P_i(x,y,z) = (a,b,c)
\end{equation}
\begin{equation}
P_j(a,b,c) = (u,v,w)
\end{equation}
\begin{equation}
J_{i}(a,b,c) = \prescript{}{a} J_{i}(a(u))\prescript{}{b} J_{i}(b(v))\prescript{}{c} J_{i}(c(w))
\end{equation}
\begin{equation}
\hat{r}_c^*(x,y,z) = \hat{r}_c^*(P^{-1}(a,b,c)) = \hat{r}_c^*(a,b,c)
\end{equation}
\begin{equation}
\hat{r}_c^*(a,b,c) = \prescript{}{a}{\hat{r}}_c^*(a)\prescript{}{b}{\hat{r}}_c^*(b)\prescript{}{c}{\hat{r}}_c^*(c)
\end{equation}
\begin{equation}
\left | \prescript{}{a} J_{i}(a(u)) \frac{\partial{a}}{\partial{u}}(u) \right | \propto \frac{1}{\prescript{}{a}{\hat{r}}_c^*(a(u))} 
\end{equation}
\begin{equation}
\left | \prescript{}{b} J_{i}(b(v)) \frac{\partial{b}}{\partial{v}}(v) \right | \propto \frac{1}{\prescript{}{b}{\hat{r}}_c^*(b(v))} 
\end{equation}
\begin{equation}
\left | \prescript{}{c} J_{i}(c(w)) \frac{\partial{c}}{\partial{w}}(w) \right | \propto \frac{1}{\prescript{}{c}{\hat{r}}_c^*(c(w))} 
\end{equation}

If we drop the absolute values, the equations are still valid and are more restrictive but simpler. We use just the first coordinates, $a$ and $u$, to continue the derivation.

\begin{equation}
 \frac{\partial{a}}{\partial{u}}(u) \propto \frac{1}{\prescript{}{a} J_{i}(a(u)) \prescript{}{a}{\hat{r}}_c^*(a(u))} 
\end{equation}

This differential equation has the following solution.

\begin{equation}
 \frac{\partial{y}}{\partial{x}}(x) \propto \frac{1}{g(y(x)) f(y(x))}
 \end{equation}
\begin{equation}
F(y) = \int g(\gamma) f(\gamma)\; d\gamma \mid_{\gamma = y}
\end{equation}
\begin{equation}
y(x) = F^{-1}(x)
\end{equation}


Using these equations, we might be able to solve for $a(u)$, $b(v)$, and $c(w)$ and therefore have the projection from intermediate coordinates to our final projection coordinates $P_j$. Since we already know the intermediate projection, we can then easily solve for the full projection.

\begin{equation}
P(x,y,z) = (u,v,w) = P_j(P_i(x,y,z))
\end{equation}

\subsubsection{Spherical Intermediate Projection}

Because we (so far) only use spherical coordinates as an intermediate projection, the Jacobians are included here.

\begin{equation}
x = \rho \cos (\phi)\cos (\theta)
\end{equation}
\begin{equation}
y = \rho \cos(\phi) \sin(\theta)
\end{equation}
\begin{equation}
z = \rho \sin(\phi)
\end{equation}


\begin{equation}
J_{i}(\theta,\phi,\rho) = \prescript{}{\theta} J_{i}(\theta(u))\prescript{}{\phi} J_{i}(\phi(v))\prescript{}{c} J_{i}(\rho(w)) = \rho^2 \cos(b)
\end{equation}

\begin{equation}
\prescript{}{\theta} J_{i}(\theta(u)) = 1
\end{equation}
\begin{equation}
\prescript{}{\phi} J_{i}(\phi(v)) = \cos(\phi)
\end{equation}
\begin{equation}
\prescript{}{\rho} J_{i}(\rho(w)) = \rho^2
\end{equation}

\section{Example Optimal Projections}
\subsection{Azimuthal Cosine MRF}

The following is the derivation for an optimal projection for a minimum resolution function that is a cosine in azimuth and is constant in elevation. $\lambda$ should be greater than or equal to 2 to ensure that the resolution requirement is not negative. This $\lambda$ parameter can be used to widen or narrow the azimuth angles with good resolution.

\begin{equation}
\prescript{}{\theta}{\hat{r}}_c^*(\theta) = \cos\left(\frac{\theta}{\lambda}\right)
\end{equation}
\begin{equation}
\prescript{}{\phi}{\hat{r}}_c^*(\phi) = 1
\end{equation}
\begin{equation}
\prescript{}{\rho}{\hat{r}}_c^*(\rho) = \frac{1}{\rho^2}
\end{equation}

\begin{equation}
\hat{r}_c^*(\theta,\phi,\rho) = \prescript{}{\theta}{\hat{r}}_c^*(\theta)\prescript{}{\phi}{\hat{r}}_c^*(\phi)\prescript{}{\rho}{\hat{r}}_c^*(\rho) = \frac{\cos\left (\frac{\theta}{\lambda}\right )}{\rho^2}
\end{equation}

\begin{equation}
\left | \frac{\partial{\theta}}{\partial{u}}(u) \right | \propto \frac{1}{\cos\left (\frac{\theta(u)}{\lambda} \right )} 
\end{equation}
\begin{equation}
\left | \cos(\phi(v)) \frac{\partial{\phi}}{\partial{v}}(v) \right | \propto 1 
\end{equation}
\begin{equation}
\left | \frac{1}{\rho(w)^2} \frac{\partial{\rho}}{\partial{w}}(w) \right | \propto \frac{1}{\rho(w)^2} 
\end{equation}

\begin{equation}
\frac{\partial{\theta}}{\partial{u}}(u) \propto \frac{1}{\cos\left (\frac{\theta(u)}{\lambda} \right )} 
\end{equation}
\begin{equation}
\frac{\partial{\phi}}{\partial{v}}(v) \propto \frac{1}{\cos(\phi(v))}
\end{equation}
\begin{equation}
\frac{\partial{\rho}}{\partial{w}}(w) \propto 1
\end{equation}

\begin{equation}
\theta(u) = \lambda \arcsin \left ( \frac{u}{\lambda} \right )
\end{equation}
\begin{equation}
\phi(v) = \arcsin ( v )
\end{equation}
\begin{equation}
\rho(w) = w
\end{equation}

\begin{equation}
x(u,v,w) = w \cos (\arcsin ( v ))\cos (\lambda \arcsin \left ( \frac{u}{\lambda} \right ))= w \sqrt{1 -  v^2}\cos (\lambda \arcsin \left ( \frac{u}{\lambda} \right ))
\end{equation}
\begin{equation}
y(u,v,w) = w \cos(\arcsin ( v )) \sin(\lambda \arcsin \left ( \frac{u}{\lambda} \right )) = w \sqrt{1 -  v^2} \sin(\lambda \arcsin \left ( \frac{u}{\lambda} \right )) 
\end{equation}
\begin{equation}
z(u,v,w) = w \sin(\arcsin ( v )) = wv
\end{equation}

\subsection{Uniform MRF}

An optimal projection for a uniform minimum resolution function is called an \emph{Equal Area Projection}. The following is the derivation for an optimal projection for a minimum resolution function that is a cosine in azimuth and is constant in elevation. We use the above result to simplify the calculation. It turns out that derived projection is known as the Lambert Equal Area Cylindrical projection.

\begin{equation}
\prescript{}{\theta}{\hat{r}}_c^*(\theta) = 1
\end{equation}
\begin{equation}
\prescript{}{\phi}{\hat{r}}_c^*(\phi) = 1
\end{equation}
\begin{equation}
\prescript{}{\rho}{\hat{r}}_c^*(\rho) = \frac{1}{\rho^2}
\end{equation}

\begin{equation}
\prescript{}{\theta}{\hat{r}}_c^*(\theta) = 1 = \lim_{\lambda\rightarrow \infty} \cos \left ( \frac{\theta}{\lambda} \right )
\end{equation}

\begin{equation}
\theta(u) = \lim_{\lambda\rightarrow \infty} \lambda \arcsin \left ( \frac{u}{\lambda} \right )
\end{equation}

\begin{equation}
\theta(u) = u
\end{equation}
\begin{equation}
\phi(v) = \arcsin ( v )
\end{equation}
\begin{equation}
\rho(w) = w
\end{equation}

\subsection{Elevational Cosine MRF}

A minimum resolution function that varies in elevation leads to some issues. Since the intermediate projection's Jacobian is not constant with respect to the elevation, the differential equation that must be solved is much more complicated. We have not yet found a solution for the optimal projection in this case.

\begin{equation}
\prescript{}{\theta}{\hat{r}}_c^*(\theta) = 1
\end{equation}
\begin{equation}
\prescript{}{\phi}{\hat{r}}_c^*(\phi) = \cos(\frac{\phi}{\lambda})
\end{equation}
\begin{equation}
\prescript{}{\rho}{\hat{r}}_c^*(\rho) = \frac{1}{\rho^2}
\end{equation}

\begin{equation}
\hat{r}_c^*(\theta,\phi,\rho) = \prescript{}{\theta}{\hat{r}}_c^*(\theta)\prescript{}{\phi}{\hat{r}}_c^*(\phi)\prescript{}{\rho}{\hat{r}}_c^*(\rho) = \frac{\cos(\phi)}{\rho^2}
\end{equation}

\begin{equation}
\left | \frac{\partial{\theta}}{\partial{u}}(u) \right | \propto 1
\end{equation}
\begin{equation}
\left | \cos(\phi(v)) \frac{\partial{\phi}}{\partial{v}}(v) \right | \propto \frac{1}{\cos\left (\frac{\phi(v)}{\lambda} \right )}  
\end{equation}
\begin{equation}
\left | \frac{1}{\rho(w)^2} \frac{\partial{\rho}}{\partial{w}}(w) \right | \propto \frac{1}{\rho(w)^2} 
\end{equation}

\begin{equation}
\frac{\partial{\theta}}{\partial{u}}(u) \propto 1
\end{equation}
\begin{equation}
\frac{\partial{\phi}}{\partial{v}}(v) \propto \frac{1}{\cos(\phi(v)) \cos\left (\frac{\phi(v)}{\lambda} \right )} 
\end{equation}
\begin{equation}
\frac{\partial{\rho}}{\partial{w}}(w) \propto 1
\end{equation}

\begin{equation}
\theta(u) = u
\end{equation}
\begin{equation}
\phi(v) = ?
\end{equation}
\begin{equation}
\rho(w) = w
\end{equation}

\subsection{Azimuthal Gaussian MRF}

\begin{equation}
\prescript{}{\theta}{\hat{r}}_c^*(\theta) = \exp \left ( -\frac{\theta^2}{2 \sigma_\theta^2} \right )
\end{equation}
\begin{equation}
\prescript{}{\phi}{\hat{r}}_c^*(\phi) = 1
\end{equation}
\begin{equation}
\prescript{}{\rho}{\hat{r}}_c^*(\rho) = \frac{1}{\rho^2}
\end{equation}

\begin{equation}
\hat{r}_c^*(\theta,\phi,\rho) = \prescript{}{\theta}{\hat{r}}_c^*(\theta)\prescript{}{\phi}{\hat{r}}_c^*(\phi)\prescript{}{\rho}{\hat{r}}_c^*(\rho) = \frac{1}{\rho^2}\exp \left ( -\frac{\theta^2}{2 \sigma_\theta^2} \right )
\end{equation}

\begin{equation}
\left | \frac{\partial{\theta}}{\partial{u}}(u) \right | \propto \frac{1}{\exp \left ( -\frac{\theta(u)^2}{2 \sigma_\theta^2} \right )}
\end{equation}
\begin{equation}
\left | \cos(\phi(v)) \frac{\partial{\phi}}{\partial{v}}(v) \right | \propto 1
\end{equation}
\begin{equation}
\left | \frac{1}{\rho(w)^2} \frac{\partial{\rho}}{\partial{w}}(w) \right | \propto \frac{1}{\rho(w)^2} 
\end{equation}

\begin{equation}
\frac{\partial{\theta}}{\partial{u}}(u) \propto \exp \left (\frac{\theta(u)^2}{2 \sigma_\theta^2} \right )
\end{equation}
\begin{equation}
\frac{\partial{\phi}}{\partial{v}}(v) \propto \frac{1}{\cos(\phi(v))} 
\end{equation}
\begin{equation}
\frac{\partial{\rho}}{\partial{w}}(w) \propto 1
\end{equation}

\begin{equation}
\theta(u) = \sqrt{2}\sigma_\theta \erfinv\left (\frac{\sqrt{2}u}{\sqrt{\pi}\sigma_\theta}\right )
\end{equation}
\begin{equation}
\phi(v) = \arcsin (v)
\end{equation}
\begin{equation}
\rho(w) = w
\end{equation}

\subsection{Elevational Gaussian MRF}

\begin{equation}
\prescript{}{\theta}{\hat{r}}_c^*(\theta) = 1
\end{equation}
\begin{equation}
\prescript{}{\phi}{\hat{r}}_c^*(\phi) = \exp \left ( -\frac{\phi^2}{2 \sigma_\phi^2} \right )
\end{equation}
\begin{equation}
\prescript{}{\rho}{\hat{r}}_c^*(\rho) = \frac{1}{\rho^2}
\end{equation}

\begin{equation}
\hat{r}_c^*(\theta,\phi,\rho) = \prescript{}{\theta}{\hat{r}}_c^*(\theta)\prescript{}{\phi}{\hat{r}}_c^*(\phi)\prescript{}{\rho}{\hat{r}}_c^*(\rho) = \frac{1}{\rho^2}\exp \left ( -\frac{\phi^2}{2 \sigma_\phi^2} \right )
\end{equation}

\begin{equation}
\left | \frac{\partial{\theta}}{\partial{u}}(u) \right | \propto 1
\end{equation}
\begin{equation}
\left | \cos(\phi(v)) \frac{\partial{\phi}}{\partial{v}}(v) \right | \propto \frac{1}{\exp \left ( -\frac{\phi(v)^2}{2 \sigma_\phi^2} \right )}
\end{equation}
\begin{equation}
\left | \frac{1}{\rho(w)^2} \frac{\partial{\rho}}{\partial{w}}(w) \right | \propto \frac{1}{\rho(w)^2} 
\end{equation}

\begin{equation}
\frac{\partial{\theta}}{\partial{u}}(u) \propto 1
\end{equation}
\begin{equation}
\frac{\partial{\phi}}{\partial{v}}(v) \propto \frac{\exp \left ( \frac{\phi(v)^2}{2 \sigma_\phi^2} \right )}{\cos(\phi(v)) } 
\end{equation}
\begin{equation}
\frac{\partial{\rho}}{\partial{w}}(w) \propto 1
\end{equation}

\begin{equation}
\theta(u) = 1
\end{equation}
\begin{equation}
\phi(v) = ?
\end{equation}
\begin{equation}
\rho(w) = w
\end{equation}


\end{document}
