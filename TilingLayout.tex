\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{mathtools}
\usepackage{esint}
\usepackage{tikz}
\usepackage{float}
\usepackage{bm}
\usetikzlibrary{matrix,arrows,calc}
\usepackage{hyperref}

\newcounter{sarrow}
\newcommand\tab[1][1cm]{\hspace*{#1}}
\newcommand\xrsquigarrow[1]{%
\stepcounter{sarrow}%
\begin{tikzpicture}[decoration=snake]
\node (\thesarrow) {\strut#1};
\draw[->,decorate] (\thesarrow.south west) -- (\thesarrow.south east);
\end{tikzpicture}%
}


\DeclareMathOperator\erf{erf}
\DeclareMathOperator\erfinv{erf^{-1}}

\DeclareMathOperator*{\argmin}{arg\,min}

\title{Tiling Layout}
\author{charles dunn\\YouVisit}
\date{\today}

\begin{document}

\maketitle

Want to arrange $N$ tiles so they cover a dome at the top and bottoms of the omnidirectional sphere. The top and bottom caps will be symmetrical about the equator, so we will only reference the top cap from this point on.

The top cap starts at an elevation of $\phi_t$, determined by the blocksize of the compression algorithm and the dimensions of the video. We assume that the video is a multiple number of blocks high and wide.

The cap will be tiled by square blocks. We constrain the first ring of these tiles to have a single tile. We have a special mapping from a sphere to a rectangle for this tile that contains the pole.

The subsequent rings must in total have $N-1$ tiles. We would like all the tiles to cover a similar area on the sphere. We would also like to mitigate distortion. We therefore have a system of two equations in terms of two variables for every ring $i$, the outer radius of the ring $\phi_i$ and the number of tiles in the ring $n_i$. We will solve each ring independently from the rings that follow it in order to reduce the dimensionality of the problem.

We first solve for the outer radius of the circular cap in the center of the dome, or the innermost ring with 1 tile. We will force this tile to have the same area as the average area of all the tiles. The total area of the dome is calculated as follows.

\begin{equation}
A_{cap} =\int_{0}^{2\pi}\int_{\phi_t}^{\pi/2} cos(\phi)\;d\phi \: d\theta = 2\pi\left(\sin\left(\frac{\pi}{2}\right) - \sin(\phi_t)\right) =  2\pi\left(1 - \sin(\phi_t)\right)
\end{equation}

The area for any tile in ring $i$ is similarly calculated.

\begin{equation}
a_{i} =\int_{0}^{\frac{2\pi}{n_i}}\int_{\phi_i}^{\phi_{i-1}} cos(\phi)\;d\phi \: d\theta = \frac{2\pi}{n_i}\left(\sin\left(\phi_{i-1}\right) - \sin(\phi_i)\right) =  \frac{2\pi}{n_i}\left(\sin\left(\phi_{i-1}\right) - \sin(\phi_i)\right) 
\end{equation}

Since $n_1=1$, we can calculate the outer radius of the inner ring $\phi_1$ by setting the area equal to the average area of all N tiles. In this case, $\phi_0 = \frac{\pi}{2}$.

\begin{equation}\label{eq:c1}
\frac{2\pi}{n_i}\left(\sin\left(\phi_{i-1}\right) - \sin(\phi_i)\right) = \frac{2\pi}{N}\left(1 - \sin(\phi_t)\right)
\end{equation}

\begin{equation}
\left(1 - \sin(\phi_1)\right) = \frac{1}{N}\left(1 - \sin(\phi_t)\right)
\end{equation}

\begin{equation}
\phi_1  = \arcsin \left(1 - \frac{1}{N}\left(1 - \sin(\phi_t)\right)\right)
\end{equation}

For subsequent rings, we need to simultaneously calculate the number of tiles per ring and the outer radius of the ring. We would like minimal distortion, as we will be using spherical coordinate transformations between the square tiles in the rectangular projection and the arched tiles in the caps. We will therefore require that the cross dimensions of each tile span the same distance on the sphere. Let $\Delta\sigma_\alpha$ be the central angle covered by the latidudinal center of the tile, and let $\Delta\sigma_\beta$ be the central angle covered by the longitudinal center of the tile. Using a formula for \href{https://en.wikipedia.org/wiki/Great-circle_distance}{great circle distance}, we can set these values to be equal. Note that for computational accuracy, we should use the Vincenty formula for the calculation of the great circle distance.

\begin{equation}
\Delta\sigma=\arctan \left (\frac{\sqrt{\left(\cos\phi_2\cdot\sin(|\theta_2 - \theta_1|)\right)^2+\left(\cos\phi_1\cdot\sin\phi_2-\sin\phi_1\cdot\cos\phi_2\cdot\cos(|\theta_2 - \theta_1|)\right)^2}}{\sin\phi_1\cdot\sin\phi_2+\cos\phi_1\cdot\cos\phi_2\cdot\cos(|\theta_2 - \theta_1|)} \right ) 
\end{equation}
\begin{equation}
\Delta\sigma_\alpha=\arctan \left (\frac{\sqrt{\left(\cos\phi_m\cdot\sin(\frac{2\pi}{n_i})\right)^2+\left(\cos\phi_m\cdot\sin\phi_m-\sin\phi_m\cdot\cos\phi_m\cdot\cos(\frac{2\pi}{n_i})\right)^2}}{\sin\phi_m\cdot\sin\phi_m+\cos\phi_m\cdot\cos\phi_m\cdot\cos(\frac{2\pi}{n_i})} \right ) 
\end{equation}
\begin{equation}
\Delta\sigma_\beta=\arctan \left (\frac{\sqrt{\left(\cos\phi_i\cdot\sin(|\theta - \theta|)\right)^2+\left(\cos\phi_{i-1}\cdot\sin\phi_i-\sin\phi_{i-1}\cdot\cos\phi_i\cdot\cos(|\theta - \theta|)\right)^2}}{\sin\phi_{i-1}\cdot\sin\phi_i+\cos\phi_{i-1}\cdot\cos\phi_i\cdot\cos(|\theta - \theta|)} \right ) 
\end{equation}
\begin{equation}
\Delta\sigma_\beta=\arctan \left (\frac{\sqrt{\left(\cos\phi_{i-1}\cdot\sin\phi_i-\sin\phi_{i-1}\cdot\cos\phi_i\right)^2}}{\sin\phi_{i-1}\cdot\sin\phi_i+\cos\phi_{i-1}\cdot\cos\phi_i} \right ) 
\end{equation}
\begin{equation}
\Delta\sigma_\beta=\arctan \left (\frac{\left | \sin ( \phi_i - \phi_{i-1}) \right | }{\cos ( \phi_i - \phi_{i-1})} \right )  =  \phi_{i-1} - \phi_{i} 
\end{equation}
We now set $\Delta\sigma_\alpha=\Delta\sigma_\beta$ in order to mitigate distortion in the final projection.
\begin{equation}
\phi_{i-1} - \phi_{i} = \arctan \left (\frac{\sqrt{\left(\cos\phi_m\cdot\sin(\frac{2\pi}{n_i})\right)^2+\left(\cos\phi_m\cdot\sin\phi_m-\sin\phi_m\cdot\cos\phi_m\cdot\cos(\frac{2\pi}{n_i})\right)^2}}{\sin\phi_m\cdot\sin\phi_m+\cos\phi_m\cdot\cos\phi_m\cdot\cos(\frac{2\pi}{n_i})} \right ) 
\end{equation}
Using the relationship constraining the area of each tile in the ring (Eq \ref{eq:c1}), we can solve for $n_i$ and substitute it above.

\begin{equation}
n_i = \frac{N\left(\sin\phi_{i-1} - \sin\phi_i\right)}{ 1 - \sin\phi_t}
\end{equation}
\begin{equation}
\phi_{i-1} - \phi_{i} = \arctan \left (\frac{\cos\phi_m\sqrt{\sin^2\left (\frac{ 2\pi \left (1 - \sin\phi_t\right)}{N\left(\sin\phi_{i-1} - \sin\phi_i\right)}\right )+\sin^2\phi_m\left(1-\cos\left(\frac{ 2\pi \left (1 - \sin\phi_t\right)}{N\left(\sin\phi_{i-1} - \sin\phi_i\right)}\right)\right)^2}}{\sin^2\phi_m+\cos^2\phi_m\cdot\cos\left(\frac{ 2\pi \left (1 - \sin\phi_t\right)}{N\left(\sin\phi_{i-1} - \sin\phi_i\right)}\right)} \right ) 
\end{equation}
We can now resort to numerical methods to solve this equation. Let $f(\cdot)$ be the error from the correct solution. We can use Newton's method to approximate the solution for $\phi_i$ by searching for the points where $f(\phi_i)=0$.
\begin{equation}
\begin{split}
f(\phi_i) &= \tan\left (\phi_{i-1} - \phi_{i}\right )\left (\sin^2\phi_m+\cos^2\phi_m\cdot\cos\left(\frac{ 2\pi \left (1 - \sin\phi_t\right)}{N\left(\sin\phi_{i-1} - \sin\phi_i\right)}\right)\right)  - \\
&  \cos\phi_m\sqrt{\sin^2\left (\frac{ 2\pi \left (1 - \sin\phi_t\right)}{N\left(\sin\phi_{i-1} - \sin\phi_i\right)}\right )+\sin^2\phi_m\left(1-\cos\left(\frac{ 2\pi \left (1 - \sin\phi_t\right)}{N\left(\sin\phi_{i-1} - \sin\phi_i\right)}\right)\right)^2}
 \end{split}
\end{equation}
We now solve for the derivative, keeping in mind that $\phi_m$ is a function of $\phi_i$, with $\frac{\partial \phi_m}{\partial \phi_i} = \frac{1}{2}$
%*(sin(p)^2+cos(p)^2*cos(2*pi*(1 - sin(t))/(N*(sin(p) - sin(x)))

\begin{equation}
f'(\phi_i) = 
\end{equation}



\end{document}
