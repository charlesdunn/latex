% $Id: template.tex 11 2007-04-03 22:25:53Z jpeltier $

\documentclass{vgtc}                          % final (conference style)
\let\ifpdf\relax
%\documentclass[review]{vgtc}                 % review
%\documentclass[widereview]{vgtc}             % wide-spaced review
%\documentclass[preprint]{vgtc}               % preprint
%\documentclass[electronic]{vgtc}             % electronic version

%% Uncomment one of the lines above depending on where your paper is
%% in the conference process. ``review'' and ``widereview'' are for review
%% submission, ``preprint'' is for pre-publication, and the final version
%% doesn't use a specific qualifier. Further, ``electronic'' includes
%% hyperreferences for more convenient online viewing.

%% Please use one of the ``review'' options in combination with the
%% assigned online id (see below) ONLY if your paper uses a double blind
%% review process. Some conferences, like IEEE Vis and InfoVis, have NOT
%% in the past.

%% Figures should be in CMYK or Grey scale format, otherwise, colour 
%% shifting may occur during the printing process.

%% These three lines bring in essential packages: ``mathptmx'' for Type 1 
%% typefaces, ``graphicx'' for inclusion of EPS figures. and ``times''
%% for proper handling of the times font family.

\usepackage{mathptmx}
\usepackage{graphicx}
\usepackage{times}
\usepackage{amsmath}
\usepackage{mathtools}
\usepackage{array}
\usepackage{calc}
\usepackage{booktabs}

\usepackage{asymptote}
\usepackage[utf8]{inputenc}
\usepackage{esint}
\usepackage{tikz}
\usepackage{float}
\usepackage{bm}
\usetikzlibrary{matrix,arrows,calc}
\usepackage{breqn}
\usepackage[margin=0.5in]{geometry}

\DeclareMathOperator*{\argmin}{arg\,min}
\DeclareMathOperator*{\argmax}{arg\,max}

\newlength\celldim \newlength\fontheight \newlength\extraheight
\newcounter{sqcolumns}
\newcolumntype{S}{ @{}
>{\centering \rule[-0.5\extraheight]{0pt}{\fontheight + \extraheight}}
p{\celldim} @{} }
\newcolumntype{Z}{ @{} >{\centering} p{\celldim} @{} }
\newenvironment{squarecells}[1]
{\setlength\celldim{2em}%
\settoheight\fontheight{A}%
\setlength\extraheight{\celldim - \fontheight}%
\setcounter{sqcolumns}{#1 - 1}%
\begin{tabular}{|S|*{\value{sqcolumns}}{Z|}}\hline}
% squarecells tabular goes here
{\end{tabular}}

\newcommand\nl{\tabularnewline\hline}


%% We encourage the use of mathptmx for consistent usage of times font
%% throughout the proceedings. However, if you encounter conflicts
%% with other math-related packages, you may want to disable it.

%% If you are submitting a paper to a conference for review with a double
%% blind reviewing process, please replace the value ``0'' below with your
%% OnlineID. Otherwise, you may safely leave it at ``0''.
\onlineid{0}

%% declare the category of your paper, only shown in review mode
\vgtccategory{Research}

%% allow for this line if you want the electronic option to work properly
\vgtcinsertpkg

%% In preprint mode you may define your own headline.
%\preprinttext{To appear in an IEEE VGTC sponsored conference.}

%% Paper title.

\title{Resolution-Defined Projections for Virtual Reality Video Compression}

%% This is how authors are specified in the conference style

%% Author and Affiliation (single author).
%%\author{Roy G. Biv\thanks{e-mail: roy.g.biv@aol.com}}
%%\affiliation{\scriptsize Allied Widgets Research}

%% Author and Affiliation (multiple authors with single affiliations).
\author{Charles Dunn\thanks{e-mail: charles.dunn@youvisit.com} %
\and Brian Knott\thanks{e-mail: brian.knott@youvisit.com}}
\affiliation{\scriptsize YouVisit Research}

%% Author and Affiliation (multiple authors with multiple affiliations)
%%\author{Charles Dunn\thanks{e-mail:charles.dunn@youvisit.com}\\ %
%%        \scriptsize YouVisit %
%%\and Brian Knott\thanks{e-mail:brian.knott@youvisit.com}\\ %
%%     \scriptsize YouVisit %
%%}

%% A teaser figure can be included as follows, but is not recommended since
%% the space is now taken up by a full width abstract.
%\teaser{
%  \includegraphics[width=1.5in]{sample.eps}
%  \caption{Lookit! Lookit!}
%}

%% Abstract section.
\abstract{Spherical data compression methods for Virtual Reality (VR) currently leverage popular rectangular data encoding algorithms. Traditional compression algorithms have the advantages of availability and hardware decoding support on mobile devices. Unfortunately, using these two-dimensional (2D) compression methods for spherical data necessitates a projection from the three-dimensional (3D) surface of a sphere to a two-dimensional space. Any such projection uniquely determines the final resolution distribution of the data on the sphere.

Popular projections benefit from mathematical or geometric simplicity, but result in suboptimal resolution distributions. We introduce a method of generating a projection that matches a desired resolution function. This method allows for projection customization with smooth, continuous and nearly optimal resolution functions. Using the generated projection results in efficient use of existing data compression.

%In this paper, we will describe our methodology, propose two projections that improve on existing widely-used projections, and demonstrate the improvement our method provides to resolution function and distortion.
} % end of abstract

%% ACM Computing Classification System (CCS). 
%% See <http://www.acm.org/class/1998/> for details.
%% The ``\CCScat'' command takes four arguments.

\CCScatlist{ 
  \CCScat{H.5.1}{Multimedia Information Systems}%
{Virtual Reality}{};
  \CCScat{E.4}{Coding and Information Theory}{Data compaction and compression}{}
}
%% Copyright space is enabled by default as required by guidelines.
%% It is disabled by the 'review' option or via the following command:
% \nocopyrightspace

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%% START OF THE PAPER %%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

%% The ``\maketitle'' command must be the first command after the
%% ``\begin{document}'' command. It prepares and prints the title block.
%% the only exception to this rule is the \firstsection command
\firstsection{Introduction}

\maketitle

%% \section{Introduction} 

Omnidirectional videos are capable of capturing scenes in every direction from a point, making them the preferred medium for Virtual Reality applications. However, omnidirectional video data is captured from and rendered to a sphere while current video encoders operate only on two-dimensional rectangular images. Therefore, it is necessary to map image data from a sphere to a flat plane before using traditional encoding methods.

A common approach is to use the \emph{equirectangular} projection ---  a direct mapping from azimuth and elevation angles $(\theta,\phi)$ to the x-y plane. While this mapping is very simple, equirectangular images contain a lot of redundant pixel data as they are subject to oversampling toward the poles. The \emph{cubemap} projection reduces oversampling by projecting the sphere to a cube and encoding its faces as planar images, removing poles from the projection's geometry, providing data reduction of 25 percent.

These two projections maintain all of the pixel information on the sphere, but more modern approaches leverage the knowledge that users do not view the entire sphere simultaneously. For VR video streaming on mobile devices, where headtracking data is available and data reduction is crucial, it is preferable to use a projection that improves the resolution in the user's viewing direction, even at the expense of degradation in unseen directions. The \emph{pyramid} projection is a common example where for a particular viewing direction, the intended high resolution part of the image is projected to the base of a rectangular pyramid, while the rest of the image is projected (and degraded) to fit on its triangular faces. Using this projection can reduce the bitrate of a video by 80 percent relative to the \emph{equirectangular} projection.

These common approaches greatly improve omnidirectional video compression, but their geometries introduce distortion and lead to unfavorable distributions of resolution (pixel density) on the sphere. In this paper, we propose projections defined by desired resolution functions that are able to maintain the bitrate reduction of geometric projections while reducing distortion and optimizing resolution distributions in accordance with user experience.


%While these projections reduce 360 video file sizes by 25 percent and 80 percent, respectively, from equirectangular layouts, we will show that each exhibits a suboptimal pixel density distribution. In this paper, we also propose projections for static streaming (as in CubeMap) as well as dynamic streaming (as in Pyramid) that aim for more favorable pixel density distributions to improve effective resolution while maintaining data reduction benefits.

\section{Method}

Assuming the final displayed resolution is limited only by the representation during transmission (i.e. not limited by the display resolution or the initial resolution of the data), then the final resolution function $R$ as a function of azimuth and elevation viewing direction $(\theta,\phi)$ depends only on the projection.

\begin{equation} \label{eq:res}
R(\theta,\phi) = \frac{N_uN_v}{ L_uL_v } \frac{1}{||J(\theta,\phi,1)||}
\end{equation}

$N_u$ and $N_v$ are the horizontal and vertical projection dimensions in pixels. $L_u$ and $L_v$ are the horizontal and vertical lengths of the unit-sphere rectangle after projection. $J(\theta,\phi,\rho)$ is the Cartesian Jacobian of the projection as a function of spherical coordinates.

\begin{equation} \label{eq:J}
J(\theta,\phi,\rho) = \left . \begin{bmatrix}
    \frac{\partial{x}}{\partial{u}} & \frac{\partial{x}}{\partial{v}} & \frac{\partial{x}}{\partial{w}} \\    \frac{\partial{y}}{\partial{u}} & \frac{\partial{y}}{\partial{v}} & \frac{\partial{y}}{\partial{w}} \\    \frac{\partial{z}}{\partial{u}} & \frac{\partial{z}}{\partial{v}} & \frac{\partial{z}}{\partial{w}} \\
\end{bmatrix}\right |_{(\theta,\phi,\rho)}
\end{equation}

We create novel resolution-defined projections by solving for the Jacobian given a resolution function. Once the Jacobian is known, a projection can be derived by solving a series of first-order differential equations. Note that our method requires an existing projection with a separable Jacobian and an input resolution function that is also separable in this projection's coordinate space. 

\subsection{Hybrid Projections}

Here, we describe in depth our method of calculating a projection from a desired resolution function.

From the above equations, it is clear how a projection impacts the resulting resolution functions. Starting with a desired resolution function, if we can find a projection whose Jacobian determinant is equal up to a scalar to our desired resolution function, then we have solved for the ideal projection. Since this is still a very open-ended problem, we solve for the ideal function as a separable projection applied along each dimension of an existing separable projection from a sphere to a rectangle.

If $\tilde{P}$ is a projection from a Cartesian sphere in $(x,y,z)$ space to a rectangle in $(a,b,c)$ space with a separable Jacobian, we would like to solve for a projection $P$ defined by the functions $u(a)$ and $v(b)$, where we let $w(c) = c$ without losing generality since $c$ must already be orthonogal to our rectangular projection space. Our \emph{hybrid projection} $\bar{P}$ is then defined by the complete mapping functions $(u(a(x,y,z)),v(b(x,y,z)),w(c(x,y,z)))$. Due to the separability of $P$, its Jacobian has only diagonal elements.

\begin{equation} \label{eq:J_h}
    J(\theta,\phi,\rho) = 
    \left .
\begin{bmatrix}
    \frac{d{a}}{d{u}} & 0 & 0 \\
    0 & \frac{d{b}}{d{v}} & 0 \\
    0 & 0 & 1\\
\end{bmatrix}
\right |_{(\theta,\phi,\rho)}
\end{equation}

Note that $a$ and $\frac{d{a}}{d{u}}$ are functions of just $u$, and are independent of $v$ and $w$. This is important later.

The Jacobian of the hybrid projection $\bar{J}$ is simply a product of its component projection Jacobians, and the distributive property of the determinant allows us to solve for the hybrid projection's Jacobian determinant in terms of its component Jacobians.

\begin{equation}
||\bar{J}||  = ||\tilde{J} ||||{J}||
\end{equation}

\begin{equation} \label{eq:Jtores}
||\bar{J}||  = ||\tilde{J} ||\left |\frac{d{a}}{d{u}} \frac{d{b}}{d{v}}  \right|
\end{equation}

Combining Equations \ref{eq:res} and \ref{eq:Jtores}, we create a multivariable first-order differential equation to solve in order to derive the resolution-defined hybrid projection $\bar{P}$.

\begin{equation} \label{eq:Jcomb}
||\tilde{J} ||\left |\frac{d{a}}{d{u}} \right|\left |\frac{d{b}}{d{v}} \right| =  \frac{N_uN_v}{ L_uL_v r } 
\end{equation}

We now apply two additional constraints. If we restrict our intermediate projection Jacobian determinant and our target resolution function to be separable in the $(a,b,c)$ coordinate basis, then all of Equation \ref{eq:Jcomb} is separable in $(u,v,w)$ coordinates. This separability also relies on the diagonality of $J$, as we mentioned earlier.

\begin{equation}
\tilde{J}(a,b,c) = {\tilde{J}_{a}}(a){\tilde{J}_{b}}(b){\tilde{J}_c}(c)
\end{equation}
\begin{equation}
r(a,b) =  {r_{a}}(a){r_{b}}(b)
\end{equation}
\begin{equation} \label{eq:Jcombsep}
\begin{split}
||{\tilde{J}_{a}}(a(u)){\tilde{J}_{b}}(b(v)){\tilde{J}_c}(c(w))||\left |\frac{d{a}}{d{u}}(a) \right|\left |\frac{d{b}}{d{v}}(b) \right|=\\  \frac{N_uN_v}{ L_uL_v  {r_{a}}(a(u)){r_{b}}(b(v))} 
\end{split}
\end{equation}
We can now isolate the part of equation relating to $a$ and $u$.

\begin{equation}
\frac{d{a}}{d{u}}(u) \propto  \frac{1}{{r_{a}}(a(u))||{\tilde{J}_{a}}(a(u))||} 
\end{equation}

Note that we dropped the absolute value, making the equation only slightly more strict. If this first-order differential equation is solvable, then we have our mapping function $a(u)$ and can solve for the inverse, $u(a)$. 

\begin{equation}
u(x,y,z) = u(a(x,y,z))
\end{equation}

We can similarly solve for $v(x,y,z)$ and $w(x,y,z)$.

Now that we have the full mapping functions from $(x,y,z)$ space to $(u,v,w)$ space defining our hybrid projection, $L_u$ and $L_v$ are simply the size of the ranges of $u(x,y,z)$ and $v(x,y,z)$, respectively, over the unit sphere in $(x,y,z)$ Cartesian space.

\subsubsection{Example}

We will use the hybrid projection method described to derive a projection with a uniform resolution function. 

\begin{equation}
r(a,b) = 1
\end{equation}

We choose the Equirectangular projection as our intermediate function in this example.

\begin{equation}
a(x,y,z) = \arctan \left ({\frac{y}{x}}\right )
\end{equation}
\begin{equation}
b(x,y,z) = \arcsin \left ( {\frac{z}{\sqrt{x^2 + y^2 + z^2}}}\right)
\end{equation}
\begin{equation}
c(x,y,z) = \sqrt{x^2 + y^2 + z^2}
\end{equation}

\begin{equation}
||J(a(u),b(v),c(w))|| = c(w)^2 \cos(b(v))
\end{equation}

\begin{equation}
\frac{d{a}}{d{u}}(u) \propto 1
\end{equation}
\begin{equation}
a(u) = u
\end{equation}
\begin{equation}
u(a) = a
\end{equation}
\begin{equation}
\frac{d{b}}{d{v}}(v) \propto \frac{1}{\cos(b(v))}
\end{equation}
\begin{equation}
b(v) = \arcsin(v)
\end{equation}
\begin{equation}
v(b) = \sin(b)
\end{equation}
\begin{equation}
w(c) = c
\end{equation}

\begin{equation}
u(x,y,z) = \arctan\left (\frac{y}{x}\right)
\end{equation}
\begin{equation}
v(x,y,z) =  {\frac{z}{\sqrt{x^2 + y^2 + z^2}}}
\end{equation}
\begin{equation}
w(x,y,z) = \sqrt{x^2 + y^2 + z^2}
\end{equation}

We have derived the Lambert Equal-Area Cylindrical projection!

\section{Projections}

We will briefly describe an assortment of projections before comparing their performance.

\subsection{Static Projections}

Static projections are used to deliver a full sphere of VR image or video data in a single file. This is the standard approach to image and video compression for virtual reality as the data is agnostic to the user's viewing direction or any other data determined by the local viewing.

\subsection{Equirectangular}

\begin{equation}
u(x,y,z) = \arctan \left ({\frac{y}{x}}\right )
\end{equation}
\begin{equation}
v(x,y,z) = \arcsin \left ( {\frac{z}{\sqrt{x^2 + y^2 + z^2}}}\right)
\end{equation}
\begin{equation}
w(x,y,z) = \sqrt{x^2 + y^2 + z^2}
\end{equation}
\begin{equation}
x(u,v,w) =w\cos(u) \cos(v) 
\end{equation}
\begin{equation}
y(u,v,w) = w\sin(u)\cos(v)
\end{equation}
\begin{equation}
z(u,v,w) = w\sin(v)
\end{equation}

The equirectangular projection is the industry standard. It is the simplest azimuth-elevation projection but it introduces large distortion and an extremely undesirable resolution distribution for typical VR video content.

\begin{equation}
r_u(\theta,\phi)  = \frac{N_u}{2\pi \cos(\phi)}
\end{equation}
\begin{equation}
r_v(\theta,\phi)  = \frac{N_v}{\pi}
\end{equation}
\begin{equation}
r(\theta,\phi)  = \frac{N_uN_v}{2\pi^2 \cos(\phi)}
\end{equation}

\subsection{Cubic}

The cubic or cubemap projection refers to any rectangular arrangement of the faces of a cube. In this paper, we specifically refer to the following final arrangement of the forward (F), backward (B), left (L), right (R), up (U), and down (D) faces.

\begin{center}
\begin{squarecells}{3}
    \hline
    \rotatebox[origin=c]{270}{R}&  U \nl
    \rotatebox[origin=c]{270}{B} &  F \nl
    \rotatebox[origin=c]{270}{L} & D \nl
\end{squarecells}
\end{center}

\begin{equation}
u(x,y,z) = \begin{cases}
\frac{y\sqrt{x^2 + y^2 + z^2}}{|x|} &\text{$x = \argmax_{\nu \in \{x,y,z\}}|\nu|$ and $x>0$}\\
\frac{-z\sqrt{x^2 + y^2 + z^2}}{|x|} &\text{$x = \argmax_{\nu \in \{x,y,z\}}|\nu|$ and $x<0$}\\
\frac{-z\sqrt{x^2 + y^2 + z^2}}{|y|} &\text{$y = \argmax_{\nu \in \{x,y,z\}}|\nu|$ and $y>0$}\\
\frac{-z\sqrt{x^2 + y^2 + z^2}}{|y|} &\text{$y = \argmax_{\nu \in \{x,y,z\}}|\nu|$ and $y<0$}\\
\frac{y\sqrt{x^2 + y^2 + z^2}}{|z|} &\text{$z = \argmax_{\nu \in \{x,y,z\}}|\nu|$ and $z>0$}\\
\frac{y\sqrt{x^2 + y^2 + z^2}}{|z|} &\text{$z = \argmax_{\nu \in \{x,y,z\}}|\nu|$ and $z<0$}\\
\end{cases}
\end{equation}

\begin{equation}
v(x,y,z) = \begin{cases}
\frac{z\sqrt{x^2 + y^2 + z^2}}{|x|} &\text{$x = \argmax_{\nu \in \{x,y,z\}}|\nu|$ and $x>0$}\\
\frac{-y\sqrt{x^2 + y^2 + z^2}}{|x|} &\text{$x = \argmax_{\nu \in \{x,y,z\}}|\nu|$ and $x<0$}\\
\frac{-x\sqrt{x^2 + y^2 + z^2}}{|y|} &\text{$y = \argmax_{\nu \in \{x,y,z\}}|\nu|$ and $y>0$}\\
\frac{x\sqrt{x^2 + y^2 + z^2}}{|y|} &\text{$y = \argmax_{\nu \in \{x,y,z\}}|\nu|$ and $y<0$}\\
\frac{-x\sqrt{x^2 + y^2 + z^2}}{|z|} &\text{$z = \argmax_{\nu \in \{x,y,z\}}|\nu|$ and $z>0$}\\
\frac{x\sqrt{x^2 + y^2 + z^2}}{|z|} &\text{$z = \argmax_{\nu \in \{x,y,z\}}|\nu|$ and $z<0$}\\
\end{cases}
\end{equation}

\begin{equation}
w(x,y,z) = \sqrt{x^2 + y^2 + z^2}
\end{equation}

\begin{equation}
x(u,v,w) = \begin{cases}
 \frac{w|w|}{\sqrt{u^2 +v^2 + w^2}} &\text{$u<0$ and $|v|<1$}\\
 \frac{-w|w|}{\sqrt{u^2 +v^2 + w^2}} &\text{$u>0$ and $|v|<1$}\\
 \frac{-v|w|}{\sqrt{u^2 +v^2 + w^2}} &\text{$u>0$ and $v<-1$}\\
 \frac{v|w|}{\sqrt{u^2 +v^2 + w^2}} &\text{$u>0$ and $v>1$}\\
 \frac{-v|w|}{\sqrt{u^2 +v^2 + w^2}} &\text{$u<0$ and $v>1$}\\
 \frac{v|w|}{\sqrt{u^2 +v^2 + w^2}} &\text{$u<0$ and $v<-1$}\\
\end{cases}
\end{equation}

\begin{equation}
y(u,v,w) = \begin{cases}
\frac{ u|w|}{\sqrt{u^2 +v^2 + w^2}} &\text{$u<0$ and $|v|<1$}\\
 \frac{-v|w|}{\sqrt{u^2 +v^2 + w^2}} &\text{$u>0$ and $|v|<1$}\\
 \frac{w|w|}{\sqrt{u^2 +v^2 + w^2}} &\text{$u>0$ and $v<-1$}\\
 \frac{-w|w|}{\sqrt{u^2 +v^2 + w^2}} &\text{$u>0$ and $v>1$}\\
 \frac{u|w|}{\sqrt{u^2 +v^2 + w^2}} &\text{$u<0$ and $v>1$}\\
 \frac{u|w|}{\sqrt{u^2 +v^2 + w^2}} &\text{$u<0$ and $v<-1$}\\
\end{cases}
\end{equation}

\begin{equation}
z(u,v,w) = \begin{cases}
\frac{ v|w|}{\sqrt{u^2 +v^2 + w^2}} &\text{$u<0$ and $|v|<1$}\\
\frac{ -u|w|}{\sqrt{u^2 +v^2 + w^2}} &\text{$u>0$ and $|v|<1$}\\
\frac{ -u|w|}{\sqrt{u^2 +v^2 + w^2}} &\text{$u>0$ and $v<-1$}\\
\frac{ -u|w|}{\sqrt{u^2 +v^2 + w^2}} &\text{$u>0$ and $v>1$}\\
\frac{ w |w|}{\sqrt{u^2 +v^2 + w^2}}&\text{$u<0$ and $v>1$}\\
\frac{ -w|w|}{\sqrt{u^2 +v^2 + w^2}} &\text{$u<0$ and $v<-1$}\\
\end{cases}
\end{equation}

The Cubic, or cubemap, projection is popular because unlike the equirectangular projection, it avoids infinite resolution at the poles. It does so at the cost of lower horizon resolution and non-adjacency.

\begin{equation}
S(\theta,\phi) = \{\cos\phi \cos\theta,\cos\phi \sin\theta, \sin\phi\}
\end{equation}
\begin{equation}
R(\theta,\phi)  = \begin{cases}
\begin{bmatrix}
    1 & 0 &  0 \\  0& 1 & 0 \\ 0 & 0 & 1 \\
\end{bmatrix}  & \text{$ \cos\phi \cos\theta = \argmax_{\nu \in S(\theta,\phi)}|\nu|$ and $|\theta |<\frac{\pi}{2}$}\\
\begin{bmatrix}
 -1 & 0 &  0 \\  0 & 0 & -1 \\ 0 & -1 & 0 \\
 \end{bmatrix} &\text{$\cos\phi \cos\theta =\argmax_{\nu \in S(\theta,\phi)}|\nu|$ and $ |\theta | >\frac{\pi}{2}$}\\
\begin{bmatrix}
 0 & 1 &  0 \\  0 & 0 & -1 \\ -1 & 0 & 0 \\
 \end{bmatrix} &\text{$\cos\phi \sin\theta = \argmax_{\nu \in S(\theta,\phi)}|\nu|$ and $\theta>0$}\\
\begin{bmatrix}
 0 & -1 &  0 \\  0 & 0 & -1 \\ 1 & 0 & 0 \\
 \end{bmatrix}  &  \text{$\cos\phi \sin\theta= \argmax_{\nu \in S(\theta,\phi)}|\nu|$and $\theta<0$}\\
\begin{bmatrix}
 0 & 0 & 1 \\  0 & 1 & 0 \\ -1 & 0 & 0 \\
 \end{bmatrix}  &\text{$ \sin\phi\ = \argmax_{\nu \in S(\theta,\phi)}|\nu|$ and $\phi>0$}\\
\begin{bmatrix}
 0 & 0 & -1 \\  0 & 1 & 0 \\ 1 & 0 & 0 \\
 \end{bmatrix}  &\text{$ \sin\phi\ =\argmax_{\nu \in S(\theta,\phi)}|\nu|$ and $\phi<0$}\\
\end{cases}
\end{equation}
\begin{equation}
\begin{bmatrix}
x_R(\theta,\phi) \\   y_R(\theta,\phi)\\  z_R(\theta,\phi) \\
 \end{bmatrix} = R(\theta,\phi)
\begin{bmatrix}
 \cos\theta\cos\phi \\   \sin\theta\cos\phi \\  \sin\phi \\
 \end{bmatrix}
\end{equation}
\begin{equation}
\begin{split}
\alpha(\theta,\phi) = \arctan \left (\frac{y_R(\theta,\phi)}{x_R(\theta,\phi)}\right )
\end{split}
\end{equation}
\begin{equation}
\begin{split}
\beta(\theta,\phi) = \arctan \left (z_R(\theta,\phi)\right )
\end{split}
\end{equation}
\begin{equation}
r_u(\theta,\phi)  = \frac{N_u}{4\cos(\beta(\theta,\phi))\cos(\alpha(\theta,\phi))\sqrt{1 - \sin(\alpha(\theta,\phi))^2\cos(\beta(\theta,\phi))^2}}
\end{equation}
\begin{equation}
r_v(\theta,\phi)  = \frac{N_v}{6\cos(\alpha(\theta,\phi))\cos(\beta(\theta,\phi))^2}
\end{equation}
\begin{equation}
r(\theta,\phi)  = \frac{N_uN_v}{24\cos(\alpha(\theta,\phi))^3\cos(\beta(\theta,\phi))^3}
\end{equation}

\subsection{Lambert Equal-Area Cylindrical}

\begin{equation}
u(x,y,z) = \arctan \left ({\frac{y}{x}}\right )
\end{equation}
\begin{equation}
v(x,y,z) = \frac{z}{\sqrt{x^2 + y^2 + z^2}}
\end{equation}
\begin{equation}
w(x,y,z) = \sqrt{x^2 + y^2 + z^2}
\end{equation}

\begin{equation}
x(u,v,w) =w\cos(u) \cos(\arcsin(v)) 
\end{equation}
\begin{equation}
y(u,v,w) = w\sin(u)\cos(\arcsin(v))
\end{equation}
\begin{equation}
z(u,v,w) = w v
\end{equation}

For static streaming, we use a projection based on the Lambert cylindrical equal-area projection. Our motivation for using this model is its constant resolution and low distortion away from the poles. To counteract its spike in pixel density and distortion at the poles, we remove the caps at each pole. We then separate each cap into rings and project each ring to a rectangle.
\begin{equation}
r_u(\theta,\phi)  = \frac{N_u}{2\pi \cos(\phi)}
\end{equation}
\begin{equation}
r_v(\theta,\phi)  = \frac{N_v \cos(\phi)}{2}
\end{equation}
\begin{equation}
r(\theta,\phi)  = \frac{N_uN_v}{4\pi}
\end{equation}

%voluptua~\cite{kitware2003,Max:1995:OMF}. At vero eos et accusam et

\subsection{Facebook Pyramid}

For static streaming, we use a projection based on the Lambert cylindrical equal-area projection. Our motivation for using this model is its constant resolution and low distortion away from the poles. To counteract its spike in pixel density and distortion at the poles, we remove the caps at each pole. We then separate each cap into rings and project each ring to a rectangle.

\subsection{Cauchy}

\begin{equation}
u(x,y,z) = \gamma_u\arctan \left (\frac{\arctan \left ({\frac{y}{x}}\right )}{\gamma_u}\right)
\end{equation}
\begin{equation}
v(x,y,z) = \sin\left(\gamma_v\arctan \left (\frac{\arcsin \left ({\frac{z}{\sqrt{x^2 + y^2 + z^2}}}\right )}{\gamma_v}\right)\right )
\end{equation}
\begin{equation}
w(x,y,z) = \sqrt{x^2 + y^2 + z^2}
\end{equation}

\begin{equation}
x(u,v,w) =w\cos\left (\gamma_u\tan\left(\frac{u}{\gamma_u}\right)\right) \cos\left(\gamma_v \tan\left(\frac{\arcsin(v)}{\gamma_v}\right)\right) 
\end{equation}
\begin{equation}
y(u,v,w) =w\sin\left (\gamma_u\tan\left(\frac{u}{\gamma_u}\right)\right) \cos\left(\gamma_v \tan\left(\frac{\arcsin(v)}{\gamma_v}\right)\right) 
\end{equation}
\begin{equation}
z(u,v,w) = w\sin\left(\gamma_v \tan\left(\frac{\arcsin(v)}{\gamma_v}\right)\right)
\end{equation}

\begin{equation}
L_u = 2\gamma_u\arctan\left(\frac{\pi}{\gamma_u}\right)
\end{equation}
\begin{equation}
L_v = 2\sin\left(\gamma_v\arctan\left(\frac{\pi}{2\gamma_v}\right)\right)
\end{equation}

\begin{equation}
r_u(\theta,\phi)  = \frac{N_u}{L_u \cos(\phi)\left(1 + \left (\frac{\theta}{\gamma_u}\right)^2\right)}
\end{equation}
\begin{equation}
r_v(\theta,\phi)  =\frac{N_v\cos\left(\gamma_v\arctan \left (\frac{\phi}{\gamma_v}\right)\right)}{L_v\left(1 + \left (\frac{\phi}{\gamma_v}\right)^2\right)}
\end{equation}
\begin{equation}
r(\theta,\phi)  = \frac{N_uN_v\cos\left(\gamma_v\arctan \left (\frac{\phi}{\gamma_v}\right)\right)}{L_uL_v\cos(\phi)\left(1 + \left (\frac{\theta}{\gamma_u}\right)^2\right)\left(1 + \left (\frac{\phi}{\gamma_v}\right)^2\right)}
\end{equation}

\section{Results}



In order to measure distortion, we define $r_u$ and $r_v$ to be the horizontal and vertical resolutions which are related to the scale elements $J_u$ and $J_v$, respectively. $J_u$ and $J_v$ are simply the corresponding columns of the Jacobian.

\begin{equation}\label{eq:r_u}
R_u(\theta,\phi) = \frac{N_u}{ L_u } \frac{1}{||J_u(\theta,\phi,1)||}
\end{equation}

We can now measure the uniformity of the projection ${U}$ as a ratio of the horizontal and vertical resolution functions.

\begin{equation} \label{eq:U}
{U}(\theta,\phi) = \frac{\min \left( R_u(\theta,\phi),R_v(\theta,\phi)\right)}{\max \left( R_u(\theta,\phi),R_v(\theta,\phi)\right)}
\end{equation} 

We can compare projections by calculating weighted averages of resolution and uniformity over the unit sphere.

\begin{equation} \label{eq:U}
\bar{R} = \oiint\limits_{S}\hat{W}(\theta,\phi)R(\theta,\phi)dA
\end{equation}

\begin{equation} \label{eq:U}
\bar{U} = \oiint\limits_{S}\hat{W}(\theta,\phi)U(\theta,\phi)dA
\end{equation}

\subsection{Weighting Functions}

We compare our projections using three weighting functions. The first is an impulse function in the forward direction, measuring only the projection resolution and distortion in the forward direction. The second resembles the foveated resolution function of the human eye when looking forward. The third approximates the viewing patterns of humans while viewing VR video content.

We measure each as a ratio to the resolution of the theoretical perfect uniform projection, that has a weighted resolution of $\frac{1}{4\pi}$ and a weighted uniformity of $1$ for any weighting function. See Table \ref{vis_accept} for these metrics.

\begin{table}
  \caption{Projection Metrics}
  \label{vis_accept}
  \scriptsize
  \begin{center}
    \begin{tabular}{ccccccc}
     \; & \multicolumn{3}{c}{Resolution} & \multicolumn{3}{c}{Uniformity}\\
      Projection & center & fovea & empirical & center & fovea & empirical \\
    \hline
      Equirectangular & 0.64 & 0.65 & 0.76 & 1.00 & 0.98 & 0.88 \\
      Lambert & 1.00 & 1.00 & 1.00 & 0.64 & 0.67 & 0.71\\
      Cubic & 0.52 & 0.66 & 0.99 & 1.00 & 0.96 & 0.90 \\
      Pyramid & 1.39 & 1.75 & 1.49 & 0.86 & 0.84 & 0.59 \\
      Cauchy & 2.49 & 2.28 & 1.32 & 1.00 & 0.92 & 0.62 \\
    \end{tabular}
  \end{center}
\end{table}

\begin{figure}[htb]
  \centering
  \includegraphics[width=1.5in]{sample.eps}
  \caption{Sample illustration.}
\end{figure}

\subsection{Mezcal Head}

Duis autem~\cite{Lorensen:1987:MCA} vel eum iriure dolor in hendrerit
in vulputate velit esse molestie consequat, vel illum dolore eu
feugiat nulla facilisis at vero eros et accumsan et iusto odio
dignissim qui blandit praesent luptatum zzril delenit augue duis
dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet,
consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt
ut laoreet dolore magna aliquam erat volutpat%
\footnote{Footnotes appear at the bottom of the column}.

\subsubsection{Ejector Seat Reservation}

Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper
suscipit lobortis nisl ut aliquip ex ea commodo
consequat~\cite{Nielson:1991:TAD}. Duis autem vel eum iriure dolor in
hendrerit in vulputate velit esse molestie consequat, vel illum dolore
eu feugiat nulla facilisis at vero eros et accumsan et iusto odio
dignissim qui blandit praesent luptatum zzril delenit augue duis
dolore te feugait nulla facilisi.

\paragraph{Rejected Ejector Seat Reservation}

Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper
suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem
vel eum iriure dolor in hendrerit in vulputate velit esse molestie

\section{Conclusion}

Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat,
sed diam voluptua. At vero eos et accusam et justo duo dolores et ea
rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem
ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur
sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et
dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam
et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea
takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit
amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor
invidunt ut labore et dolore magna aliquyam erat, sed diam
voluptua. At vero eos et accusam et justo duo dolores et ea
rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem
ipsum dolor sit amet.

Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat,
sed diam voluptua. At vero eos et accusam et justo duo dolores et ea
rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem
ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur
sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et
dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam
et justo duo dolores et ea rebum. 

%% if specified like this the section will be ommitted in review mode
\acknowledgements{
The authors wish to thank A, B, C. This work was supported in part by
a grant from XYZ.}

\bibliographystyle{abbrv}
%%use following if all content of bibtex file should be shown
%\nocite{*}
\bibliography{template}
\end{document}
