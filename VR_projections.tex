\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{mathtools}
\usepackage{esint}
\usepackage{tikz}
\usepackage{float}
\usetikzlibrary{matrix,arrows,calc}

\newcounter{sarrow}
\newcommand\xrsquigarrow[1]{%
\stepcounter{sarrow}%
\begin{tikzpicture}[decoration=snake]
\node (\thesarrow) {\strut#1};
\draw[->,decorate] (\thesarrow.south west) -- (\thesarrow.south east);
\end{tikzpicture}%
}


\DeclareMathOperator\erf{erf}
\DeclareMathOperator\erfinv{erf^{-1}}

\DeclareMathOperator*{\argmin}{arg\,min}

\title{Resolution-Defined Projections for Virtual Reality Content Delivery}
\author{Charles Dunn\\YouVisit}
\date{January 2016}

\begin{document}

\maketitle

\section{Introduction}

Virtual Reality (VR) content is immersive because a user is entirely surrounded by a virtual environment. As opposed to traditional image or video displayed on a screen, VR experiences fill every possible viewing direction. A VR experience is therefore a full sphere of content. This sphere of content often must be delivered in high resolution over a bandwidth-constrained network. Compression of the content is essential, but to optimally utilize existing algorithms for image and video data compression, the input must be flat and rectangular.

A projection must be used to convert between the three-dimensional (3D) spherical content in VR space and the two-dimensional (2D) rectangular representation for input to traditional encoding methods. In this paper we will analyze this projection step and demonstrate a method for finding useful projections for VR content delivery.

\begin{figure}[H] \label{fig:intro}
\begin{tikzpicture}[scale=1]
  \coordinate (s1) at (1,0);
  \coordinate (r1) at (4.5,0);
  \coordinate (c1) at (7.5,0);
  \node at (s1) {};
  \node at (r1) {};
  \node at (c1) {};
    \draw [gray!50!white] ($ (s1) + (-1,0) $) arc (180:360:1cm and 0.5cm);
    \draw[dashed,gray!30!white] ($ (s1) + (-1,0) $) arc (180:0:1cm and 0.5cm);
    \draw[gray!50!white]  ($ (s1) + (0,1) $) arc (90:270:0.5cm and 1cm);
    \draw[dashed,gray!30!white] ($ (s1) + (0,1) $) arc (90:-90:0.5cm and 1cm);
    \draw ($ (s1) + (0,0) $) circle (1cm);
    \shade[ball color=gray!10!white,opacity=0.20] ($ (s1) + (0,0) $) circle (1cm);
    \draw[->] (1,0) (xyz cs:x=1 ) -- (xyz cs:x=.67) node[left=-.1cm] {\tiny{$y$}};
\draw[->] (1,0) (xyz cs:y=0,x=1) -- (xyz cs:y=.33,x=1) node[above=-.1cm] {\tiny{$z$}};
\draw[->] (1,0) (xyz cs:z=0,x=1) -- (xyz cs:z=-.5,x=1) node[above right=-.12cm] {\tiny{$x$}};
    
    \draw [->,shorten <=.25cm,shorten >=.25cm]  ($ (s1) + (1,0) $) to[bend left=25] node[midway,above]{${P}$} ($ (r1) + (-1,0) $) ;
    \draw [->,shorten <=.25cm,shorten >=.25cm]  ($ (r1) + (-1,0) $) to[bend left=25] node[midway,below]{${P}^{-1}$} ($ (s1) + (1,0) $) ;
     
    \draw [fill=gray!15] ($ (r1) + (-1,.5) $) -- ++(2,0) -- ++(0,-1) -- ++(-2,0) -- cycle;
    
    \draw [->, fill=gray!15,line width=.01cm] ($ (r1) + (-1,.5) + (.1,-.1) $) -- ++(.5,0) node[right]{\tiny{$u$}};
    \draw [->, fill=gray!15,line width=.01cm] ($ (r1) + (-1,.5) + (.1,-.1) $) -- ++(0,-.5) node[below]{\tiny{$v$}};
    
    \draw [->,shorten <=.25cm,shorten >=.25cm]  ($ (r1) + (1,0) $) to[bend left=25] node[midway,above]{encode} ($ (c1) + (-.25,0) $) ;
    \draw [->,shorten <=.25cm,shorten >=.25cm]  ($ (c1) + (-.25,0) $) to[bend left=25] node[midway,below]{decode} ($ (r1) + (1,0) $) ;
     
\pgfmathsetmacro{\cube}{.5}
\draw[fill=gray!15] ($(c1) + (\cube/2,\cube/2)$) -- ++(0,0,-\cube) -- ++(0,-\cube,0) -- ++(0,0,\cube) -- cycle;
\draw[fill=gray!15] ($(c1) + (\cube/2,\cube/2)$) -- ++(-\cube,0,0) -- ++(0,0,-\cube) -- ++(\cube,0,0) -- cycle;
\draw[fill=gray!15] ($(c1) + (\cube/2,\cube/2)$) -- ++(-\cube,0,0) -- ++(0,-\cube,0) -- ++(\cube,0,0) -- cycle;
\node[] at (c1) {\tiny{data}};
 
\end{tikzpicture}
\centering
\caption{Converting from spherical content to a rectangular representation allows traditional encoding algorithms to compress VR content for transmission.}
\end{figure}

\section{Projections}

The projections we will consider map 3D Cartesian coordinates, $(x,y,z)$, to a new coordinate space, $(u,v,w)$. Let $P\in \mathcal{R}^3 \rightarrow \mathcal{R}^3$ be any such projection and let $p_{(\cdot)} \in \mathcal{R}^3 \rightarrow \mathcal{R}^1$ be the associated projections to the individual coordinates $u$, $v$ and $w$.

\begin{equation}
P(x,y,z) = (u,v,w) = (p_u(x,y,z),p_v(x,y,z),p_w(x,y,z))
\end{equation}

For our applications, the projection must be invertible, with a defined $P^{-1}\in \mathcal{R}^3 \rightarrow \mathcal{R}^3$ and associated $p^{-1}_{(\cdot)} \in \mathcal{R}^3 \rightarrow \mathcal{R}^1$.

\begin{equation}
(x,y,z) = P^{-1}(u,v,w) = (p^{-1}_x(u,v,w),p^{-1}_y(u,v,w),p^{-1}_z(u,v,w))
\end{equation}

The projections we will consider must map the unit sphere (u.s.) in Cartesian space to a rectangle spanning only $u$ and $v$ in projection space, with $w=w_0$ (See Figure \ref{fig:intro}). We call this region in projection space the unit-sphere rectangule (u.s.r.).

\begin{equation}
P(x,y,z)|_{(x,y,z)\in u.s.} = (u,v,w_0) = (u,v)|_{w_0}
\end{equation}

Since the value of $w_0$ can be calculated for each projection, we will often omit the implied $|_{w_0}$ notation.

\begin{equation}
P^{-1}(u,v) \equiv P^{-1}(u,v)|_{w_0} = P^{-1}(u,v,w_0) 
\end{equation}

\subsection{Surface Integration}

Let $u\in [u_a,u_b)$, $v\in [v_a,v_b)$ and $w=w_0$ define the unit-sphere rectangle in projection space. Let $|A_P|$ be the area of the unit-sphere rectangle in projection space and let $\Sigma_P$ be a surface area element in projection space.

\begin{equation} \label{eq:Ap}
\iint\displaylimits_{u.s.r.} d\Sigma_P = \int_{v_a}^{v_b} \int_{u_a}^{u_b} \; du \; dv = (v_b - v_a)(u_b - u_a) = A_P
\end{equation}

Note that to differentiate the coordinate bases, the subscript $_C$ refers to a value in Cartesian coordinates while the subscript $_P$ refers to a value in projection coordinates. 

We also know the area of the unit sphere in Cartesian space. Let $A_C$ be the area of the unit sphere in Cartesian space and let $\Sigma_C$ be a surface area element in Cartesian space. 

\begin{equation}
\iint\displaylimits_{u.s.} d\Sigma_C = 4 \pi = A_C
\end{equation}

In order to calculate surface integrals across coordinate spaces, we must include the Jacobian of a projection. The Jacobian is a function of the inverse individual coordinate projection equations (i.e. $p^{-1}_{(\cdot)}$).

\begin{equation}
J(u,v) = \left . \begin{bmatrix}
    \frac{\partial{p^{-1}_x}}{\partial{u}} & \frac{\partial{p^{-1}_x}}{\partial{v}} & \frac{\partial{p^{-1}_x}}{\partial{w}} \\    \frac{\partial{p^{-1}_y}}{\partial{u}} & \frac{\partial{p^{-1}_y}}{\partial{v}} & \frac{\partial{p^{-1}_y}}{\partial{w}} \\    \frac{\partial{p^{-1}_z}}{\partial{u}} & \frac{\partial{p^{-1}_z}}{\partial{v}} & \frac{\partial{p^{-1}_z}}{\partial{w}} \\
\end{bmatrix}\right |_{(u,v,w_0)}
\end{equation}

We have defined the constraints on the projections $P$ we consider valid, and explained the associated functions $P^{-1}$, $p_{(\cdot)}$, $p^{-1}_{(\cdot)}$, and $J$, all of which can be derived from any valid $P$.

We can now evaluate in projection space the surface integral of a function in Cartesian space. Let $f_C(x,y,z)$ be any function in Cartesian space.

\begin{equation}
\iint\displaylimits f_C(x,y,z) \; d\Sigma_C = \iint\displaylimits f_C(P^{-1}(u,v)) \; ||J(u,v)|| \; d\Sigma_P
\end{equation}

If we are integrating over the unit sphere in Cartesian space, we can be more explicit with the integration since we are integrating over the unit-sphere rectangle in projection space.

\begin{equation} \label{eq:changeofvariable}
\begin{split}
\iint\displaylimits_{u.s.} f_C(x,y,z) \; d\Sigma_C & = \iint\displaylimits_{u.s.r.} f_C(P^{-1}(u,v)) \; ||J(u,v)|| \; d\Sigma_P \\ & = \int_{v_a}^{v_b} \int_{u_a}^{u_b} f_C(P^{-1}(u,v)) \; ||J(u,v)|| \; du \; dv
\end{split}
\end{equation}

The Jacobian of a projection defines the warping of space due to a projection. It is this same warping of space that affects the resolution of the spherical output content.

\section{Resolution Functions}

Spatial resolution $r$ is defined as the local density of independent pixels, or the local ratio of independent pixels $\partial N$ to the area they occupy $\partial \Sigma$. This is the information density of an image. Spatial resolution is not necessarily constant over an image.

\begin{equation}
r(u,v) \equiv \frac{\partial N}{\partial \Sigma} (u,v)
\end{equation}

Pixel density $\rho$ is defined as the ratio of discrete pixels $M$ to the area they occupy $|A|$. These discrete pixels are not necessarily independent, as in the case of a blurred or upsampled image. In this paper, pixel density will always be constant over an image. Even when considering the theoretical pixels over a 3D sphere of content, the pixels are all identical.\footnote{One can picture a spherical tessellation of triangular pixels to form an icosahedron.}

\begin{equation}
\rho \equiv \frac{M}{|A|} 
\end{equation}

Note that pixel density $\rho$ and spatial resolution $r$ are not necessary equivalent. They are only the same when each pixel is independent from all other pixels. Spatial resolution cannot exceed pixel density, but pixel density could be higher than spatial resolution, as is the case with a blurred or upsampled image.

\begin{equation}
r \leq \rho
\end{equation}

Let $\prescript{}{i}{r}_C$, $\prescript{}{i}{\rho}_C$, $\prescript{}{i}{N}_C$, and $|\prescript{}{i}{A}_C|$ be the spatial resolution, pixel density, independent pixel count, and total area of the spherical input content, respectively. We assume that the content was generated (via cameras or computers) in a way such that the spatial resolution is constant and equal to the pixel density. The spherical input data covers a unit sphere, so the total area of the content $\prescript{}{i}{A}_C$ is $4\pi$.

\begin{equation}
\prescript{}{i}{r}_C = \prescript{}{i}{\rho}_C = \frac{\prescript{}{i}{N}_C}{|\prescript{}{i}{A}_C|}  = \frac{\prescript{}{i}{N}_C}{4 \pi} 
\end{equation}

The resolution of the mapped data $r_P$ is the same as the pixel density $\rho_P$ if we limit the pixel count $N_P$ such that no oversampling of the spherical input content occurs. This is a reasonable assumption since the pixel count of the mapped content is typically much lower than the largest available version of the spherical input content.

\begin{equation}
r_P = \rho_P = \frac{N_P}{|A_P|}
\end{equation}

The spatial resolution of the spherical output content $r_C$ is constrained by the mapped content resolution. The pixel density of the spherical output data $\rho_C$ is determined by the field of view and screen size of the display device. Considering current VR headset resolutions and mobile network bandwidths, the pixel density will typically be much higher than the spatial resolution, which is bandwidth constrained. This is the first case where the pixel density is higher than the spatial resolution, due to oversampling when inverse projecting the mapped content to spherical output content.

\begin{equation}
r_C(x,y,z) = \frac{\partial N_C}{\partial \Sigma_C}(x,y,z)  \leq \rho_C = \frac{M}{|A_C|} =  \frac{M}{4 \pi} 
\end{equation}

The spatial resolution is not uniform due to the warping of the uniform pixels of the mapped content during reverse projection. This warping is exactly captured by the magnitude of the determinant of the Jacobian. We include a normalizing term $\alpha$ so the integral of spatial resolution is the total information content, which is equal to the total number of mapped content pixels $N_P$.

\begin{equation}
r_C(x,y,z) = \frac{\partial N_C}{\partial \Sigma_C}(x,y,z) = \frac{N_P}{ \alpha } \frac{1}{||J(P(x,y,z))||}
\end{equation}

Using Equations \ref{eq:Ap} and \ref{eq:changeofvariable}, we can solve for $\alpha$.

\begin{equation} \label{eq:alpha}
\begin{split}
\alpha & = \left | \iint\displaylimits_{u.s.} \frac{1}{||J(P(x,y,z))||}\; d\Sigma_C \right | \\
& = \left | \iint\displaylimits_{u.s.r.} \frac{1}{||J(P(P^{-1}(u,v)))||} ||J(u,v)|| \; d\Sigma_P \right | \\
& = \left | \int_{v_a}^{v_b} \int_{u_a}^{u_b} \frac{||J(u,v)||}{||J(u,v)||}\; du \; dv \right | \\
& = |A_P|
\end{split}
\end{equation}

We see that the normalizing factor $\alpha$ for any projection is equal to the area of the unit-sphere rectangle in projection space. 

We now have an equation explicitly relating a projection to the resulting spatial resolution of the final VR content.

\begin{equation} \label{eq:resalpha}
r_C(x,y,z) = \frac{N_P}{ |A_P| } \frac{1}{||J(P(x,y,z))||}
\end{equation}


\subsection{Equirectangular Resolution Function}

The Equirectangular projection is a projection from the unit sphere to a rectangle in latitude-longitude space. It is the spherical coordinate representation of a sphere, with longitude $u\in [-\pi, \pi)$, latitude $v \in [-\frac{\pi}{2}, \frac{\pi}{2}]$, and radius $w$ ($w_0 = 1$). The Equirectangular projection is the current standard for VR content delivery and storage.

\begin{equation} \label{eq:eq1}
p_x^{-1}(u,v,w) = w \cos (v)\cos (u)
\end{equation}
\begin{equation}
p_y^{-1}(u,v,w) = w \cos(v) \sin(u)
\end{equation}
\begin{equation}
p_z^{-1}(u,v,w) = w \sin(v)
\end{equation}
\begin{equation}
p_u(x,y,z) = \tan^{-1}\left (\frac{y}{x}\right )
\end{equation}
\begin{equation}
p_v(x,y,z) = \sin^{-1}\left ({\frac{z}{\sqrt{x^2 + y^2 + z^2}}}\right) 
\end{equation}
\begin{equation}
p_w(x,y,z) = \sqrt{x^2 + y^2 + z^2}
\end{equation}
\begin{equation}
\begin{split}
J(u,v,w) &  = \left . \begin{bmatrix}
    \frac{\partial{p^{-1}_x}}{\partial{u}} & \frac{\partial{p^{-1}_x}}{\partial{v}} & \frac{\partial{p^{-1}_x}}{\partial{w}} \\    \frac{\partial{p^{-1}_y}}{\partial{u}} & \frac{\partial{p^{-1}_y}}{\partial{v}} & \frac{\partial{p^{-1}_y}}{\partial{w}} \\    \frac{\partial{p^{-1}_z}}{\partial{u}} & \frac{\partial{p^{-1}_z}}{\partial{v}} & \frac{\partial{p^{-1}_z}}{\partial{w}} \\
\end{bmatrix}\right |_{(u,v,w)} \\
& = 
\begin{bmatrix}
    -w\cos(v)\sin(u) & -w\sin(v) \cos(u) & \cos(v)\sin(u) \\   w\cos(v)\cos(u) & -w\sin(v)\sin(u) & \cos(v)\sin(u) \\   0 & w\cos(v) & \sin(v) \\
\end{bmatrix}
\end{split}
\end{equation}
\begin{equation}
||J(u,v,w)||  = |w^2 \cos(v)|
\end{equation}

We now use the fact that we are transforming points from the unit sphere, so $\sqrt{x^2 + y^2 + z^2} = 1$ and $w_0 = 1$.

\begin{equation} \label{eq:eqJ}
||J(u,v)||  = \cos(v)
\end{equation}

\begin{equation} \label{eq:eq2}
|A_P| = |(v_b - v_a)(u_b - u_a) |= 2\pi^2
\end{equation}

Using Equation \ref{eq:resalpha}, we can solve for the resolution function resulting from the Equirectangular projection.

\begin{equation}
r_C(x,y,z) = \frac{N_P}{|A_P |} \frac{1}{||J(P(x,y,z))||} = \frac{N_P}{ 2 \pi^2 \cos(\sin^{-1}(z)) } = \frac{N_P}{ 2 \pi^2 \sqrt{1 - z^2} } 
\end{equation}

We see that the spatial resolution of VR content delivered via the Equirectangular projection is actually at a minimum along the equator ($z^2=0$), and approaches $\infty$ at the poles ($z^2 = 1$)! Since most VR content has important content along the equator, this is nearly the opposite of what we would like.

We have demonstrated how the projection chosen determines the shape of the spatial resolution of the final content - through the projection's Jacobian. We will now investigate how to solve for a Jacobian given a target resolution function. This way, we can specify an appropriate resolution function and derive an application-specific projection. 

\section{Resolution-Defined Projections}

Given a target resolution function $r_C(x,y,z)$, we would like to solve for the corresponding resolution-defined projection $P$. If a solution exists, using this projection will lead to a spherical output content resolution identical to our target resolution function.

\begin{equation} \label{eq:restoJ}
    r_C(x,y,z) = \frac{N_{p}}{|A_{p}|} \frac{1}{||J(P(x,y,z))||}
\end{equation}

Again, $N_{p}$ is the number of pixels in the unit-sphere rectangle in the resolution-defined projection space and $|A_{p}|$ is the area of the unit-sphere rectangle in the resolution-defined projection space. $J$ is the Jacobian for the resolution-defined projection $P$.

We can easily rearrange terms to have an explicit equation for $J$ since we define our target resolution function $r_C$.

\begin{equation} \label{eq:Jtores}
    ||J(u,v)|| = \frac{N_{p}}{|A_{p}|} \frac{1}{r_C(P^{-1}(u,v))}
\end{equation}

\subsection{Hybrid Projections}

Unfortunately, $J$ has nine degrees of freedom, so solving Equation \ref{eq:Jtores} for all the elements is extremely open ended. In order to reduce the degrees of freedom, we introduce a new constraint. We will use an existing projection from the unit sphere to a rectangle, and then apply projections to warp the rectangle.

We will call the existing valid projection an \emph{intermediate projection} $\tilde{P}$. This projection maps from the 3D unit sphere to a rectangle spanning $(a,b)$ in $(a,b,c)$ coordinate space. The next projection will be the \emph{first coordinate remapping} $\check{P}$, which will warp the rectangle along the $a$ axis, preserving the $b$ and $c$ coordinates. This first coordinate remapping will map from $(a,b,c)$ coordinates to $(d,e,f)$ coordinates. The final projection will be the \emph{second coordinate remapping} $\hat{P}$, which will warp the rectangle in $(d,e,f)$ space along the $e$ axis. This second coordinate remapping maps from $(d,e,f)$ space to $(u,v,w)$ space. We will call the combination of these three projections a \emph{hybrid projection} $\bar{P}$.

\begin{equation}
    \bar{P}(x,y,z) = \hat{P}(\check{P}(\tilde{P}(x,y,z)))) = (u,v,w)
\end{equation} 

\begin{figure}[H]
\centering
\begin{tikzpicture}
  \coordinate (si) at (1,0);
  \coordinate (r1) at (4.5,0);
  \coordinate (r2) at (8,0);
  \coordinate (r3) at (11.5,0);
  \coordinate (c1) at (14,0);
  \node at (si) {};
  \node at (r1) {};
  \node at (r2) {};
  \node at (r3) {};
  \node at (c1) {};
    \draw [gray!50!white] ($ (si) + (-1,0) $) arc (180:360:1cm and 0.5cm);
    \draw[dashed,gray!30!white] ($ (si) + (-1,0) $) arc (180:0:1cm and 0.5cm);
    \draw[gray!50!white]  ($ (si) + (0,1) $) arc (90:270:0.5cm and 1cm);
    \draw[dashed,gray!30!white] ($ (si) + (0,1) $) arc (90:-90:0.5cm and 1cm);
    \draw ($ (si) + (0,0) $) circle (1cm);
    \shade[ball color=gray!10!white,opacity=0.20] ($ (si) + (0,0) $) circle (1cm);
    \draw[->] (1,0) (xyz cs:x=1 ) -- (xyz cs:x=.67) node[left=-.1cm] {\tiny{$y$}};
\draw[->] (1,0) (xyz cs:y=0,x=1) -- (xyz cs:y=.33,x=1) node[above=-.1cm] {\tiny{$z$}};
\draw[->] (1,0) (xyz cs:z=0,x=1) -- (xyz cs:z=-.5,x=1) node[above right=-.12cm] {\tiny{$x$}};
    
    \draw [->,shorten <=.25cm,shorten >=.25cm]  ($ (si) + (1,0) $) to[bend left=25] node[midway,above]{$\tilde{P}$} ($ (r1) + (-1,0) $) ;
    \draw [->,shorten <=.25cm,shorten >=.25cm]  ($ (r1) + (-1,0) $) to[bend left=25] node[midway,below]{$\tilde{P}^{-1}$} ($ (si) + (1,0) $) ;
     
    \draw [fill=gray!15] ($ (r1) + (-1,.5) $) -- ++(2,0) -- ++(0,-1) -- ++(-2,0) -- cycle;
    
    \draw [->, fill=gray!15,line width=.01cm] ($ (r1) + (-1,.5) + (.1,-.1) $) -- ++(.5,0) node[right]{\tiny{$a$}};
    \draw [->, fill=gray!15,line width=.01cm] ($ (r1) + (-1,.5) + (.1,-.1) $) -- ++(0,-.5) node[below]{\tiny{$b$}};
    
    \draw [->,shorten <=.25cm,shorten >=.25cm]  ($ (r1) + (1,0) $) to[bend left=25] node[midway,above]{$\check{P}$} ($ (r2) + (-1,0) $) ;
    \draw [->,shorten <=.25cm,shorten >=.25cm]  ($ (r2) + (-1,0) $) to[bend left=25] node[midway,below]{$\check{P}^{-1}$} ($ (r1) + (1,0) $) ;
     
    \draw [fill=gray!15] ($ (r2) + (-.75,.5) $) -- ++(1.5,0) -- ++(0,-1) -- ++(-1.5,0) -- cycle;
    \draw [->, fill=gray!15,line width=.01cm] ($ (r2) + (-.75,.5) + (.1,-.1) $) -- ++(.5,0) node[right]{\tiny{$d$}};
    \draw [->, fill=gray!15,line width=.01cm] ($ (r2) + (-.75,.5) + (.1,-.1) $) -- ++(0,-.5) node[below]{\tiny{$e$}};
    
    \draw [->,shorten <=.25cm,shorten >=.25cm]  ($ (r2) + (1,0) $) to[bend left=25] node[midway,above]{$\hat{P}$} ($ (r3) + (-1,0) $) ;
    \draw [->,shorten <=.25cm,shorten >=.25cm]  ($ (r3) + (-1,0) $) to[bend left=25] node[midway,below]{$\hat{P}^{-1}$} ($ (r2) + (1,0) $) ;
     
    \draw [fill=gray!15] ($ (r3) + (-.75,.67) $) -- ++(1.5,0) -- ++(0,-1.34) -- ++(-1.5,0) -- cycle;
    \draw [->, fill=gray!15,line width=.01cm] ($ (r3) + (-.75,.67) + (.1,-.1) $) -- ++(.5,0) node[right]{\tiny{$u$}};
    \draw [->, fill=gray!15,line width=.01cm] ($ (r3) + (-.75,.67) + (.1,-.1) $) -- ++(0,-.5) node[below]{\tiny{$v$}};
    
    \draw [->]  ($ (si) + (1,1) $) to[bend left=10] node[midway,above]{$\bar{P}$} ($ (r3) + (-1,1) $) ;
    \draw [->]  ($ (r3) + (-1,-1) $) to[bend left=10] node[midway,below]{$\bar{P}^{-1}$} ($ (si) + (1,-1) $) ;   
   
    
\end{tikzpicture}
\caption{The steps in a hybrid projection showing how $\bar{P}$ is comprised of $\tilde{P}$, $\check{P}$ and $\hat{P}$.}
\end{figure}

The Jacobian of the hybrid projection $\bar{J}$ is simply a product of its component projections. We also use the distributive property of the determinant and then the constraints applied to $\check{P}$ and $\hat{P}$.

\begin{equation}
\begin{split}
    ||\bar{J}(u,v,w)|| & = ||\hat{J}(u,v,w) || ||\check{J}(\hat{P}^{-1}(u,v,w))||||\tilde{J}(\check{P}^{-1}(\hat{P}^{-1}(u,v,w)))|| \\
    & = ||\hat{J}(u,v,w) || ||\check{J}(u,\hat{p}_e^{-1}(v),w))||||\tilde{J}(\check{p}^{-1}_d(u),\hat{p}_e^{-1}(v),w)||
\end{split}
\end{equation}

We can solve for $\tilde{J}(a,b,c)$ given the intermediate projection. The remapping Jacobians have only one variable element in each.

\begin{equation}
    ||\check{J}(d,e,f)|| = 
\left |\det \begin{bmatrix}
    \frac{\partial{\check{p}^{-1}_a}}{\partial{d}}(d) & 0 & 0 \\
    0 & 1 & 0 \\
    0 & 0 & 1\\
\end{bmatrix}
\right | = \left | \frac{\partial{\check{p}^{-1}_a}}{\partial{d}}(d) \right |
\end{equation}

\begin{equation}
    ||\hat{J}(u,v,w)|| = 
\left |\det \begin{bmatrix}
   1 & 0 & 0 \\
    0 &  \frac{\partial{\hat{p}^{-1}_e}}{\partial{v}}(v)  & 0 \\
    0 & 0 & 1\\
\end{bmatrix}
\right | = \left | \frac{\partial{\hat{p}^{-1}_e}}{\partial{v}}(v) \right |
\end{equation}

\begin{equation} \label{eq:Jexpand}
    ||\bar{J}(u,v,w)|| =  \left | \frac{\partial{\hat{p}^{-1}_e}}{\partial{v}}(v) \right |\left | \frac{\partial{\check{p}^{-1}_a}}{\partial{d}}(u) \right | ||\tilde{J}(\check{p}^{-1}_a(u),\hat{p}_e^{-1}(v),w)||
\end{equation}

Combining equations \ref{eq:Jtores} and \ref{eq:Jexpand}, we create a multivariable first-order differential equation to solve in order to derive the resolution-defined hybrid projection $\bar{P}$.

\begin{equation} \label{eq:Jcomb}
 \left | \frac{\partial{\hat{p}^{-1}_d}}{\partial{v}}(v) \right |\left | \frac{\partial{\check{p}^{-1}_a}}{\partial{d}}(u) \right | ||\tilde{J}(\check{p}^{-1}_d(u),\hat{p}_e^{-1}(v),w)||
= \frac{N_{\bar{P}}}{|A_{\bar{P}}|} \frac{1}{r_C(\tilde{P}^{-1}(\check{p}^{-1}_d(u),\hat{p}_e^{-1}(v),w))}
\end{equation}

We now apply two additional constraints. If we restrict our intermediate projection to have a separable Jacobian and our target resolution function to be separable in $(a,b,c)$ intermediate projection space, then all of Equation \ref{eq:Jcomb} is separable.

\begin{equation}
\tilde{J}(a,b,c) = \prescript{}{a}{\tilde{J}}(a)\prescript{}{b}{\tilde{J}}(b)\prescript{}{c}{\tilde{J}}(c)
\end{equation}
\begin{equation}
r_C(x,y,z) = \prescript{}{a}{r}_C(\tilde{p}_a(x,y,z))\prescript{}{b}{r}_C(\tilde{p}_b(x,y,z))\prescript{}{c}{r}_C(\tilde{p}_c(x,y,z))
\end{equation}
\begin{equation}
r_C(\tilde{P}^{-1}(a,b,c)) = \prescript{}{a}{r}_C(a)\prescript{}{b}{r}_C(b)\prescript{}{c}{r}_C(c)
\end{equation}
\begin{equation} \label{eq:gen}
\begin{split}
 \left | \frac{\partial{\hat{p}^{-1}_e}}{\partial{v}}(v) \right |\left | \frac{\partial{\check{p}^{-1}_a}}{\partial{d}}(u) \right | ||\prescript{}{a}{\tilde{J}}(\check{p}^{-1}_a(u))\prescript{}{b}{\tilde{J}}(\hat{p}_e^{-1}(v))\prescript{}{c}{\tilde{J}}(w)||
\\ = \frac{N_{\bar{P}}}{|A_{\bar{P}}|} \frac{1}{\prescript{}{a}{r}_C(\check{p}^{-1}_a(u))\prescript{}{b}{r}_C(\hat{p}_e^{-1}(v))\prescript{}{c}{r}_C(w)}
\end{split}
\end{equation}

We can now write separate differential equations for $\check{p}^{-1}_a$ and $\hat{p}_e^{-1}$.

\begin{equation}
\left | \frac{\partial \check{p}^{-1}_a}{\partial{d}}(u) \right | \propto \frac{1}{ ||\prescript{}{a}{\tilde{J}}(\check{p}^{-1}_a(u))|| \prescript{}{a}{r}_C(\check{p}^{-1}_a(u))} 
\end{equation}
\begin{equation}
\left | \frac{\partial{\hat{p}^{-1}_e}}{\partial{v}}(v) \right | \propto \frac{1}{||\prescript{}{b}{\tilde{J}}(\hat{p}_e^{-1}(v))|| \prescript{}{b}{r}_C(\hat{p}_e^{-1}(v))} 
\end{equation}

We can drop the absolute value since $||J||$ and $r$ are positive values.
Dropping the absolute values creates a slightly stricter equation, but we get the differential equations in to a solvable form with the following solution.

\begin{equation}
 \frac{\partial{f}}{\partial{x}}(x) \propto \frac{1}{g(f(x)) h(f(x))}
 \end{equation}
\begin{equation}
f(x) = y \;\: \text{s.t.}\;  x = \int g(\gamma) h(\gamma)\; d\gamma \mid_{\gamma = y}
\end{equation}

Using these equations, we might be able to solve for $\check{p}^{-1}_a$ and $\hat{p}_e^{-1}$. $\check{p}^{-1}_a$ fully defines $\check{P}$ and $\hat{p}_e^{-1}$ fully defines $\hat{P}$. Since we get to choose an intermediate projection $\tilde{P}$ (so long as it has the requirements above are satisfied), we have all three projections that comprise the final hybrid projection $\bar{P}$.

\subsection{Equirectangular Intermediate Projection}

We can prove that the Equirectangular projection satisfies the requirements of an intermediate projection above. It is obvious from Equation \ref{eq:eqJ} that the Equirectangular Jacobian is separable.

\begin{equation} \label{eq:eq3}
\tilde{J}(a,b,c) = \cos(b)
\end{equation}
\begin{equation}
\prescript{}{a}{\tilde{J}}(a) = 1
\end{equation}
\begin{equation}
\prescript{}{b}{\tilde{J}}(b) = \cos(b)
\end{equation}
\begin{equation} \label{eq:eq4}
\prescript{}{c}{\tilde{J}}(c) = 1
\end{equation}

\section{Examples}

We can now demonstrate how to derive a hybrid projection from a target resolution function.

\subsection{Uniform Resolution Function}

If we would like a uniform resolution function, all directions in VR would be of equal visual quality. We can use the Equirectangular projection as an intermediate projection. We will employ Equations \ref{eq:eq1}-\ref{eq:eq2} and \ref{eq:eq3}-\ref{eq:eq4}. Our target uniform resolution is any positive value $k$.

\begin{equation}
r_C(x,y,z) = k = r_C(\tilde{P}^{-1}(a,b,c))
\end{equation}
We now use Equation \ref{eq:gen}.
\begin{equation} 
 \left | \frac{\partial{\hat{p}^{-1}_e}}{\partial{v}}(v) \right |\left | \frac{\partial{\check{p}^{-1}_a}}{\partial{d}}(u) \right | \cos(\hat{p}_e^{-1}(v))
= \frac{N_{\bar{P}}}{|A_{\bar{P}}|} \frac{1}{k}
\end{equation}

\begin{equation}
\frac{\partial \check{p}^{-1}_a}{\partial{d}}(u) \propto 1
\end{equation}
\begin{equation}
\frac{\partial{\hat{p}^{-1}_e}}{\partial{v}}(v) \propto \frac{1}{\cos(\hat{p}_e^{-1}(v))} 
\end{equation}
\begin{equation}
 \check{p}^{-1}_a(u) = u
\end{equation}
\begin{equation}
 \check{p}^{-1}_e(v) = \sin^{-1}(v)
\end{equation}
\begin{equation}
 \check{p}_u(a) = a
\end{equation}
\begin{equation}
 \hat{p}_v(e) = \sin(e)
\end{equation}
We now combine our first and second coordinate remappings to our Equirectangular intermediate projections to solve for the final hybrid projection of the unit sphere to a unit-sphere rectangle spanned by $(u,v)$.
\begin{equation}
\bar{p}_u(x,y,z) = \tan^{-1}\left (\frac{y}{x}\right )
\end{equation}
\begin{equation}
\bar{p}_v(x,y,z) = \sin \left (\sin^{-1}\left ({\frac{z}{\sqrt{x^2 + y^2 + z^2}}}\right) \right ) = {\frac{z}{\sqrt{x^2 + y^2 + z^2}}}
\end{equation}
\begin{equation}
\bar{p}_w(x,y,z) = \sqrt{x^2 + y^2 + z^2}
\end{equation}
For the unit sphere, $w_0 = \sqrt{x^2 + y^2 + z^2}=1$, and we can solve for a simple mapping from 3D space to 2D space. 
\begin{equation}
\bar{p}_u(x,y,z) = \tan^{-1}\left (\frac{y}{x}\right )
\end{equation}
\begin{equation}
\bar{p}_v(x,y,z) = z
\end{equation}
We have in fact derived the Lambert Cylindrical Equal-Area projection! We can now solve for the number of pixels in projection space $N_{\bar{P}}$ in order to exactly match our target resolution function.
\begin{equation}
a\in [-\pi,\pi)
\end{equation}
\begin{equation}
b\in [-\frac{\pi}{2},\frac{\pi}{2}]
\end{equation}
\begin{equation}
u\in \check{p}_u([-\pi,\pi)) = [-\pi,\pi)
\end{equation}
\begin{equation}
v\in \hat{p}_v([-\frac{\pi}{2},\frac{\pi}{2}]) = [-1,1]
\end{equation}
\begin{equation}
A_{\bar{P}} = (\pi + \pi)(1 + 1) = 4\pi
\end{equation}
\begin{equation}
N_{\bar{P}} = \frac{1}{\cos(\sin^{-1}(v))}\cos(\sin^{-1}(v)) k |A_{\bar{P}}| =4\pi k
\end{equation}
Therefore, if we would like a uniform spatial resolution of $k$ of the final spherical output content, we should use the Lambert projection with a total of $4 \pi k$ pixels.

\subsection{Gaussian Resolution Function}

If we would like to concentrate the resolution in a specific direction, we might want a bivariate gaussian shape for the resolution function. The resolution will fall off smoothly in azimuth and elevation with standard deviations $\sigma_{a}$ and $\sigma_{b}$, respectively. We can again use the Equirectangular projection as an intermediate projection.

\begin{equation}
r_C(\tilde{P}^{-1}(a,b,c)) = k\exp{\left (-\frac{a^2}{2\sigma_a^2}\right )}\exp{\left (- \frac{b^2}{2\sigma_b^2}\right)}
\end{equation}
Unfortunately, we were unable to solve for a projection with this resolution function. With the addition of a complicated term, however, we could solve for a valid solution. The extra term increases resolutions near the poles, but the result is still preferable to the Equirectangular projection.
\begin{equation}\label{eq:gaussr} 
r_C(\tilde{P}^{-1}(a,b,c)) = k\exp{\left (-\frac{a^2}{2\sigma_a^2}\right )}\exp{\left (- \frac{b^2}{2\sigma_b^2}\right)}\frac{\cos\left ( \sqrt{\frac{\pi}{2}}\sigma_b \erf \left ( \frac{b}{\sqrt{2}\sigma_b}\right ) \right ) }{\cos(b)}
\end{equation}
\begin{equation}
\frac{\partial \check{p}^{-1}_a}{\partial{d}}(u) \propto \exp{\left (\frac{(\check{p}_a^{-1}(u))^2}{2\sigma_a^2}\right )}
\end{equation}
\begin{equation}
\frac{\partial{\hat{p}^{-1}_e}}{\partial{v}}(v) \propto \frac{\exp{\left (\frac{(\hat{p}_e^{-1}(v))^2}{2\sigma_b^2}\right )}}{\cos\left ( \sqrt{\frac{\pi}{2}}\sigma_b \erf \left ( \frac{\hat{p}_e^{-1}(v)}{\sqrt{2}\sigma_b}\right ) \right ) } 
\end{equation}
\begin{equation}
 \check{p}^{-1}_a(u) = \sqrt{2}\sigma_a \erf^{-1} \left ( \frac{\sqrt{2}u}{\sqrt{\pi} \sigma_a}\right ) 
\end{equation}
\begin{equation}
 \check{p}^{-1}_e(v) = \sqrt{2}\sigma_b \erf^{-1} \left ( \frac{\sqrt{2}\sin^{-1}(v)}{\sqrt{\pi} \sigma_b}\right ) 
\end{equation}
\begin{equation}
 \check{p}_u(a) = \frac{\sqrt{\pi}\sigma_a}{\sqrt{2}}\erf\left(\frac{a}{\sqrt{2}\sigma_a}\right) 
\end{equation}
\begin{equation}
 \hat{p}_v(e) = \sin\left ( \frac{\sqrt{\pi}\sigma_b}{\sqrt{2}}\erf\left(\frac{e}{\sqrt{2}\sigma_b}\right) \right)
\end{equation}%
\begin{equation}
\bar{p}_u(x,y,z) = \frac{\sqrt{\pi}\sigma_a}{\sqrt{2}}\erf\left(\frac{\tan^{-1}\left (\frac{y}{x}\right )}{\sqrt{2}\sigma_a}\right) 
\end{equation}
\begin{equation}
\bar{p}_v(x,y,z) = \sin\left ( \frac{\sqrt{\pi}\sigma_b}{\sqrt{2}}\erf\left(\frac{
\sin^{-1}\left ({\frac{z}{\sqrt{x^2 + y^2 + z^2}}}\right) }{\sqrt{2}\sigma_b}\right) \right)
\end{equation}
\begin{equation}
\bar{p}_w(x,y,z) = \sqrt{x^2 + y^2 + z^2}
\end{equation}
For the unit sphere, $w_0 = \sqrt{x^2 + y^2 + z^2}=1$, and we have our final mapping from $(x,y,z)$ to our unit-sphere rectangle in $(u,v)$ space.
\begin{equation}
\bar{p}_u(x,y,z) = \frac{\sqrt{\pi}\sigma_a}{\sqrt{2}}\erf\left(\frac{\tan^{-1}\left (\frac{y}{x}\right )}{\sqrt{2}\sigma_a}\right) 
\end{equation}
\begin{equation}
\bar{p}_v(x,y,z) = \sin\left ( \frac{\sqrt{\pi}\sigma_b}{\sqrt{2}}\erf\left(\frac{
\sin^{-1}\left ({z}\right) }{\sqrt{2}\sigma_b}\right) \right)
\end{equation}
We again solve for the number of pixels in projection space $N_{\bar{P}}$ in order to match any value of $k$ in the original resolution function.
\begin{equation}
a\in [-\pi,\pi)
\end{equation}
\begin{equation}
b\in [-\frac{\pi}{2},\frac{\pi}{2}]
\end{equation}
\begin{equation}
u\in \check{p}_u([-\pi,\pi)) = \left[\frac{\sqrt{\pi}\sigma_a}{\sqrt{2}}\erf\left(\frac{-\pi}{\sqrt{2}\sigma_a}\right) ,\frac{\sqrt{\pi}\sigma_a}{\sqrt{2}}\erf\left(\frac{\pi}{\sqrt{2}\sigma_a}\right)\right)
\end{equation}
\begin{equation}
v\in \hat{p}_v([-\frac{\pi}{2},\frac{\pi}{2}]) = \left [\sin\left ( \frac{\sqrt{\pi}\sigma_b}{\sqrt{2}}\erf\left(\frac{-\frac{\pi}{2}}{\sqrt{2}\sigma_b}\right) \right), \sin\left ( \frac{\sqrt{\pi}\sigma_b}{\sqrt{2}}\erf\left(\frac{\frac{\pi}{2}}{\sqrt{2}\sigma_b}\right) \right)\right ]
\end{equation}
We use the fact that $\sin$ and $\erf$ are both odd functions to simplify the calculation of the total area.
\begin{equation}
\begin{split}
A_{\bar{P}}&  = 2\left (\frac{\sqrt{\pi}\sigma_a}{\sqrt{2}}\erf\left(\frac{\pi}{\sqrt{2}\sigma_a}\right)\right)2\left( \sin\left ( \frac{\sqrt{\pi}\sigma_b}{\sqrt{2}}\erf\left(\frac{\frac{\pi}{2}}{\sqrt{2}\sigma_b}\right) \right) \right) \\ 
& = 2\sqrt{2\pi}\sigma_a\erf\left(\frac{\pi}{\sqrt{2}\sigma_a}\right)\sin\left ( \frac{\sqrt{\pi}\sigma_b}{\sqrt{2}}\erf\left(\frac{\frac{\pi}{2}}{\sqrt{2}\sigma_b}\right) \right)
\end{split}
\end{equation}
Just like the uniform resolution projection previously, all the terms except $k$ and $|A_{\bar{P}}|$ cancel in the equation for the number of pixels $N_{\bar{p}}$.
\begin{equation}
N_{\bar{P}} = k |A_{\bar{P}}| = k2\sqrt{2\pi}\sigma_a\erf\left(\frac{\pi}{\sqrt{2}\sigma_a}\right)\sin\left ( \frac{\sqrt{\pi}\sigma_b}{\sqrt{2}}\erf\left(\frac{\frac{\pi}{2}}{\sqrt{2}\sigma_b}\right) \right)
\end{equation}
Note that in Equation \ref{eq:gaussr}, the resolution at $(a,b)=(0,0)$ (forward, center), is equal to $k$. Therefore, we must simply scale our desired forward resolution by $|A_{\bar{P}}|$ to calculate the required number of pixels.

\subsection{Summary of Projections}

Below, we summarize the resolution functions of the projections we analyzed in this paper. We use Equirectangular latitude-longitude coordinates $(a,b)$ in each, with radius $c=c_0=1$.

\subsubsection{Equirectangular}

\begin{equation}
r_C(\tilde{P}^{-1}(a,b)) = \frac{N_P}{ 2 \pi^2 \cos(b) } 
\end{equation}

\subsubsection{Lambert}

\begin{equation}
r_C(\tilde{P}^{-1}(a,b)) = \frac{N_{\bar{P}}}{4 \pi}
\end{equation}

\subsubsection{Gaussian}

\begin{equation}
r_C(\tilde{P}^{-1}(a,b)) = \frac{N_{\bar{P}}\exp{\left (-\frac{a^2}{2\sigma_a^2}\right )}\exp{\left (- \frac{b^2}{2\sigma_b^2}\right)}\cos\left ( \sqrt{\frac{\pi}{2}}\sigma_b \erf \left ( \frac{b}{\sqrt{2}\sigma_b}\right ) \right )}{2\sqrt{2\pi}\sigma_a\cos(b)\erf\left(\frac{\pi}{\sqrt{2}\sigma_a}\right)\sin\left ( \frac{\sqrt{\pi}\sigma_b}{\sqrt{2}}\erf\left(\frac{\frac{\pi}{2}}{\sqrt{2}\sigma_b}\right) \right)}
\end{equation}




\end{document}
